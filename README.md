# CRATER-DATA

Ce repo contient l'application python crater-data, qui permet de générer l'ensemble des données utilisées par CRATer
sous forme de fichiers csv.

## Comment contribuer ?

Toutes les contributions sont les bienvenues sur le projet !

Etapes à suivre pour contribuer :

0. [pré requis] Disposer d'un compte framagit, si besoin [le créer ici](https://framagit.org/users/sign_up) (attention un délai de 1 à 5 jours peut être nécessaire)
1. installer un environnement de dév sur votre poste
2. se positionner sur une issue
3. développer en local
4. s'assurer que le code est valide
5. faire relire son code et le fusionner





## 1) Installer l'environnement de développement

Les instructions ci-dessous sont valables pour linux, mac. 

### Cloner le repository git de crater-data

```
git clone git@framagit.org:lga/crater-data.git 
cd crater-data
```

### Installer conda via miniforge, puis conda-lock

Le projet utilise :
* conda via miniforge pour simplifier l'installation sur différents systèmes (conda forge de manière plus systématique les packages compilés, yc pour les nouveaux Mac M1)
* conda-lock pour gérer les dépendances


1/ Installer miniforge

La version recommandée est miniforge3-4.10.3-10
Il est possible d'installer directement miniforge (voir doc sur le site miniforge), ou bien de passer par pyenv (pyenv permet de gérer facilement plusieurs environnements
python)


Pour l'installation via pyenv :
```
# Installer pyenv voir https://github.com/pyenv), voir https://github.com/pyenv/pyenv-installer
curl https://pyenv.run | bash

# Mettre à jour pyenv pour disposer des dernières versions disponibles
pyenv update
# Voir les versions de miniforge 
pyenv install -l | grep miniforge
# installer la version souhaitée de miniforge dans pyenv
pyenv install miniforge3-4.10.3-10

# Définir miniforge3-4.10.3-10 comme l'environnement python par défaut pour le projet crater-data
# Cela a pour effet de générer le fichier .python-version, Rq : ce fichier est déjà présent dans le repo git de crater-data
pyenv local miniforge3-4.10.3-10
```

2/ Créer un environnement pour crater-data

Depuis le dossier crater-data :

```
# Installer conda-lock
conda install -c conda-forge conda-lock

# Créer un environnement avec toutes les dépendances du projet dans le dossier ./conda-env
conda-lock install -p conda-env

# Activer l'environnement
conda activate ./conda-env
```



### Mettre à jour ou installer les dépendances Python

A la racine du projet :


- `conda-lock -f environment.yml` pour recréer le fichier de lock
- `conda-lock install -p conda-env` pour installer les dépendances ou les mettre à jour selon leur description dans le fichier lock

### Cas particulier de Windows

Il est également possible d'installer un environnement de développement sur Windows :

* soit en natif, en installant la version windows de miniforge. Dans ce cas, l'ensemble des commandes décrites dans cette documentation ne sont pas utilisables (car nécessitent un shell linux par ex bash)
* soit en installant au préalable WSL (voir [ici](https://learn.microsoft.com/fr-fr/windows/wsl/)), ce qui permet de disposer d'un environnement Linux et d'un shell. Il faut alors installer miniforge en version linux dans cet environnement. Il est également possible d'utiliser VS Code avec cet environnement, voir [ici](https://learn.microsoft.com/fr-fr/windows/wsl/tutorials/wsl-vscode)


## 2) Se positionner sur une issue

Voir le paragraphe correspondant dans [ce guide](https://framagit.org/lga/crater/-/blob/main/guides/processus-de-developpement.md)

La liste des issues crater-data ouvertes à contribution est visible [dans ce board](https://framagit.org/lga/crater-data/-/boards?label_name[]=Contribution)


## 3) Développer en local

Voir une description de l'[architecture de crater-data](./architecture-data.md)

Il faut respecter les standards de dev du projet, voir pour cela les [règles de codage](https://framagit.org/lga/crater/-/blob/main/guides/regles-de-codage.md)


## 4) Vérifier son code

Voir le paragraphe correspondant dans [ce guide](https://framagit.org/lga/crater/-/blob/main/guides/processus-de-developpement.md)

Et en complément, se mettre à la racine du projet, et lancer le script de validation :
```plantuml
./validate.sh
```

Ce script est a exécuter avant chaque commit. Il permet de formatter  le code (avec black) et de lancer les tests unitaires. Il faut vérifier dans le log du script que tous les tests sont OK avant de faire le commit.

## 5) Faire relire son code et le fusionner

Voir le paragraphe correspondant dans [ce guide](https://framagit.org/lga/crater/-/blob/main/guides/processus-de-developpement.md)

## Commplément : Exécuter les chaînes de traitement crater-data

### Pré-requis

a) Dérouler les étapes d'installation (voir 1. ci-dessus)

b) Récupếrer les dossiers contenant les données sources et résultats :

* les données sources sont disponibles sur l'espace partagé des Greniers d'Abondance (demande d'accès à faire par mail
  crater@resiliencealimentaire.org). Le dossier crater-data-sources contenant ces données doit être positionné au même
  niveau de le dossier crater-data (dans le même repertoire parent)
* la dernière version des données résultats est disponible dans le
  repo [crater-data-resultats](https://framagit.org/lga/crater-data-resultats) (demande d'accès à faire via gitlab).

### Lancer un traitement

crater-data utilise les fichiers présents dans crater-data-sources pour générer les résultats sous forme de fichiers csv
dans crater-data-resultats.

crater-data permet également de récupérer certains fichiers sources (en automatisant leur téléchargement par exemple)

Le script principal permet lancer une étape de traitement en particulier.

Pour consulter la liste des étapes disponible :

```
cd crater-data
python -m crater.crater_data --help
```

Pour lancer une étape en particulier (ici la génération de l'ensemble des fichiers résultat crater-data) :

```
cd crater-data
python -m crater.crater_data crater
```

### Détails des étapes de traitement

Le diagramme suivant présente les étapes proposées par crater-data, ainsi que leur lien de dépendance.

```mermaid
flowchart TB

   
  subgraph telecharger
  crt-copie([crater-referentiel-territoires])
  t-rpg([telecharger-rpg])
  tpt([telecharger-parcel-id-territoires])  
  tpa([telecharger-parcel-besoins-assiette-actuelle])
  crt-copie --> tpt
  tpt --> tpa  
  end
  
  subgraph "crater-complet / crater-incomplet-rapide"
  crt([crater-referentiel-territoires])
  cdt([crater-donnees-territoires])
  ccs([crater-calculer-sau])
  cia([crater-intrants-agricoles])
  cpb([crater-productions-besoins])
  cpa([crater-pratiques-agricoles])
  cpo([crater-population-agricole])
  cpf([crater-politique-fonciere])
  cp([crater-production])
  cca([crater-carte])
  cc[DEBUT crater-complet] & ci[DEBUT crater-incomplet-rapide] --> crt
  ccs --> cpb
  crt -. referentiel_territoires.csv .-> ccs
  crt -- referentiel_territoires.csv --> cdt
  ccs -. sau_rpg_communes_Rxx.csv .-> cdt & cpb
  crt -- referentiel_territoires.csv --> territoires[/referentiel et donnees territoires.csv/]
  cdt -- donnees_territoires.csv--> territoires[/referentiel et donnees territoires.csv/]
  territoires --> cpo & cpf & cpa & cpb & cia & cca
  cpo -- population_agricole.csv --> FIN
  cpf -- politique_fonciere.csv --> FIN
  cpa -- pratiques_agricoles.csv --> cp
  cpb -- productions_besoins/*.csv --> cp
  cp -- production.csv --> FIN
  cia -- intrants_agricoles_*.csv --> FIN
  cca -- *.geojson .-> FIN
  end

  t-rpg -. sources rpg ...-> ccs
  tpa -. besoins_assiette_actuelle .-> cpb

```
