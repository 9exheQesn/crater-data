echo "### Lancement de Black..."
python -m black . --line-length=150
echo "### Lancement de Ruff..."
python -m ruff --fix .
echo "### Lancement de Mypy..."
mypy crater tests
echo "### Lancement des tests unitaires..."
python -m unittest
