Descriptif des fichiers de config Parcel :

* parcel_produits_et_categories_202008.csv : liste des codes produits utilisés par PARCEL, fichier construit à partir des informations fournies par le Basic
* produits_hors_elevage_vers_codes_cultures.csv : correspondance entre les codes produits hors élevage de PARCEL  et les codes culture CRATer.
* produits_elevage_vers_codes_cultures.csv : correspondance entre les codes produits élevage de PARCEL et les codes cultures CRATer, en incluant les données de rendement pour chaque région
* correspondance_territoires_parcel_crater.csv : liste des territoires avec leur ID parcel (nécessaire pour récupérer les données sur les besoins).
    => Fichier construit à partir de données fournies par le BASIC (id communes),
    => complété avec une liste de communes extraites de l'api parcel (Paris, Marseille, Lyon et Corse) (Processus semi automatique, voir scripts dans crater/parcel)
    => et complété également avec les IDs de Region, Departement et EPCI  (Processus semi automatique, voir scripts dans crater/parcel)

