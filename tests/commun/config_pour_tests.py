from pathlib import Path

CHEMIN_INPUT_DATA = Path("tests/data/input")
CHEMIN_OUTPUT_DATA = Path("tests/data/output")
CHEMIN_EXPECTED_DATA = Path("tests/data/expected")
