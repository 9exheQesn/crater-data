import unittest
from pathlib import Path

from crater.pratiques_agricoles.calculateur_pratiques_agricoles import (
    calculer_pratiques_agricoles,
)
from crater.territoires.chargeur_territoires import (
    charger_territoires,
)
from tests.commun.outils_pour_tests import (
    assert_csv_files_are_equals,
    augmenter_nombre_colonnes_affichees_pandas,
)

augmenter_nombre_colonnes_affichees_pandas()


CHEMIN_INPUT_DATA = Path("tests/pratiques_agricoles/data/input")
CHEMIN_OUTPUT_DATA = Path("tests/pratiques_agricoles/data/output")
CHEMIN_EXPECTED_DATA = Path("tests/pratiques_agricoles/data/expected")


class TestPratiquesAgricoles(unittest.TestCase):
    def test_pratiques_agricoles(self):
        # given
        territoires = territoires = charger_territoires(CHEMIN_INPUT_DATA / "territoires", CHEMIN_INPUT_DATA / "territoires")
        # when
        calculer_pratiques_agricoles(
            territoires,
            CHEMIN_INPUT_DATA / "hvn.xlsx",
            CHEMIN_INPUT_DATA / "surfaces_bio.xlsx",
            CHEMIN_OUTPUT_DATA,
        )
        # then
        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA / "pratiques_agricoles.csv",
            CHEMIN_OUTPUT_DATA / "pratiques_agricoles.csv",
        )


if __name__ == "__main__":
    unittest.main()
