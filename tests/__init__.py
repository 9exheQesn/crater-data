import os

# Force le positionnement dans le dossier crater-data
# Permet d'éviter de configurer les config de lancement de test dans pycharm (qui sinon se place dans le dossier tests)
os.chdir(os.getcwd().split("crater-data")[0] + "crater-data")
