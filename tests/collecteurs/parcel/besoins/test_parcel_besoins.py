import unittest
from pathlib import Path

from crater.collecteurs.parcel import collecteur_besoins_api_parcel
from crater.collecteurs.parcel.collecteur_besoins_api_parcel import ID_ASSIETTE_ACTUELLE
from tests.commun.outils_pour_tests import assert_csv_files_are_equals
from tests.commun.outils_pour_tests import (
    augmenter_nombre_colonnes_affichees_pandas,
)

augmenter_nombre_colonnes_affichees_pandas()

CHEMIN_INPUT_DATA = Path("tests/parcel/besoins/data/input")
CHEMIN_OUTPUT_DATA = Path("tests/parcel/besoins/data/output")
CHEMIN_EXPECTED_DATA = Path("tests/parcel/besoins/data/expected")


class TestApiParcelBesoins(unittest.TestCase):
    @unittest.skip("Ce test dépend des données PARCEL - permet de voir si elles ont changé ou pas. Pas lancé de " "manière automatique")
    def test_collecter_reponse_api_parcel_besoins_sau_par_produits(self):
        collecteur_besoins_api_parcel.collecter_donnees_besoins_parcel(
            ID_ASSIETTE_ACTUELLE,
            CHEMIN_INPUT_DATA / "correspondance_territoires_parcel_crater_VALIDE.csv",
            CHEMIN_INPUT_DATA / "parcel_produits_et_categories_202008.csv",
            CHEMIN_OUTPUT_DATA / "response_api_parcel_besoins_sau_par_produits.csv",
        )

        assert_csv_files_are_equals(
            expected_file_path=CHEMIN_EXPECTED_DATA / "response_api_parcel_besoins_sau_par_produits.csv",
            result_file_path=CHEMIN_OUTPUT_DATA / "response_api_parcel_besoins_sau_par_produits.csv",
            colonnes_a_exclure=["date_import"],
        )


if __name__ == "__main__":
    unittest.main()
