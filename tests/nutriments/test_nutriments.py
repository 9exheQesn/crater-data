import unittest
from pathlib import Path

from crater.commun.export_fichier import reinitialiser_dossier
from crater.nutriments.calculateur_azote import (
    calculer_indicateurs_azote,
)
from crater.territoires.chargeur_territoires import (
    charger_territoires,
)
from tests.commun.outils_pour_tests import (
    assert_csv_files_are_equals,
    augmenter_nombre_colonnes_affichees_pandas,
)

augmenter_nombre_colonnes_affichees_pandas()

CHEMIN_INPUT_DATA = Path("tests/nutriments/data/input")
CHEMIN_OUTPUT_DATA = Path("tests/nutriments/data/output")
CHEMIN_EXPECTED_DATA = Path("tests/nutriments/data/expected")


class TestNutriments(unittest.TestCase):
    def test_flux_azote(self):
        # given
        reinitialiser_dossier(CHEMIN_OUTPUT_DATA)
        territoires = charger_territoires(CHEMIN_INPUT_DATA / "territoires", CHEMIN_INPUT_DATA / "territoires")
        # when
        calculer_indicateurs_azote(
            territoires,
            CHEMIN_INPUT_DATA / "flux_azote.xlsx",
            CHEMIN_OUTPUT_DATA,
        )
        # then
        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA / "flux_azote.csv",
            CHEMIN_OUTPUT_DATA / "flux_azote.csv",
        )


if __name__ == "__main__":
    unittest.main()
