import unittest
from pathlib import Path

from crater.commun.export_fichier import reinitialiser_dossier
from crater.territoires.otex.calculateur_otex_territoires import calculer_otex_territoires
from crater.territoires.chargeur_territoires import (
    charger_territoires,
)

from tests.commun.outils_pour_tests import (
    assert_csv_files_are_equals,
    augmenter_nombre_colonnes_affichees_pandas,
)

augmenter_nombre_colonnes_affichees_pandas()

CHEMIN_INPUT_DATA = Path("tests/territoires/otex/data/input")
CHEMIN_OUTPUT_DATA = Path("tests/territoires/otex/data/output")
CHEMIN_EXPECTED_DATA = Path("tests/territoires/otex/data/expected")


class TestTerritoiresOtex(unittest.TestCase):
    def test_calculer_otex_territoires(self):
        # given
        reinitialiser_dossier(CHEMIN_OUTPUT_DATA)
        territoires = charger_territoires(CHEMIN_INPUT_DATA / "territoires", CHEMIN_INPUT_DATA / "territoires")
        # when
        calculer_otex_territoires(
            territoires,
            CHEMIN_INPUT_DATA / "cartostat.csv",
            CHEMIN_OUTPUT_DATA,
        )
        # then
        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA / "otex_territoires.csv",
            CHEMIN_OUTPUT_DATA / "otex_territoires.csv",
        )


if __name__ == "__main__":
    unittest.main()
