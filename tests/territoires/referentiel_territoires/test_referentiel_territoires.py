import unittest
import pandas as pd
from pathlib import Path

from crater.territoires.calculateur_referentiel_territoires import calculer_referentiel_territoires, _modifier_noms_epcis
from crater.territoires.chargeur_regroupements_communes import (
    _ajouter_le_departement_de_la_commune_principale_aux_noms_des_regroupements_communes_non_uniques,
)
from tests.commun.outils_pour_tests import (
    assert_csv_files_are_equals,
    augmenter_nombre_colonnes_affichees_pandas,
)

augmenter_nombre_colonnes_affichees_pandas()

CHEMIN_INPUT_DATA = Path("tests/territoires/referentiel_territoires/data/input")
CHEMIN_OUTPUT_DATA = Path("tests/territoires/referentiel_territoires/data/output")
CHEMIN_EXPECTED_DATA = Path("tests/territoires/referentiel_territoires/data/expected")


class TestReferentielTerritoires(unittest.TestCase):
    def test_modifier_noms_epcis(self):
        # given
        df = pd.Series(
            [
                "CA ca CA",
                "CC cc CC",
                "CU cu CU",
                "blabla (ABC) toto (DEF)",
            ]
        )
        # when
        df_resultat = _modifier_noms_epcis(df)
        # then
        df_resultat_attendu = pd.Series(
            [
                "Communauté d'agglomération ca CA",
                "Communauté de communes cc CC",
                "Communauté urbaine cu CU",
                "blabla toto",
            ]
        )
        pd.testing.assert_series_equal(df_resultat, df_resultat_attendu)

    def test_ajouter_le_departement_de_la_commune_principale_aux_noms_des_regroupements_communes_non_uniques(self):
        # given
        df = pd.DataFrame(
            columns=["id_commune", "nom_commune", "id_regroupement_communes", "nom_regroupement_communes"],
            data=[
                ["56000", "A", "BV1", "A"],
                ["56001", "X", "BV1", "A"],
                ["29000", "B", "BV2", "B"],
                ["31000", "X", "BV2", "B"],
                ["29000", "C", "BV3", "C"],
                ["31000", "C", "BV4", "C"],
                ["29000", "D", "BV5", "D"],
                ["30000", "D", "BV6", "D"],
                ["31000", "X", "BV6", "D"],
            ],
        )
        # when
        df_resultat = _ajouter_le_departement_de_la_commune_principale_aux_noms_des_regroupements_communes_non_uniques(df)
        # then
        df_resultat_attendu = pd.DataFrame(
            columns=["id_commune", "nom_commune", "id_regroupement_communes", "nom_regroupement_communes"],
            data=[
                ["56000", "A", "BV1", "A"],
                ["56001", "X", "BV1", "A"],
                ["29000", "B", "BV2", "B"],
                ["31000", "X", "BV2", "B"],
                ["29000", "C", "BV3", "C (29)"],
                ["31000", "C", "BV4", "C (31)"],
                ["29000", "D", "BV5", "D (29)"],
                ["30000", "D", "BV6", "D (30)"],
                ["31000", "X", "BV6", "D (30)"],
            ],
        )
        pd.testing.assert_frame_equal(df_resultat, df_resultat_attendu)

    def test_calculer_referentiel_territoires(self):

        calculer_referentiel_territoires(
            CHEMIN_INPUT_DATA / "communes_et_arrondissements.xlsx",
            CHEMIN_INPUT_DATA / "table_passage_communes.xlsx",
            CHEMIN_INPUT_DATA / "epcis.xlsx",
            CHEMIN_INPUT_DATA / "departements.csv",
            CHEMIN_INPUT_DATA / "regions.csv",
            CHEMIN_INPUT_DATA / "codes_postaux.csv",
            [
                {
                    "code": "PAT",
                    "fichiers_sources": [CHEMIN_INPUT_DATA / "regroupements_geographiques" / "pat.xlsx"],
                },
                {
                    "code": "SCOT",
                    "fichiers_sources": [CHEMIN_INPUT_DATA / "regroupements_geographiques" / "scot.xlsx"],
                },
            ],
            CHEMIN_OUTPUT_DATA,
        )

        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA / "referentiel_territoires.csv",
            CHEMIN_OUTPUT_DATA / "referentiel_territoires.csv",
        )


if __name__ == "__main__":
    unittest.main()
