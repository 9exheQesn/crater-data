import unittest
from pathlib import Path

from crater.territoires.calculateur_donnees_territoires import (
    calculer_donnees_territoires,
)
from crater.territoires.calculateur_referentiel_territoires import (
    charger_referentiel_territoires,
)
from tests.commun.outils_pour_tests import (
    assert_csv_files_are_equals,
    augmenter_nombre_colonnes_affichees_pandas,
)

augmenter_nombre_colonnes_affichees_pandas()

CHEMIN_INPUT_DATA = Path("tests/territoires/donnees_territoires/data/input")
CHEMIN_OUTPUT_DATA = Path("tests/territoires/donnees_territoires/data/output")
CHEMIN_EXPECTED_DATA = Path("tests/territoires/donnees_territoires/data/expected")


class TestDonneesTerritoires(unittest.TestCase):
    def test_calculer_donnees_territoires(self):
        territoires = charger_referentiel_territoires(CHEMIN_INPUT_DATA)

        calculer_donnees_territoires(
            territoires,
            CHEMIN_INPUT_DATA / "geometries_communes_test",
            CHEMIN_INPUT_DATA / "population_totale_2017.csv",
            CHEMIN_INPUT_DATA / "population_totale_historique_2015.xlsx",
            CHEMIN_INPUT_DATA / "sau_par_commune_et_culture",
            CHEMIN_INPUT_DATA / "agreste" / "2010",
            CHEMIN_INPUT_DATA / "agreste" / "2020" / "cartostat.csv",
            CHEMIN_INPUT_DATA / "correspondance_nomenclature_rpg_vers_crater.csv",
            CHEMIN_INPUT_DATA / "correspondance_territoires_parcel_crater.csv",
            CHEMIN_OUTPUT_DATA,
        )

        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA / "donnees_territoires.csv",
            CHEMIN_OUTPUT_DATA / "donnees_territoires.csv",
        )


if __name__ == "__main__":
    unittest.main()
