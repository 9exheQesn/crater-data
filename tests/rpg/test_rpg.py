import unittest
from pathlib import Path

from crater.commun.config import URL_SOURCE_RPG
from crater.surface_agricole_utile import collecteur_rpg
from tests.commun.outils_pour_tests import augmenter_nombre_colonnes_affichees_pandas

augmenter_nombre_colonnes_affichees_pandas()

NOMS_FICHIERS_RPG = {2017: {"R-1": "RPG_2-0__SHP_UTM20W84GUAD_R01-2017_2017-01-01.7z.001"}}
CHEMIN_OUTPUT_DATA = Path("tests/data/output")


class TestRPG(unittest.TestCase):
    @unittest.skip("Mecanisme de collecte du RPG sur le site IGN peu fiable et exécuté ponctuellement donc on ne l'éxécute pas automatiquement")
    def test_telecharger_rpg_regions(self):
        annee = 2017
        collecteur_rpg.telecharger_rpg_toutes_les_regions(URL_SOURCE_RPG, NOMS_FICHIERS_RPG, annee, CHEMIN_OUTPUT_DATA)
        for code_region in NOMS_FICHIERS_RPG[annee]:
            fichier_est_telecharge = (CHEMIN_OUTPUT_DATA / str(annee) / NOMS_FICHIERS_RPG[annee][code_region]).is_file()
            self.assertTrue(fichier_est_telecharge)


if __name__ == "__main__":
    unittest.main()
