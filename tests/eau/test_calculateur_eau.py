import unittest
from pathlib import Path

from crater.eau.calculateur_eau import calculer_indicateurs_eau
from crater.territoires.chargeur_territoires import (
    charger_territoires,
)
from tests.commun.outils_pour_tests import (
    assert_csv_files_are_equals,
    augmenter_nombre_colonnes_affichees_pandas,
)

augmenter_nombre_colonnes_affichees_pandas()

CHEMIN_INPUT_DATA = Path("tests/eau/data/input")
CHEMIN_OUTPUT_DATA = Path("tests/eau/data/output")
CHEMIN_EXPECTED_DATA = Path("tests/eau/data/expected")


class TestEau(unittest.TestCase):
    def test_calculer_indicateurs_eau(self):
        # given
        territoires = territoires = charger_territoires(CHEMIN_INPUT_DATA / "territoires", CHEMIN_INPUT_DATA / "territoires")
        # when
        calculer_indicateurs_eau(
            territoires,
            CHEMIN_INPUT_DATA / "prelevements_eau_par_communes.csv",
            CHEMIN_OUTPUT_DATA,
        )
        # then
        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA / "eau.csv",
            CHEMIN_OUTPUT_DATA / "eau.csv",
        )


if __name__ == "__main__":
    unittest.main()
