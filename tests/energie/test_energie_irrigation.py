import unittest

from crater.commun.export_fichier import reinitialiser_dossier
from crater.energie.calculateur_energie_irrigation import calculer_indicateurs_energie_irrigation
from crater.territoires.chargeur_territoires import (
    charger_territoires,
)
from tests.commun.outils_pour_tests import (
    assert_csv_files_are_equals,
    augmenter_nombre_colonnes_affichees_pandas,
)
from tests.energie.config_test_energie import CHEMIN_OUTPUT_DATA_ENERGIE, CHEMIN_EXPECTED_DATA_ENERGIE, CHEMIN_INPUT_DATA_ENERGIE

augmenter_nombre_colonnes_affichees_pandas()

DOSSIER_ENERGIE_IRRIGATION = "irrigation"


class TestEnergieIrrigation(unittest.TestCase):
    def test_calculer_indicateurs_energie_irrigation(self):
        # given
        reinitialiser_dossier(CHEMIN_OUTPUT_DATA_ENERGIE / DOSSIER_ENERGIE_IRRIGATION)
        territoires = territoires = charger_territoires(
            CHEMIN_INPUT_DATA_ENERGIE / DOSSIER_ENERGIE_IRRIGATION / "territoires",
            CHEMIN_INPUT_DATA_ENERGIE / DOSSIER_ENERGIE_IRRIGATION / "territoires",
        )
        # when
        calculer_indicateurs_energie_irrigation(territoires, CHEMIN_INPUT_DATA_ENERGIE / DOSSIER_ENERGIE_IRRIGATION / "eau.csv",).to_csv(
            CHEMIN_OUTPUT_DATA_ENERGIE / DOSSIER_ENERGIE_IRRIGATION / "energie_irrigation.csv",
            sep=";",
            float_format="%.2f",
        )
        # then
        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA_ENERGIE / DOSSIER_ENERGIE_IRRIGATION / "energie_irrigation.csv",
            CHEMIN_OUTPUT_DATA_ENERGIE / DOSSIER_ENERGIE_IRRIGATION / "energie_irrigation.csv",
        )


if __name__ == "__main__":
    unittest.main()
