from pathlib import Path

CHEMIN_INPUT_DATA_ENERGIE = Path("tests/energie/data/input")
CHEMIN_OUTPUT_DATA_ENERGIE = Path("tests/energie/data/output")
CHEMIN_EXPECTED_DATA_ENERGIE = Path("tests/energie/data/expected")
