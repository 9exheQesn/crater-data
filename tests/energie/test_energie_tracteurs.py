import unittest

from crater.commun.export_fichier import reinitialiser_dossier
from crater.energie.calculateur_energie_tracteurs import calculer_indicateurs_energie_tracteurs
from crater.territoires.chargeur_territoires import (
    charger_territoires,
)
from tests.commun.outils_pour_tests import (
    assert_csv_files_are_equals,
    augmenter_nombre_colonnes_affichees_pandas,
)
from tests.energie.config_test_energie import CHEMIN_INPUT_DATA_ENERGIE, CHEMIN_OUTPUT_DATA_ENERGIE, CHEMIN_EXPECTED_DATA_ENERGIE

augmenter_nombre_colonnes_affichees_pandas()

DOSSIER_ENERGIE_TRACTEURS = "tracteurs"


class TestEnergieTracteurs(unittest.TestCase):
    def test_calculer_indicateurs_energie_tracteurs(self):
        # given
        reinitialiser_dossier(CHEMIN_OUTPUT_DATA_ENERGIE / DOSSIER_ENERGIE_TRACTEURS)
        territoires = territoires = charger_territoires(
            CHEMIN_INPUT_DATA_ENERGIE / DOSSIER_ENERGIE_TRACTEURS / "territoires",
            CHEMIN_INPUT_DATA_ENERGIE / DOSSIER_ENERGIE_TRACTEURS / "territoires",
        )
        # when
        calculer_indicateurs_energie_tracteurs(
            territoires,
            CHEMIN_INPUT_DATA_ENERGIE / DOSSIER_ENERGIE_TRACTEURS / "sau_par_commune_et_culture",
            CHEMIN_INPUT_DATA_ENERGIE / DOSSIER_ENERGIE_TRACTEURS / "correspondance_nomenclature_rpg_vers_crater.csv",
        ).to_csv(
            CHEMIN_OUTPUT_DATA_ENERGIE / DOSSIER_ENERGIE_TRACTEURS / "energie_tracteurs.csv",
            sep=";",
            float_format="%.2f",
        )
        # then
        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA_ENERGIE / DOSSIER_ENERGIE_TRACTEURS / "energie_tracteurs.csv",
            CHEMIN_OUTPUT_DATA_ENERGIE / DOSSIER_ENERGIE_TRACTEURS / "energie_tracteurs.csv",
        )


if __name__ == "__main__":
    unittest.main()
