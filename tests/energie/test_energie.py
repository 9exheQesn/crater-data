import unittest

from crater.energie.calculateur_energie import calculer_indicateurs_energie
from crater.territoires.chargeur_territoires import (
    charger_territoires,
)
from tests.commun.outils_pour_tests import (
    assert_csv_files_are_equals,
    augmenter_nombre_colonnes_affichees_pandas,
)
from tests.energie.config_test_energie import CHEMIN_INPUT_DATA_ENERGIE, CHEMIN_OUTPUT_DATA_ENERGIE, CHEMIN_EXPECTED_DATA_ENERGIE

augmenter_nombre_colonnes_affichees_pandas()


class TestEnergie(unittest.TestCase):
    def test_calculer_indicateurs_energie(self):
        # given
        territoires = territoires = charger_territoires(CHEMIN_INPUT_DATA_ENERGIE / "territoires", CHEMIN_INPUT_DATA_ENERGIE / "territoires")
        # when
        calculer_indicateurs_energie(
            territoires,
            CHEMIN_INPUT_DATA_ENERGIE / "irrigation" / "eau.csv",
            CHEMIN_INPUT_DATA_ENERGIE / "tracteurs" / "sau_par_commune_et_culture",
            CHEMIN_INPUT_DATA_ENERGIE / "tracteurs" / "correspondance_nomenclature_rpg_vers_crater.csv",
            CHEMIN_INPUT_DATA_ENERGIE / "serres" / "synthese_etude_serres_ctifl_2016.xlsx",
            CHEMIN_INPUT_DATA_ENERGIE / "engrais_azotes" / "flux_azote.csv",
            CHEMIN_OUTPUT_DATA_ENERGIE,
        )
        # then
        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA_ENERGIE / "energie.csv",
            CHEMIN_OUTPUT_DATA_ENERGIE / "energie.csv",
        )


if __name__ == "__main__":
    unittest.main()
