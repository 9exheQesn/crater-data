import unittest

from crater.commun.export_fichier import reinitialiser_dossier
from crater.energie.calculateur_energie_serres import calculer_indicateurs_energie_serres
from crater.territoires.chargeur_territoires import (
    charger_territoires,
)
from tests.commun.outils_pour_tests import (
    assert_csv_files_are_equals,
    augmenter_nombre_colonnes_affichees_pandas,
)
from tests.energie.config_test_energie import CHEMIN_OUTPUT_DATA_ENERGIE, CHEMIN_EXPECTED_DATA_ENERGIE, CHEMIN_INPUT_DATA_ENERGIE

augmenter_nombre_colonnes_affichees_pandas()

DOSSIER_ENERGIE_CHAUFFAGE_SERRES = "serres"


class TestEnergieChauffageSerres(unittest.TestCase):
    def test_calculer_indicateurs_energie_serres(self):
        # given
        reinitialiser_dossier(CHEMIN_OUTPUT_DATA_ENERGIE / DOSSIER_ENERGIE_CHAUFFAGE_SERRES)
        territoires = territoires = charger_territoires(
            CHEMIN_INPUT_DATA_ENERGIE / DOSSIER_ENERGIE_CHAUFFAGE_SERRES / "territoires",
            CHEMIN_INPUT_DATA_ENERGIE / DOSSIER_ENERGIE_CHAUFFAGE_SERRES / "territoires",
        )
        # when
        calculer_indicateurs_energie_serres(
            territoires, CHEMIN_INPUT_DATA_ENERGIE / DOSSIER_ENERGIE_CHAUFFAGE_SERRES / "synthese_etude_serres_ctifl_2016.xlsx"
        ).to_csv(
            CHEMIN_OUTPUT_DATA_ENERGIE / DOSSIER_ENERGIE_CHAUFFAGE_SERRES / "energie_serres.csv",
            sep=";",
            float_format="%.2f",
        )
        # then
        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA_ENERGIE / DOSSIER_ENERGIE_CHAUFFAGE_SERRES / "energie_serres.csv",
            CHEMIN_OUTPUT_DATA_ENERGIE / DOSSIER_ENERGIE_CHAUFFAGE_SERRES / "energie_serres.csv",
        )


if __name__ == "__main__":
    unittest.main()
