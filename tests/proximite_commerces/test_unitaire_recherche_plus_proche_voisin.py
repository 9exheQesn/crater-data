import unittest
import geopandas
import numpy as np
import pandas

from crater.proximite_commerces.indicateurs_proximite_commerces.recherche_plus_proche_voisin import (
    rechercher_plus_proche_voisin_selon_distance_euclidienne,
)


class TestProximiteCommercesRecherchePlusProcheVoisin(unittest.TestCase):
    def setUp(self) -> None:
        # given
        points = pandas.DataFrame(
            columns=["nom_point", "x", "y"],
            data=[["A", 0, 0], ["B", 10, 10], ["C", 11, 11]],
            index=["a", "z", "C"],
        )
        self.gdf_points = geopandas.GeoDataFrame(
            points.loc[:, "nom_point"],
            geometry=geopandas.points_from_xy(points.x, points.y),
            crs="EPSG:2154",
        )

    def test_rechercher_plus_proche_voisin(self):
        # given
        voisins_potentiels = pandas.DataFrame(
            columns=["nom_voisin", "x", "y"],
            data=[
                ["voisinA", -4, -3],
                ["voisinBetC", 11, 10],
                ["pasVoisin1", 20, 10],
                ["pasVoisin2", 21, 10],
            ],
        )

        gdf_voisins_potentiels = geopandas.GeoDataFrame(
            voisins_potentiels.loc[:, "nom_voisin"],
            geometry=geopandas.points_from_xy(voisins_potentiels.x, voisins_potentiels.y),
            crs="EPSG:2154",
        )
        # when
        gdf_points_et_voisins = rechercher_plus_proche_voisin_selon_distance_euclidienne(self.gdf_points, gdf_voisins_potentiels)
        # then
        self.assertEqual(gdf_points_et_voisins.nom_point.tolist(), ["A", "B", "C"])
        self.assertEqual(
            gdf_points_et_voisins.nom_voisin.tolist(),
            ["voisinA", "voisinBetC", "voisinBetC"],
        )
        self.assertEqual(gdf_points_et_voisins.distance_m.tolist(), [5, 1, 1])

    def test_rechercher_plus_proche_voisin_cas_retour_vide(self):
        # given
        aucun_voisins_potentiels = pandas.DataFrame(columns=["nom_voisin", "x", "y"], data=[])
        gdf_aucun_voisins_potentiels = geopandas.GeoDataFrame(
            aucun_voisins_potentiels.loc[:, "nom_voisin"],
            geometry=geopandas.points_from_xy(aucun_voisins_potentiels.x, aucun_voisins_potentiels.y),
            crs="EPSG:2154",
        )
        # when
        gdf_points_et_voisins = rechercher_plus_proche_voisin_selon_distance_euclidienne(self.gdf_points, gdf_aucun_voisins_potentiels)
        # then
        self.assertEqual(gdf_points_et_voisins.nom_point.tolist(), ["A", "B", "C"])
        np.testing.assert_equal(gdf_points_et_voisins.nom_voisin.tolist(), [np.nan, np.nan, np.nan])
        np.testing.assert_equal(gdf_points_et_voisins.distance_m.tolist(), [np.nan, np.nan, np.nan])

    def test_rechercher_plus_proche_voisin_cas_erreur_crs(self):
        # given
        voisins_potentiels = pandas.DataFrame(
            columns=["nom_voisin", "x", "y"],
            data=[
                ["voisinA", -4, -3],
                ["voisinBetC", 11, 10],
                ["pasVoisin1", 20, 10],
                ["pasVoisin2", 21, 10],
            ],
        )

        gdf_voisins_sans_crs = geopandas.GeoDataFrame(
            voisins_potentiels.loc[:, "nom_voisin"],
            geometry=geopandas.points_from_xy(voisins_potentiels.x, voisins_potentiels.y),
        )
        # when
        # then
        with self.assertRaises(ValueError):
            rechercher_plus_proche_voisin_selon_distance_euclidienne(self.gdf_points, gdf_voisins_sans_crs)


if __name__ == "__main__":
    unittest.main()
