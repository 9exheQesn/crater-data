import unittest
from pathlib import Path

import numpy as np
import pandas as pd

from crater.commun.config import (
    CLASSIFICATIONS_SUBSTANCES_RETENUES_DANGEREUSES_POUR_LA_SANTE,
    CLASSIFICATIONS_SUBSTANCES_RETENUES_TOUTES_HORS_AUTRE,
    CLASSIFICATIONS_SUBSTANCES_RETENUES_CMR,
    CLASSIFICATIONS_SUBSTANCES_RETENUES_TOUTES,
)
from crater.intrants_agricoles.chargeur_donnees_pesticides import (
    _extraire_fichiers_a_charger,
)
from crater.intrants_agricoles.pesticides import (
    _nettoyer_noms_substances,
    _calculer_repartition_codes_postaux_dans_communes,
    _ajouter_colonne_substance_retenue,
)
from crater.intrants_agricoles.calculateur_intrants_agricoles import _ajouter_note

from tests.commun.outils_pour_tests import augmenter_nombre_colonnes_affichees_pandas

augmenter_nombre_colonnes_affichees_pandas()


class TestUnitaireIntrantsAgricoles(unittest.TestCase):
    def test_nettoyer_noms_substances(self):
        # given
        df_input = pd.DataFrame(columns=["substance"], data=[["SubSTANCE"], ["Substance_é_è_ê_à"]])
        resultat_attendu = pd.Series(data=["substance", "substance_e_e_e_a"], dtype="str")
        # when
        df_resultat = _nettoyer_noms_substances(df_input)
        # then
        np.testing.assert_array_equal(resultat_attendu.tolist(), df_resultat["substance"].tolist())
        self.assertEqual(resultat_attendu.dtype, df_resultat["substance"].dtype)

    def test_extraire_fichiers_a_charger(self):
        # given
        fichiers_input = [
            Path("XXXXXX.csv"),
            Path("BNVD_2020_ACHAT_FR_SUBSTANCE_2019.csv"),
            Path("BNVD_2020_VENTE_FR_SUBSTANCE_2019.csv"),
            Path("BNVD_2020_ACHAT_DPT_SUBSTANCE_2019.csv"),
            Path("BNVD_2020_ACHAT_CP_SUBSTANCE_ILE DE FRANCE_2019.csv"),
            Path("BNVD_2020_ACHAT_CP_SUBSTANCE_INDETERMINE_2019.csv"),
            Path("BNVD_TRACABILITE_20211213_ACHAT_CP_SUBSTANCE_NOUVELLE AQUITAINE_2015.csv"),
        ]
        # when
        df_resultat = _extraire_fichiers_a_charger(fichiers_input)
        # then
        self.assertEqual(
            df_resultat,
            [
                {
                    "chemin_fichier_substance": Path("BNVD_2020_ACHAT_FR_SUBSTANCE_2019.csv"),
                    "fichier_source": "ACHAT_FR",
                },
                {
                    "chemin_fichier_substance": Path("BNVD_2020_VENTE_FR_SUBSTANCE_2019.csv"),
                    "fichier_source": "VENTE_FR",
                },
                {
                    "chemin_fichier_substance": Path("BNVD_2020_ACHAT_DPT_SUBSTANCE_2019.csv"),
                    "fichier_source": "ACHAT_DPT",
                },
                {
                    "chemin_fichier_substance": Path("BNVD_2020_ACHAT_CP_SUBSTANCE_ILE DE FRANCE_2019.csv"),
                    "fichier_source": "ACHAT_ILE DE FRANCE",
                },
                {
                    "chemin_fichier_substance": Path("BNVD_2020_ACHAT_CP_SUBSTANCE_INDETERMINE_2019.csv"),
                    "fichier_source": "ACHAT_INDETERMINE",
                },
                {
                    "chemin_fichier_substance": Path("BNVD_TRACABILITE_20211213_ACHAT_CP_SUBSTANCE_NOUVELLE AQUITAINE_2015.csv"),
                    "fichier_source": "ACHAT_NOUVELLE AQUITAINE",
                },
            ],
        )

    def test_calculer_repartition_codes_postaux_dans_communes(self):
        # given
        territoires_input = pd.DataFrame(
            columns=[
                "id_territoire",
                "categorie_territoire",
                "codes_postaux_commune",
                "sau_ra_2020_ha",
            ],
            data=[
                ["Region", "REGION", np.nan, 100],
                ["commune sans code postal", "COMMUNE", np.nan, 100],
                ["commune avec code postal unique", "COMMUNE", "A", 100],
                ["commune avec 2 codes postaux", "COMMUNE", "B|C", 100],
                ["commune 1 d'un meme code postal", "COMMUNE", "D", 60],
                ["commune 2 d'un meme code postal", "COMMUNE", "D", 40],
                [
                    "commune 3 dans 2 codes postaux dont un partagé",
                    "COMMUNE",
                    "E|F",
                    100,
                ],
                [
                    "commune 4 dans 2 codes postaux dont un partagé",
                    "COMMUNE",
                    "F|G",
                    300,
                ],
            ],
        ).set_index("id_territoire")
        df_resultat_attendu = pd.DataFrame(
            columns=["id_territoire", "code_postal_commune", "cle_repartition"],
            data=[
                ["commune avec code postal unique", "A", 1.0],
                ["commune avec 2 codes postaux", "B", 1.0],
                ["commune avec 2 codes postaux", "C", 1.0],
                ["commune 1 d'un meme code postal", "D", 0.6],
                ["commune 2 d'un meme code postal", "D", 0.4],
                ["commune 3 dans 2 codes postaux dont un partagé", "E", 1.0],
                ["commune 3 dans 2 codes postaux dont un partagé", "F", 0.25],
                ["commune 4 dans 2 codes postaux dont un partagé", "F", 0.75],
                ["commune 4 dans 2 codes postaux dont un partagé", "G", 1.0],
            ],
        )
        # when
        df_resultat = _calculer_repartition_codes_postaux_dans_communes(territoires_input).drop(columns="sau_ra_2020_ha")
        # then
        pd.testing.assert_frame_equal(df_resultat, df_resultat_attendu)
        self.assertEqual(
            (df_resultat_attendu.groupby("code_postal_commune").sum()["cle_repartition"] == 1.0).all(),
            True,
        )

    def test_regex_extraction_classification_substances(self):
        # given
        df_input = pd.DataFrame(
            columns=["classification"],
            data=[
                ["Autre"],
                ["T, T+, CMR"],
                ["CMR"],
                ["Santé A"],
                ["Env A"],
                ["Env B"],
                ["N Organique"],
                ["N minéral"],
            ],
        )

        np.testing.assert_array_equal(
            _ajouter_colonne_substance_retenue(df_input, CLASSIFICATIONS_SUBSTANCES_RETENUES_TOUTES_HORS_AUTRE)
            .query("substance_retenue")["classification"]
            .tolist(),
            [
                "T, T+, CMR",
                "CMR",
                "Santé A",
                "Env A",
                "Env B",
                "N Organique",
                "N minéral",
            ],
        )
        np.testing.assert_array_equal(
            _ajouter_colonne_substance_retenue(df_input, CLASSIFICATIONS_SUBSTANCES_RETENUES_TOUTES)
            .query("substance_retenue")["classification"]
            .tolist(),
            [
                "Autre",
                "T, T+, CMR",
                "CMR",
                "Santé A",
                "Env A",
                "Env B",
                "N Organique",
                "N minéral",
            ],
        )
        np.testing.assert_array_equal(
            _ajouter_colonne_substance_retenue(df_input, CLASSIFICATIONS_SUBSTANCES_RETENUES_CMR)
            .query("substance_retenue")["classification"]
            .tolist(),
            ["T, T+, CMR", "CMR"],
        )
        np.testing.assert_array_equal(
            _ajouter_colonne_substance_retenue(df_input, CLASSIFICATIONS_SUBSTANCES_RETENUES_DANGEREUSES_POUR_LA_SANTE)
            .query("substance_retenue")["classification"]
            .tolist(),
            ["T, T+, CMR", "CMR", "Santé A"],
        )


def test_note(self):
    # given
    df_intrants = pd.DataFrame(
        columns=[
            "id_territoire",
            "NODU_normalise",
        ],
        data=[
            ["C-1", np.nan],
            ["C-2", 0],
            ["C-3", 0.2],
            ["C-4", 0.5],
            ["C-5", 1.0],
            ["C-6", 1.5],
        ],
    ).set_index("id_territoire")
    resultat_attendu = pd.DataFrame(
        columns=["note"],
        data=[
            [np.nan],
            [10],
            [8],
            [5],
            [0],
            [0],
        ],
    ).astype({"note": "Int64"})
    # when
    df_resultat = _ajouter_note(df_intrants)
    df_resultat = df_resultat.reset_index()
    # then
    pd.testing.assert_series_equal(
        df_resultat["note"],
        resultat_attendu["note"],
        check_names=False,
    )


if __name__ == "__main__":
    unittest.main()
