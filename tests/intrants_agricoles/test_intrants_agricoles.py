import unittest
from pathlib import Path

from crater.intrants_agricoles.calculateur_intrants_agricoles import (
    calculer_intrants_agricoles,
)
from crater.territoires.chargeur_territoires import (
    charger_territoires,
)
from tests.commun.outils_pour_tests import (
    assert_csv_files_are_equals,
    augmenter_nombre_colonnes_affichees_pandas,
)

augmenter_nombre_colonnes_affichees_pandas()


CHEMIN_INPUT_DATA = Path("tests/intrants_agricoles/data/input")
CHEMIN_OUTPUT_DATA = Path("tests/intrants_agricoles/data/output")
CHEMIN_EXPECTED_DATA = Path("tests/intrants_agricoles/data/expected")

CLASSIFICATIONS_SUBSTANCES_RETENUES_CMR = "CMR"  # regex


class TestIntrantsAgricoles(unittest.TestCase):
    def test_intrants_agricoles(self):
        # given
        territoires = territoires = charger_territoires(CHEMIN_INPUT_DATA / "territoires", CHEMIN_INPUT_DATA / "territoires")
        # when
        calculer_intrants_agricoles(
            territoires,
            CLASSIFICATIONS_SUBSTANCES_RETENUES_CMR,
            [
                CHEMIN_INPUT_DATA / "bnvd" / "BNVD_2020_ACHAT_2016",
                CHEMIN_INPUT_DATA / "bnvd" / "BNVD_2020_ACHAT_2017",
                CHEMIN_INPUT_DATA / "bnvd" / "BNVD_2020_ACHAT_2018",
                CHEMIN_INPUT_DATA / "bnvd" / "BNVD_2020_ACHAT_2019",
                CHEMIN_INPUT_DATA / "bnvd" / "BNVD_2020_VENTE_2019",
            ],
            CHEMIN_INPUT_DATA / "DU_2016_2019.xlsx",
            CHEMIN_INPUT_DATA / "extrait_arrete_2017_annexe5.csv",
            CHEMIN_INPUT_DATA / "produits_usages_v3_utf8.csv",
            CHEMIN_OUTPUT_DATA,
        )
        # then
        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA / "intrants_agricoles_moyennes_triennales.csv",
            CHEMIN_OUTPUT_DATA / "intrants_agricoles_moyennes_triennales.csv",
        )
        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA / "intrants_agricoles_toutes_annees.csv",
            CHEMIN_OUTPUT_DATA / "intrants_agricoles_toutes_annees.csv",
        )
        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA / "intrants_agricoles.csv",
            CHEMIN_OUTPUT_DATA / "intrants_agricoles.csv",
        )


if __name__ == "__main__":
    unittest.main()
