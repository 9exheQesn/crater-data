**Caracteristiques des fichiers tests**

DU : substances = {glyphosate, CMR}, {sucre, Autre}

BNVd :
+ teste différentes années (2018 et 2019, un fichier France)
+ teste TOUS les 3 types de fichiers : France, région indeterminée, 2 fichiers régions
+ teste TOUTES les possibilités de CP
+ tests différentes classifications

| Nom                                                      | Annee | Géographie  | Code postal  | Substances |
|----------------------------------------------------------|-------|-------------|--------------|------------|
| BNVD_2020_ACHAT_FR_SUBSTANCE_2018.csv                    | 2018  | FRANCE      | Aucun        | CMR        |
| BNVD_2020_ACHAT_FR_SUBSTANCE_2019.csv                    | 2019  | FRANCE      | Aucun        | CMR, Autre |
| BNVD_2020_ACHAT_CP_SUBSTANCE_ILE DE FRANCE_2019.csv      | idem  | REGION      | br           | Autre      |
| BNVD_2020_ACHAT_CP_SUBSTANCE_NOUVELLE AQUITAINE_2019.csv | idem  | REGION      | br, mr, brnc | CMR, nc    |
| BNVD_2020_ACHAT_CP_SUBSTANCE_INDETERMINE_2019.csv        | idem  | INDETERMINE | 00000        | CMR        |