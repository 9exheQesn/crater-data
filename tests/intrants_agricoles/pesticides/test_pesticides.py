import unittest
from pathlib import Path

from crater.intrants_agricoles.pesticides import _calculer_et_exporter_donnees_du
from tests.commun.outils_pour_tests import (
    assert_csv_files_are_equals,
    augmenter_nombre_colonnes_affichees_pandas,
)

augmenter_nombre_colonnes_affichees_pandas()


CHEMIN_INPUT_DATA = Path("tests/intrants_agricoles/pesticides/data/input")
CHEMIN_OUTPUT_DATA = Path("tests/intrants_agricoles/pesticides/data/output")
CHEMIN_EXPECTED_DATA = Path("tests/intrants_agricoles/pesticides/data/expected")

CLASSIFICATIONS_SUBSTANCES_RETENUES_CMR = "CMR"  # regex


class TestPesticides(unittest.TestCase):
    def test_calculer_du(self):
        # when
        _calculer_et_exporter_donnees_du(
            CHEMIN_INPUT_DATA / "DU_2016_2019.xlsx",
            CHEMIN_INPUT_DATA / "extrait_arrete_2017_annexe5.csv",
            CHEMIN_OUTPUT_DATA,
        )
        # then
        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA / "doses_unites.csv",
            CHEMIN_OUTPUT_DATA / "doses_unites.csv",
        )


if __name__ == "__main__":
    unittest.main()
