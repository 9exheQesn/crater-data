import unittest
from pathlib import Path

from crater.politique_fonciere.calculateur_politique_fonciere import (
    calculer_politique_fonciere,
)
from crater.territoires.chargeur_territoires import (
    charger_territoires,
)
from tests.commun.outils_pour_tests import (
    assert_csv_files_are_equals,
    augmenter_nombre_colonnes_affichees_pandas,
)

augmenter_nombre_colonnes_affichees_pandas()

CHEMIN_INPUT_DATA = Path("tests/politique_fonciere/data/input")
CHEMIN_OUTPUT_DATA = Path("tests/politique_fonciere/data/output")
CHEMIN_EXPECTED_DATA = Path("tests/politique_fonciere/data/expected")


class TestPolitiqueFonciere(unittest.TestCase):
    def test_politique_fonciere(self):
        # given
        territoires = charger_territoires(CHEMIN_INPUT_DATA / "territoires", CHEMIN_INPUT_DATA / "territoires")
        # when
        calculer_politique_fonciere(
            territoires,
            CHEMIN_INPUT_DATA / "artificialisation_2009_2018.csv",
            CHEMIN_INPUT_DATA / "nombre_de_logements.xlsx",
            CHEMIN_INPUT_DATA / "nombre_de_logements_vacants.xlsx",
            CHEMIN_OUTPUT_DATA,
        )
        # then
        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA / "politique_fonciere.csv",
            CHEMIN_OUTPUT_DATA / "politique_fonciere.csv",
        )


if __name__ == "__main__":
    unittest.main()
