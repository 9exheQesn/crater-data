import unittest
from pathlib import Path

from crater.population_agricole.calculateur_population_agricole import (
    calculer_population_agricole,
)
from crater.territoires.chargeur_territoires import (
    charger_territoires,
)

from tests.commun.outils_pour_tests import (
    assert_csv_files_are_equals,
    augmenter_nombre_colonnes_affichees_pandas,
)

augmenter_nombre_colonnes_affichees_pandas()

CHEMIN_INPUT_DATA = Path("tests/population_agricole/data/input")
CHEMIN_OUTPUT_DATA = Path("tests/population_agricole/data/output")
CHEMIN_EXPECTED_DATA = Path("tests/population_agricole/data/expected")


class TestPopulationAgricole(unittest.TestCase):
    def test_population_agricole(self):
        # given
        territoires = territoires = charger_territoires(CHEMIN_INPUT_DATA / "territoires", CHEMIN_INPUT_DATA / "territoires")
        # when
        calculer_population_agricole(
            territoires,
            CHEMIN_INPUT_DATA,
            CHEMIN_OUTPUT_DATA,
        )
        # then
        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA / "population_agricole.csv",
            CHEMIN_OUTPUT_DATA / "population_agricole.csv",
        )


if __name__ == "__main__":
    unittest.main()
