import unittest
from pathlib import Path

from crater.surface_agricole_utile.calculateur_surface_agricole_utile import (
    calculer_surface_agricole_utile_par_commune_et_culture,
)
from crater.territoires.calculateur_referentiel_territoires import (
    charger_referentiel_territoires,
)
from tests.commun.outils_pour_tests import (
    assert_csv_files_are_equals,
    augmenter_nombre_colonnes_affichees_pandas,
)

augmenter_nombre_colonnes_affichees_pandas()

CHEMIN_INPUT_DATA = Path("tests/surface_agricole_utile/data/input")
CHEMIN_OUTPUT_DATA = Path("tests/surface_agricole_utile/data/output")
CHEMIN_EXPECTED_DATA = Path("tests/surface_agricole_utile/data/expected")
NOMS_FICHIERS_RPG_INPUT = {
    2017: {
        "R-11": "RPG_R-53.7z",
        "R-53": "RPG_R-53.7z",
        "R-75": "RPG_R-53.7z",
        "R-76": "RPG_R-53.7z",
    }
}


class TestSurfaceAgricoleUtile(unittest.TestCase):
    def test_calculer_surface_agricole_utile_par_commune_et_culture(self):
        referentiel_territoires = charger_referentiel_territoires(CHEMIN_INPUT_DATA)
        calculer_surface_agricole_utile_par_commune_et_culture(
            2017,
            referentiel_territoires,
            CHEMIN_INPUT_DATA / "geometries_communes_test",
            CHEMIN_INPUT_DATA / "rpg",
            NOMS_FICHIERS_RPG_INPUT,
            CHEMIN_OUTPUT_DATA,
            1,
        )
        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA / "sau_par_commune_et_culture" / "sau_rpg_communes_R-53.csv",
            CHEMIN_OUTPUT_DATA / "sau_par_commune_et_culture" / "sau_rpg_communes_R-53.csv",
        )
        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA / "sau_par_commune_et_culture" / "sau_rpg_communes_R-11.csv",
            CHEMIN_OUTPUT_DATA / "sau_par_commune_et_culture" / "sau_rpg_communes_R-11.csv",
        )
        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA / "sau_par_commune_et_culture" / "sau_rpg_communes_R-75.csv",
            CHEMIN_OUTPUT_DATA / "sau_par_commune_et_culture" / "sau_rpg_communes_R-75.csv",
        )
        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA / "sau_par_commune_et_culture" / "sau_rpg_communes_R-76.csv",
            CHEMIN_OUTPUT_DATA / "sau_par_commune_et_culture" / "sau_rpg_communes_R-76.csv",
        )
