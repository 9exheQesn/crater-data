import unittest
from pathlib import Path

from crater.consommation.calculateur_taux_pauvrete import calculer_indicateur_taux_pauvrete
from crater.territoires.chargeur_territoires import (
    charger_territoires,
)
from tests.commun.outils_pour_tests import (
    assert_csv_files_are_equals,
    augmenter_nombre_colonnes_affichees_pandas,
)

augmenter_nombre_colonnes_affichees_pandas()

CHEMIN_INPUT_DATA = Path("tests/consommation/data/input")
CHEMIN_OUTPUT_DATA = Path("tests/consommation/data/output")
CHEMIN_EXPECTED_DATA = Path("tests/consommation/data/expected")


class TestConsommation(unittest.TestCase):
    def test_calcul_taux_pauvrete(self):
        # given
        territoires = charger_territoires(CHEMIN_INPUT_DATA / "territoires", CHEMIN_INPUT_DATA / "territoires")
        # when
        calculer_indicateur_taux_pauvrete(
            territoires,
            CHEMIN_INPUT_DATA / "insee" / "filosofi" / "indic-struct-distrib-revenu-2019-COMMUNES_csv",
            CHEMIN_INPUT_DATA / "insee" / "filosofi" / "indic-struct-distrib-revenu-2019-SUPRA_csv",
            CHEMIN_OUTPUT_DATA,
        )
        # then
        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA / "consommation.csv",
            CHEMIN_OUTPUT_DATA / "consommation.csv",
        )


if __name__ == "__main__":
    unittest.main()
