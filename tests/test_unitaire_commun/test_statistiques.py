import unittest

import numpy as np
import pandas as pd

from crater.commun.statistiques import calculer_statistiques


class TestsUnitairesStatistiques(unittest.TestCase):
    def test_calculer_statistiques_colonnes_number_uniquement(self):
        # given
        df = pd.DataFrame(
            columns=[
                "id_territoire",
                "nom_territoire",
                "categorie_territoire",
                "ind1",
                "ind2",
                "ind3",
            ],
            data=[
                ["P-FR", "France", "PAYS", 1, np.nan, "A"],
                ["C-1", "Commune1", "COMMUNE", 100, 10, "B"],
                ["C-2", "Commune2", "COMMUNE", np.nan, 15, "C"],
                ["C-3", "Commune3", "COMMUNE", 150, 20, "C"],
            ],
        )
        # when
        df_stats = calculer_statistiques(df)
        # then
        df_attendu = pd.DataFrame(
            columns=[
                "categorie_territoire",
                "indicateur",
                "nb_lignes",
                "nb_nan",
                "moy",
                "min",
                "q1",
                "q2",
                "q3",
                "max",
            ],
            data=[
                ["PAYS", "ind1", 1, 0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0],
                ["PAYS", "ind2", 1, 1, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan],
                ["COMMUNE", "ind1", 3, 1, 125, 100, 112.5, 125, 137.5, 150],
                ["COMMUNE", "ind2", 3, 0, 15, 10, 12.5, 15, 17.5, 20],
            ],
        )
        pd.testing.assert_frame_equal(df_stats, df_attendu)

    def test_calculer_statistiques_indicateurs_par_annee(self):
        # given
        df = pd.DataFrame(
            columns=[
                "id_territoire",
                "nom_territoire",
                "categorie_territoire",
                "annee",
                "ind1",
                "ind2",
            ],
            data=[
                ["C-1", "Commune1", "COMMUNE", 2021, 100, 10],
                ["C-2", "Commune2", "COMMUNE", 2021, 55, 5],
                ["C-2", "Commune2", "COMMUNE", 2020, 500, 1],
            ],
        )
        # when
        df_stats = calculer_statistiques(df, "annee")
        df_attendu = pd.DataFrame(
            columns=[
                "categorie_territoire",
                "indicateur",
                "annee",
                "nb_lignes",
                "nb_nan",
                "moy",
                "min",
                "q1",
                "q2",
                "q3",
                "max",
            ],
            data=[
                ["COMMUNE", "ind1", 2021, 2, 0, 77.5, 55, 66.25, 77.5, 88.75, 100],
                ["COMMUNE", "ind1", 2020, 1, 0, 500, 500, 500, 500, 500, 500],
                ["COMMUNE", "ind2", 2021, 2, 0, 7.5, 5, 6.25, 7.5, 8.75, 10],
                ["COMMUNE", "ind2", 2020, 1, 0, 1, 1, 1, 1, 1, 1],
            ],
        )
        # then
        pd.testing.assert_frame_equal(df_stats.reset_index(drop=True), df_attendu.reset_index(drop=True))


if __name__ == "__main__":
    unittest.main()
