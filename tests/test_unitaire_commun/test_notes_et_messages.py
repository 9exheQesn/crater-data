import unittest

import numpy as np
import pandas as pd

from crater.commun.notes_et_messages import (
    calculer_note_par_interpolation_2_seuils,
    calculer_note_par_interpolation_3_seuils,
    discretiser_indicateur,
)


class TestsUnitairesNotesEtMessages(unittest.TestCase):
    def test_calculer_note_par_interpolation_2_seuils_cas_coef_directeur_positif(self):
        df = pd.DataFrame(
            columns=["indicateur"],
            data=[
                [-1],
                [6],
                [8],
                [12],
                [17],
                [np.nan],
            ],
        )
        # given
        df_attendu = pd.DataFrame(
            columns=["note"],
            data=[
                [0],
                [3.75],
                [5],
                [7.5],
                [10],
                [np.nan],
            ],
        )
        # when
        df.loc[:, "note"] = calculer_note_par_interpolation_2_seuils(df["indicateur"], 0, 0, 8, 5)
        # then
        pd.testing.assert_series_equal(df["note"], df_attendu["note"], check_names=False)

    def test_calculer_note_par_interpolation_2_seuils_cas_coef_directeur_negatif(self):
        df = pd.DataFrame(
            columns=["indicateur"],
            data=[
                [9],
                [8],
                [4],
                [0],
                [-1],
                [np.nan],
            ],
        )
        # given
        df_attendu = pd.DataFrame(
            columns=["note"],
            data=[
                [0],
                [0],
                [5],
                [10],
                [10],
                [np.nan],
            ],
        )
        # when
        df.loc[:, "note"] = calculer_note_par_interpolation_2_seuils(df["indicateur"], 0, 10, 8, 0)

        # then
        pd.testing.assert_series_equal(df["note"], df_attendu["note"], check_names=False)

    def test_calculer_note_par_interpolation_3_seuils(self):
        df = pd.DataFrame(
            columns=["indicateur"],
            data=[
                [-1],
                [0],
                [3],
                [8],
                [13],
                [np.nan],
            ],
        )
        # given
        df_attendu = pd.DataFrame(
            columns=["note"],
            data=[
                [0],
                [0],
                [5],
                [7.5],
                [10],
                [np.nan],
            ],
        )
        # when
        df.loc[:, "note"] = calculer_note_par_interpolation_3_seuils(df["indicateur"], 0, 0, 3, 5, 13, 10)
        # then
        pd.testing.assert_series_equal(df["note"], df_attendu["note"], check_names=False)

    def test_discretiser_indicateur_vers_classes_string(self):
        # given
        df = pd.DataFrame(columns=["indicateur"], data=[0, 2, 4, 1, 3, np.nan])

        # when
        df["message"] = discretiser_indicateur(
            df["indicateur"],
            [1, 3],
            ["Inferieur à 1", "Entre 1 et 3", "Superieur a 3"],
            pd.StringDtype(),
            "Données non disponible",
        )

        # then
        resultat_attendu = pd.Series(
            data=[
                "Inferieur à 1",
                "Entre 1 et 3",
                "Superieur a 3",
                "Entre 1 et 3",
                "Superieur a 3",
                "Données non disponible",
            ],
            dtype=pd.StringDtype(),
        )

        self.assertEqual(resultat_attendu.tolist(), df["message"].tolist())
        self.assertEqual(resultat_attendu.dtype, df["message"].dtype)

    def test_discretiser_indicateur_vers_classes_int64(self):
        # given
        df = pd.DataFrame(columns=["indicateur"], data=[0, 2, 4, 1, 3, np.nan])

        # when
        df["valeur_message"] = discretiser_indicateur(df["indicateur"], [1, 3], [0, 6, 10], pd.Int64Dtype(), -1)

        # then
        resultat_attendu = pd.Series(data=[0, 6, 10, 6, 10, -1], dtype=pd.Int64Dtype())

        self.assertEqual(resultat_attendu.tolist(), df["valeur_message"].tolist())
        self.assertEqual(resultat_attendu.dtype, df["valeur_message"].dtype)


if __name__ == "__main__":
    unittest.main()
