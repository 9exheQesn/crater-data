import unittest

import numpy as np
import pandas as pd

from crater.commun.calcul_indicateurs import calculer_moyenne_glissante


class TestsUnitairesCalculIndicateurs(unittest.TestCase):
    def test_calculer_moyenne_glissante(self):
        # given
        # Donnees volontairement dans un ordre non croissant d'années pour valider la bonne prise en compte dans ce cas
        # avec une seule année disponible pour R-12
        df = pd.DataFrame(
            data=[
                ["P-FR", 2015, 103, 200],
                ["P-FR", 2018, 100, np.nan],
                ["P-FR", 2016, 101, np.nan],
                ["P-FR", 2017, 102, np.nan],
                ["R-11", 2018, 10, 20],
                ["R-11", 2016, 11, 21],
                ["R-11", 2017, 12, 22],
                ["R-11", 2015, 13, 23],
                ["R-12", 2015, np.nan, np.nan],
            ],
            columns={
                "id_territoire": "str",
                "annee": "int64",
                "qte": "float64",
                "NODU": "float64",
            },
        ).set_index(["id_territoire", "annee"])
        df_resultat_attendu = pd.DataFrame(
            data=[
                ["P-FR", 2017, 102, 200],
                ["P-FR", 2018, 101, np.nan],
                ["R-11", 2017, 12, 22],
                ["R-11", 2018, 11, 21],
                ["R-12", 2017, np.nan, np.nan],
                ["R-12", 2018, np.nan, np.nan],
            ],
            columns={
                "id_territoire": "str",
                "annee": "int64",
                "qte": "float64",
                "NODU": "float64",
            },
        ).set_index(["id_territoire", "annee"])
        # when
        df_resultat = calculer_moyenne_glissante(df, "annee", 3)
        # then
        pd.testing.assert_frame_equal(df_resultat, df_resultat_attendu)


if __name__ == "__main__":
    unittest.main()
