import unittest

import geopandas as gpd
import numpy as np
import pandas as pd

from crater.commun.outils_verification import (
    verifier_coherence_referentiel_avec_geometries_communes,
    verifier_absence_doublons,
    verifier_colonne2_unique_par_colonne1,
    verifier_absence_na,
    verifier_nommage_ids_territoires,
)
from tests.commun.outils_pour_tests import augmenter_nombre_colonnes_affichees_pandas

augmenter_nombre_colonnes_affichees_pandas()


class TestsUnitairesOutilsVerification(unittest.TestCase):
    def test_verifier_coherence_referentiel_avec_geometries_communes_ok(self):
        # given
        referentiel_territoires = pd.DataFrame(
            columns=["id_territoire", "categorie_territoire"],
            data=[["C-1", "COMMUNE"], ["C-2", "COMMUNE"], ["D-1", "DEPARTEMENT"]],
        )
        geometries_communes = gpd.GeoDataFrame(columns=["id_commune"], data=[["C-1"], ["C-2"]])
        # then
        verifier_coherence_referentiel_avec_geometries_communes(referentiel_territoires, geometries_communes)

    def test_verifier_coherence_referentiel_avec_geometries_communes_ko(self):
        # given
        referentiel_territoires = pd.DataFrame(
            columns=["id_territoire", "categorie_territoire"],
            data=[["C-1", "COMMUNE"], ["C-2", "COMMUNE"], ["D-1", "DEPARTEMENT"]],
        )
        geometries_communes = gpd.GeoDataFrame(columns=["id_commune"], data=[["C-1"], ["C-3"]])
        # then
        with self.assertRaises(Exception):
            verifier_coherence_referentiel_avec_geometries_communes(referentiel_territoires, geometries_communes)

    def test_verifier_absence_id_doublons_ok(self):
        # given
        df = pd.DataFrame(columns=["id"], data=[["1"], ["2"], ["3"], ["4"]])
        # then
        verifier_absence_doublons(df, "id")

    def test_verifier_absence_id_doublons_ko(self):
        # given
        df = pd.DataFrame(columns=["id"], data=[["1"], ["2"], ["2"]])
        # then
        with self.assertRaises(Exception):
            verifier_absence_doublons(df, "id")

    def test_verifier_colonne2_unique_par_colonne1_ok(self):
        # given
        df1 = pd.DataFrame(columns=["id", "nom"], data=[["1", "A"], ["1", "A"]])
        df2 = pd.DataFrame(columns=["id", "nom"], data=[["1", "A"], ["2", "A"]])
        df3 = pd.DataFrame(columns=["id", "nom"], data=[["1", "A"], ["1", "B"]])
        # then
        verifier_colonne2_unique_par_colonne1(df1, "id", "nom")
        verifier_colonne2_unique_par_colonne1(df2, "id", "nom")
        verifier_colonne2_unique_par_colonne1(df3, "id", "nom", warning_only=True)

    def test_verifier_colonne2_unique_par_colonne1_ko(self):
        # given
        df = pd.DataFrame(columns=["id", "nom"], data=[["1", "A"], ["1", "B"]])
        # then
        with self.assertRaises(Exception):
            verifier_colonne2_unique_par_colonne1(df, "id", "nom")

    def test_verifier_absence_na_ok(self):
        # given
        df = pd.DataFrame(columns=["id_territoire", "colonne"], data=[["C-1", "1"], ["C-2", "1"]])
        # then
        verifier_absence_na(df, "id_territoire", "colonne")

    def test_verifier_absence_na_ko(self):
        # given
        df = pd.DataFrame(columns=["id_territoire", "colonne"], data=[["C-1", "1"], ["C-2", np.nan]])
        # then
        with self.assertRaises(Exception):
            verifier_absence_na(df, "id_territoire", "colonne")

    def test_verifier_nommage_ids_territoires_ok(self):
        # given
        df = pd.DataFrame(columns=["id_territoire"], data=[["nom_sans_espaces"]])
        # then
        verifier_nommage_ids_territoires(df, "id_territoire")

    def test_verifier_nommage_ids_territoires_ko(self):
        # given
        df = pd.DataFrame(
            columns=["id_territoire"],
            data=[
                ["nom_sans_espaces"],
                ["nom avec_1_espace"],
                ["nom avec plusieurs espaces"],
            ],
        )
        # then
        with self.assertRaises(Exception):
            verifier_nommage_ids_territoires(df, "id_territoire")


if __name__ == "__main__":
    unittest.main()
