from pathlib import Path
import os as os
import pandas as pd
from pandas import DataFrame

from crater.commun.logger import log


def charger(fichier_source: Path) -> DataFrame:

    log.info("    => Chargement PAUVRETE A 60 pourcent depuis %s", fichier_source)

    df = (
        pd.read_csv(
            fichier_source,
            sep=";",
            encoding="utf-8",
            dtype={
                "CODGEO": "str",
                "TP6019": "float",
            },
        )
        .rename(
            columns={
                "CODGEO": "id_territoire",
                "TP6019": "taux_pauvrete_60_pourcent",
            },
            errors="raise",
        )
        .reindex(
            columns=[
                "id_territoire",
                "taux_pauvrete_60_pourcent",
            ]
        )
    )
    return df
