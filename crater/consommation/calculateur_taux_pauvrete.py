from pathlib import Path

import pandas
import pandas as pd
from pandas import DataFrame

from crater.commun.config import (
    NOM_FICHIERS_TAUX_PAUVRETE,
    ID_FRANCE,
    ANNEE_REFERENTIEL_TERRITOIRE_TAUX_PAUVRETE,
)
from crater.commun.export_fichier import (
    reinitialiser_dossier,
    exporter_df_indicateurs_par_territoires,
)
from crater.commun.logger import log
from crater.commun.outils_territoires import (
    traduire_code_insee_vers_id_commune_crater,
    traduire_code_insee_vers_id_epci_crater,
    traduire_code_insee_vers_id_departement_crater,
    traduire_code_insee_vers_id_region_crater,
)
from crater.consommation import chargeur_taux_pauvrete


def calculer_indicateur_taux_pauvrete(
    territoires: DataFrame,
    chemin_dossier_filosofi_pauvrete_communes: Path,
    chemin_dossier_filosofi_pauvrete_supra: Path,
    chemin_dossier_output: Path,
) -> None:
    log.info("##### CALCUL TAUX PAUVRETE AU SEUIL DE 60 POUR CENT #####")

    reinitialiser_dossier(chemin_dossier_output)

    df_source_taux_pauvrete_60_pourcent = _charger_sources(
        chemin_dossier_filosofi_pauvrete_communes,
        chemin_dossier_filosofi_pauvrete_supra,
    )

    df_indicateur_pauvrete = territoires.copy().join(df_source_taux_pauvrete_60_pourcent.set_index("id_territoire"))

    # Données de regroupements de communes non traités, ce serait non représentatif au vu du nombre de
    # communes dont les données manquent.

    df_indicateur_pauvrete = _vider_donnees_communes_avec_mouvement(df_indicateur_pauvrete)

    df_indicateur_pauvrete = df_indicateur_pauvrete.loc[
        :,
        [
            "nom_territoire",
            "categorie_territoire",
            "taux_pauvrete_60_pourcent",
        ],
    ]

    exporter_df_indicateurs_par_territoires(df_indicateur_pauvrete, chemin_dossier_output, "consommation")


def _charger_sources(chemin_dossier_pauvrete_communes, chemin_dossier_pauvrete_supra: Path) -> DataFrame:
    log.info("##### CHARGEMENT DES DONNEES SOURCES TAUX PAUVRETE AU SEUIL DE 60 POUR CENT #####")

    df_taux_pauvrete_pays = chargeur_taux_pauvrete.charger(chemin_dossier_pauvrete_supra / (NOM_FICHIERS_TAUX_PAUVRETE + "_METROPOLE.csv"))

    df_taux_pauvrete_pays["id_territoire"] = ID_FRANCE

    df_taux_pauvrete_region = chargeur_taux_pauvrete.charger(chemin_dossier_pauvrete_supra / (NOM_FICHIERS_TAUX_PAUVRETE + "_REG.csv"))
    df_taux_pauvrete_region["id_territoire"] = traduire_code_insee_vers_id_region_crater(df_taux_pauvrete_region["id_territoire"])

    df_taux_pauvrete_departement = chargeur_taux_pauvrete.charger(chemin_dossier_pauvrete_supra / (NOM_FICHIERS_TAUX_PAUVRETE + "_DEP.csv"))
    df_taux_pauvrete_departement["id_territoire"] = traduire_code_insee_vers_id_departement_crater(df_taux_pauvrete_departement["id_territoire"])

    df_taux_pauvrete_epci = chargeur_taux_pauvrete.charger(chemin_dossier_pauvrete_supra / (NOM_FICHIERS_TAUX_PAUVRETE + "_EPCI.csv"))
    df_taux_pauvrete_epci["id_territoire"] = traduire_code_insee_vers_id_epci_crater(df_taux_pauvrete_epci["id_territoire"])

    df_taux_pauvrete_commune = chargeur_taux_pauvrete.charger(chemin_dossier_pauvrete_communes / (NOM_FICHIERS_TAUX_PAUVRETE + "_COM.csv"))
    df_taux_pauvrete_commune["id_territoire"] = traduire_code_insee_vers_id_commune_crater(df_taux_pauvrete_commune["id_territoire"])

    df_taux_pauvrete_60_pourcent = pd.concat(
        [
            df_taux_pauvrete_pays,
            df_taux_pauvrete_region,
            df_taux_pauvrete_departement,
            df_taux_pauvrete_epci,
            df_taux_pauvrete_commune,
        ]
    )

    return df_taux_pauvrete_60_pourcent


def _vider_donnees_communes_avec_mouvement(df):
    df.loc[
        df.annee_dernier_mouvement_commune > ANNEE_REFERENTIEL_TERRITOIRE_TAUX_PAUVRETE,
        "taux_pauvrete_60_pourcent",
    ] = pandas.NA
    return df
