from pathlib import Path

import numpy as np
import pandas as pd

from crater.commun.config import ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_SAU_BIO
from crater.commun.logger import log
from crater.commun.outils_territoires import (
    ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales,
)
from crater.pratiques_agricoles.gestionnaire_sources_pratiques_agricoles import (
    importer_surfaces_bio,
)


def ajouter_indicateur_part_sau_bio(donnees_pratiques_agricoles: pd.DataFrame, chemin_fichier_surfaces_bio: Path) -> pd.DataFrame:
    log.info("    => Indicateur Part SAU Bio")

    donnees_pratiques_agricoles = _ajouter_donnees_surfaces_bio(donnees_pratiques_agricoles, chemin_fichier_surfaces_bio)
    donnees_pratiques_agricoles = _calculer_part_sau_bio_pourcent(donnees_pratiques_agricoles)

    return donnees_pratiques_agricoles


def _ajouter_donnees_surfaces_bio(donnees_pratiques_agricoles: pd.DataFrame, chemin_fichier_surfaces_bio) -> pd.DataFrame:
    surfaces_bio = importer_surfaces_bio(chemin_fichier_surfaces_bio).rename(columns={"id_commune": "id_territoire"}).set_index("id_territoire")

    donnees_pratiques_agricoles = donnees_pratiques_agricoles.join(surfaces_bio)

    donnees_pratiques_agricoles = _positionner_sau_bio_nulles_et_na(donnees_pratiques_agricoles)

    donnees_pratiques_agricoles.loc[
        donnees_pratiques_agricoles.annee_dernier_mouvement_commune > ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_SAU_BIO,
        "surface_bio_ha",
    ] = np.nan

    donnees_pratiques_agricoles = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(
        donnees_pratiques_agricoles,
        "surface_bio_ha",
        categorie_territoire_a_sommer="COMMUNE",
        categories_territoires_cibles=["REGROUPEMENT_COMMUNES"],
    )

    donnees_pratiques_agricoles = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(
        donnees_pratiques_agricoles,
        "surface_bio_ha",
        categorie_territoire_a_sommer="REGION",
        categories_territoires_cibles=["PAYS"],
    )

    return donnees_pratiques_agricoles


def _positionner_sau_bio_nulles_et_na(donnees_pratiques_agricoles):
    # On met 0 quand il s'agit d'une SAU nulle, et NA quand elle n'est pas renseignée car données confidentielle
    donnees_pratiques_agricoles["surface_bio_ha"] = donnees_pratiques_agricoles["surface_bio_ha"].fillna(0).replace([-1], np.nan)
    return donnees_pratiques_agricoles


def _calculer_part_sau_bio_pourcent(donnees_pratiques_agricoles):
    mask_donnees = donnees_pratiques_agricoles["sau_productive_ha"] > 0
    donnees_pratiques_agricoles.loc[mask_donnees, "part_sau_bio_pourcent"] = (
        (donnees_pratiques_agricoles.loc[mask_donnees, "surface_bio_ha"] / donnees_pratiques_agricoles.loc[mask_donnees, "sau_productive_ha"] * 100)
        .clip(0, 100)
        .round(2)
    )

    return donnees_pratiques_agricoles
