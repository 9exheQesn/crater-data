from pathlib import Path

import pandas as pd

from crater.commun.logger import log
from crater.commun.outils_territoires import (
    traduire_code_insee_vers_id_commune_crater,
    traduire_code_insee_vers_id_departement_crater,
    traduire_code_insee_vers_id_region_crater,
    traduire_code_insee_vers_id_epci_crater,
)
from crater.commun.outils_verification import verifier_absence_doublons


def charger_source_hvn(chemin_fichier: Path) -> pd.DataFrame:
    log.info("    => Chargement HVN depuis %s", chemin_fichier)

    df = (
        pd.read_excel(
            chemin_fichier,
            sheet_name="Feuil1",
            usecols="A:J",
            dtype={
                "COD_COM2018": "str",
                "NOM_COM": "str",
                "HVN_Ind1_17": "float",
                "HVN_Ind2_17": "float",
                "HVN_Ind3_17": "float",
                "Score_HVN_2017": "float",
            },
        )
        .rename(
            columns={
                "COD_COM2018": "id_commune",
                "NOM_COM": "nom_commune",
                "HVN_Ind1_17": "hvn_indice1",
                "HVN_Ind2_17": "hvn_indice2",
                "HVN_Ind3_17": "hvn_indice3",
                "Score_HVN_2017": "hvn_score",
            },
            errors="raise",
        )
        .reindex(
            columns=[
                "id_commune",
                "hvn_indice1",
                "hvn_indice2",
                "hvn_indice3",
                "hvn_score",
            ]
        )
    )

    verifier_absence_doublons(df, "id_commune")

    return df


def importer_hvn(chemin_fichier_hvn: Path):
    hvn_communes = charger_source_hvn(chemin_fichier_hvn)
    hvn_communes.id_commune = traduire_code_insee_vers_id_commune_crater(hvn_communes.id_commune)
    return hvn_communes


def charger_surfaces_bio(chemin_fichier: Path) -> pd.DataFrame:
    log.info("    => Chargement des surfaces bio depuis %s", chemin_fichier)

    df = (
        pd.read_excel(
            chemin_fichier,
            sheet_name="PV_Echelles",
            usecols="A:S",
            na_values="c",
            dtype={
                "zone": "str",
                "Annee": "int",
                "id_region": "str",
                "Libelle_region": "str",
                "id_departement": "str",
                "libelle_departement": "str",
                "id_canton": "str",
                "libelle_canton": "str",
                "id_commune": "str",
                "libelle_commune": "str",
                "code_groupe_culture": "str",
                "groupe_culture": "str",
                "nb_exp": "int",
                "SurfAB": "float",
                "SurfC1": "float",
                "SurfC2": "float",
                "SurfC3": "float",
                "SurfC123": "float",
                "SurfBio": "float",
            },
        )
        .rename(
            columns={
                "zone": "typologie_territoire",
                "SurfAB": "surface_AB_ha",
                "SurfC1": "surface_en_conversion_1ere_annee_ha",
                "SurfC2": "surface_en_conversion_2e_annee_ha",
                "SurfC3": "surface_en_conversion_3e_annee_ha",
                "SurfC123": "surface_en_conversion_ha",
                "SurfBio": "surface_bio_ha",
            },
            errors="raise",
        )
        .query('code_groupe_culture == "ALL"')
        .query('typologie_territoire != "Toute_Zone"')
    )

    return df


def importer_surfaces_bio(chemin_fichier_surfaces_bio: Path) -> pd.DataFrame:
    surfaces_bio = charger_surfaces_bio(chemin_fichier_surfaces_bio)
    surfaces_bio = surfaces_bio.drop(surfaces_bio[surfaces_bio.typologie_territoire == "canton"].index)
    surfaces_bio.loc[surfaces_bio.typologie_territoire == "commune", "id_territoire"] = traduire_code_insee_vers_id_commune_crater(
        surfaces_bio.id_commune
    )
    surfaces_bio.loc[surfaces_bio.typologie_territoire == "département", "id_territoire"] = traduire_code_insee_vers_id_departement_crater(
        surfaces_bio.id_departement
    )
    surfaces_bio.loc[surfaces_bio.typologie_territoire == "région", "id_territoire"] = traduire_code_insee_vers_id_region_crater(
        surfaces_bio.id_region
    )
    # Pour les epcis, l'id epci est dans la colonne typologie territoire...
    surfaces_bio.loc[
        ~surfaces_bio.typologie_territoire.isin(["commune", "département", "région"]),
        "id_territoire",
    ] = traduire_code_insee_vers_id_epci_crater(surfaces_bio.typologie_territoire)
    # les na correspondent aux donnees non renseignées pour cause de confidentialité => on remplace par -1 pour pouvoir les repérer par la suite
    surfaces_bio = surfaces_bio.fillna(-1)

    verifier_absence_doublons(surfaces_bio, "id_territoire")

    return surfaces_bio.loc[:, ["id_territoire", "surface_bio_ha"]]
