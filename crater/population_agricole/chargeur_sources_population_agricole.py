import pandas as pd

from crater.commun.chargeur_agreste import (
    filtrer_et_ajouter_colonne_id_territoire,
    charger_agreste,
)
from crater.commun.logger import log
from crater.commun.outils_verification import verifier_absence_doublons


def charger_agreste_actifs_agricoles(chemin_dossier_zip, fichier):
    chemin_fichier_zip = chemin_dossier_zip / "FDS_G_2005.zip"

    log.info(f"    => Chargement fichier AGRESTE des actifs agricoles permanents depuis {chemin_fichier_zip}/{fichier}")

    # Note : Le nombre d'exploitations n'est pas fiable pour G_2005_LIB_DIM2 == 'Ensemble , bcp de NA non présents si
    # on regarde les chefs d'exploitations (voir analyses R dans crater-data-exploration)
    df = (
        charger_agreste(chemin_fichier_zip, fichier, "G_2005")
        .rename(
            columns={
                "G_2005_LIB_DIM1": "taille_exploitation",
                "G_2005_LIB_DIM2": "lien_avec_exploitation",
                "G_2005_LIB_DIM3": "indicateur",
                "VALEUR": "valeur",
                "QUALITE": "qualite",
            },
            errors="raise",
        )
        .query(
            'taille_exploitation  == "Ensemble des exploitations (hors pacages collectifs)"'
            + ' & lien_avec_exploitation  == "Ensemble"'
            + ' & indicateur == "Nombre de personnes"'
        )
        .reindex(
            columns=[
                "FRANCE",
                "FRDOM",
                "REGION",
                "DEP",
                "COM",
                "taille_exploitation",
                "lien_avec_exploitation",
                "indicateur",
                "valeur",
                "qualite",
            ]
        )
        .astype({"valeur": "Int64"})
    )

    df = filtrer_et_ajouter_colonne_id_territoire(df)
    df = df.pivot(index="id_territoire", columns="indicateur", values="valeur").reset_index()
    df = df.rename(columns={"Nombre de personnes": "actifs_agricoles_permanents"})

    verifier_absence_doublons(df, "id_territoire")

    return df


def charger_agreste_nb_exploitation_par_classes_ages_chef_exploitation(chemin_dossier_zip, fichier):
    chemin_fichier_zip = chemin_dossier_zip / "FDS_G_2004.zip"
    log.info(f"    => Chargement fichier AGRESTE du nb d'exploitations selon l'âge du chef d'exploitation depuis {chemin_fichier_zip}/{fichier}")

    df = (
        charger_agreste(chemin_fichier_zip, fichier, "G_2004")
        .rename(
            columns={
                "G_2004_LIB_DIM1": "taille_exploitation",
                "G_2004_LIB_DIM2": "classe_age_chef_exploitation",
                "G_2004_LIB_DIM3": "indicateur",
                "VALEUR": "valeur",
            },
            errors="raise",
        )
        .query('taille_exploitation  == "Ensemble des exploitations (hors pacages collectifs)"' ' & indicateur == "Exploitations"')
        .reindex(
            columns=[
                "FRANCE",
                "FRDOM",
                "REGION",
                "DEP",
                "COM",
                "taille_exploitation",
                "classe_age_chef_exploitation",
                "indicateur",
                "valeur",
            ]
        )
        .astype({"valeur": "Int64"})
    )
    df = filtrer_et_ajouter_colonne_id_territoire(df)
    df = df.pivot(index="id_territoire", columns="classe_age_chef_exploitation", values="valeur").reset_index()
    df = df.loc[:, ["id_territoire", "Ensemble", "Moins de 40 ans", "De 40 à 49 ans", "De 50 à 59 ans", "60 ans ou plus",],].rename(
        columns={
            "Ensemble": "nb_exploitations_par_classes_ages_chef_exploitation.ensemble",
            "Moins de 40 ans": "nb_exploitations_par_classes_ages_chef_exploitation.moins_40_ans",
            "De 40 à 49 ans": "nb_exploitations_par_classes_ages_chef_exploitation.40_a_49_ans",
            "De 50 à 59 ans": "nb_exploitations_par_classes_ages_chef_exploitation.50_a_59_ans",
            "60 ans ou plus": "nb_exploitations_par_classes_ages_chef_exploitation.plus_60_ans",
        }
    )

    verifier_absence_doublons(df, "id_territoire")

    return df


def charger_agreste_nb_exploitations_et_sau_par_classes_superficies(chemin_dossier_zip, fichier):
    chemin_fichier_zip = chemin_dossier_zip / "FDS_G_2003.zip"
    log.info(f"    => Chargement fichier AGRESTE du nombre d'exploitation et SAU par classes de superficies depuis {chemin_fichier_zip}/{fichier}")

    df = (
        charger_agreste(chemin_fichier_zip, fichier, "G_2003")
        .rename(
            columns={
                "G_2003_LIB_DIM1": "taille_exploitation",
                "G_2003_LIB_DIM2": "surface_exploitation",
                "G_2003_LIB_DIM3": "indicateur",
                "VALEUR": "valeur",
            },
            errors="raise",
        )
        .query(
            'taille_exploitation  == "Ensemble des exploitations (hors pacages collectifs)"'
            ' & indicateur in ["Exploitations", "Superficie agricole utilisée (ha)"]'
        )
        .reindex(
            columns=[
                "FRANCE",
                "FRDOM",
                "REGION",
                "DEP",
                "COM",
                "taille_exploitation",
                "surface_exploitation",
                "indicateur",
                "valeur",
            ]
        )
    )

    df = filtrer_et_ajouter_colonne_id_territoire(df)

    df = df.pivot(
        index="id_territoire",
        columns=["indicateur", "surface_exploitation"],
        values="valeur",
    ).reset_index()

    df.columns = [
        "id_territoire",
        *[".".join(col).strip() for col in df.columns[1:]],
    ]
    df = _renommer_groupes_colonnes_par_indicateur(df, "Exploitations", "nb_exploitations_par_classes_superficies", pd.Int64Dtype())
    df = _renommer_groupes_colonnes_par_indicateur(
        df,
        "Superficie agricole utilisée (ha)",
        "sau_ha_par_classes_superficies",
        "float64",
    )

    df = df.loc[
        :,
        [
            "id_territoire",
            "nb_exploitations_par_classes_superficies.ensemble",
            "nb_exploitations_par_classes_superficies.moins_20_ha",
            "nb_exploitations_par_classes_superficies.20_a_50_ha",
            "nb_exploitations_par_classes_superficies.50_a_100_ha",
            "nb_exploitations_par_classes_superficies.100_a_200_ha",
            "nb_exploitations_par_classes_superficies.plus_200_ha",
            "sau_ha_par_classes_superficies.ensemble",
            "sau_ha_par_classes_superficies.moins_20_ha",
            "sau_ha_par_classes_superficies.20_a_50_ha",
            "sau_ha_par_classes_superficies.50_a_100_ha",
            "sau_ha_par_classes_superficies.100_a_200_ha",
            "sau_ha_par_classes_superficies.plus_200_ha",
        ],
    ]

    verifier_absence_doublons(df, "id_territoire")

    return df


def _renommer_groupes_colonnes_par_indicateur(df, nom_indicateur_prefixe_initial, nom_indicateur_prefixe_nouveau, dtype_colonnes):
    df.columns = [col.replace(nom_indicateur_prefixe_initial, nom_indicateur_prefixe_nouveau) for col in df.columns]
    df.columns = [col.replace("Moins de 20 hectares (ha) y compris sans SAU", "moins_20_ha") for col in df.columns]
    df.columns = [col.replace("De 20 à moins de 50 ha", "20_a_50_ha") for col in df.columns]
    df.columns = [col.replace("De 50 à moins de 100 ha", "50_a_100_ha") for col in df.columns]
    df.columns = [col.replace("De 100 à moins de 200 ha", "100_a_200_ha") for col in df.columns]
    df.columns = [col.replace("200 ha ou plus", "plus_200_ha") for col in df.columns]
    df.columns = [col.replace("Ensemble", "ensemble") for col in df.columns]

    for column in df.columns:
        if column.startswith(nom_indicateur_prefixe_nouveau):
            df.loc[:, column] = df.loc[:, column].astype(dtype_colonnes)
    return df
