from pathlib import Path
import numpy as np
import pandas as pd

from crater.commun.config import (
    IDS_CATEGORIES_TERRITOIRES_SUPRACOMMUNAUX,
    ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_AGRESTE_RA_2010,
)
from crater.commun.diagrammes import generer_histogrammes
from crater.commun.export_fichier import (
    reinitialiser_dossier,
    exporter_df_indicateurs_par_territoires,
)
from crater.commun.logger import log
from crater.commun.outils_territoires import (
    trier_territoires,
    ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales,
    remplir_de_zeros_sous_colonnes_si_totaux_ok,
)
from crater.population_agricole.chargeur_sources_population_agricole import (
    charger_agreste_actifs_agricoles,
    charger_agreste_nb_exploitation_par_classes_ages_chef_exploitation,
    charger_agreste_nb_exploitations_et_sau_par_classes_superficies,
)
from crater.population_agricole.evolution_part_population_agricole import (
    ajouter_colonne_evolution_part_population_agricole,
)
from crater.population_agricole.part_population_agricole_1988 import (
    ajouter_colonne_part_population_agricole_1988,
)
from crater.population_agricole.part_population_agricole_2010 import (
    ajouter_colonne_part_population_agricole_2010,
)


def calculer_population_agricole(territoires: pd.DataFrame, dossier_sources_agreste: Path, chemin_dossier_output: Path) -> None:
    log.info("##### CALCUL POPULATION AGRICOLE #####")

    reinitialiser_dossier(chemin_dossier_output)

    sources_population_agricole = _charger_sources(dossier_sources_agreste)

    donnees_population_agricole = _construire_df_population_agricole(territoires, sources_population_agricole)
    donnees_population_agricole = ajouter_colonne_part_population_agricole_2010(donnees_population_agricole)
    donnees_population_agricole = ajouter_colonne_part_population_agricole_1988(donnees_population_agricole)
    donnees_population_agricole = ajouter_colonne_evolution_part_population_agricole(donnees_population_agricole)
    donnees_population_agricole = _ajouter_note(donnees_population_agricole)

    exporter_df_indicateurs_par_territoires(
        donnees_population_agricole.drop(columns=["annee_dernier_mouvement_commune"] + IDS_CATEGORIES_TERRITOIRES_SUPRACOMMUNAUX),
        chemin_dossier_output,
        "population_agricole",
    )
    generer_histogrammes(
        donnees_population_agricole,
        [
            "note",
        ],
        chemin_dossier_output / "diagrammes",
    )


def _ajouter_note(
    population_agricole: pd.DataFrame,
) -> pd.DataFrame:
    log.info("    => Ajout note")

    population_agricole.loc[:, "note"] = (
        (
            population_agricole["part_population_agricole_2010_note"]
            + population_agricole["evolution_part_population_agricole_note"] / 2
            - 2.5
            # TODO: pourquoi Int64 et pas int64 ? Il faudrait uniformiser
            #  (pour l'instant Int64 et Float64 sont encore expérimentaux)
        )
        .round(0)
        .clip(lower=0, upper=10)
        .astype("Int64")
    )
    return population_agricole


def _charger_sources(chemin_sources_agreste: Path) -> pd.DataFrame:
    log.info("##### CHARGEMEMENT DES DONNEES SOURCES POPULATION AGRICOLE #####")

    df_actifs_agricoles_2010 = charger_agreste_actifs_agricoles(chemin_sources_agreste, "FDS_G_2005_2010.txt").rename(
        columns={"actifs_agricoles_permanents": "actifs_agricoles_permanents_2010"}
    )

    df_actifs_agricoles_1988 = charger_agreste_actifs_agricoles(chemin_sources_agreste, "FDS_G_2005_1988.txt").rename(
        columns={"actifs_agricoles_permanents": "actifs_agricoles_permanents_1988"}
    )

    df_exploitations_par_classes_age_exploitant_2010 = charger_agreste_nb_exploitation_par_classes_ages_chef_exploitation(
        chemin_sources_agreste, "FDS_G_2004_2010.txt"
    )
    df_exploitations_2010 = df_exploitations_par_classes_age_exploitant_2010.loc[
        :,
        (
            "id_territoire",
            "nb_exploitations_par_classes_ages_chef_exploitation.ensemble",
        ),
    ].rename(columns={"nb_exploitations_par_classes_ages_chef_exploitation.ensemble": "nb_exploitations_2010"})

    df_exploitations_1988 = (
        charger_agreste_nb_exploitation_par_classes_ages_chef_exploitation(chemin_sources_agreste, "FDS_G_2004_1988.txt")
        .rename(columns={"nb_exploitations_par_classes_ages_chef_exploitation.ensemble": "nb_exploitations_1988"})
        .loc[:, ("id_territoire", "nb_exploitations_1988")]
    )

    df_exploitations_et_sau_par_classes_sau_2010 = charger_agreste_nb_exploitations_et_sau_par_classes_superficies(
        chemin_sources_agreste, "FDS_G_2003_2010.txt"
    )

    df_exploitations_et_sau_par_classes_sau_1988 = charger_agreste_nb_exploitations_et_sau_par_classes_superficies(
        chemin_sources_agreste, "FDS_G_2003_1988.txt"
    )

    df_sau_2010 = df_exploitations_et_sau_par_classes_sau_2010.loc[:, ("id_territoire", "sau_ha_par_classes_superficies.ensemble")].rename(
        columns={"sau_ha_par_classes_superficies.ensemble": "sau_ha_2010"}
    )

    df_sau_1988 = df_exploitations_et_sau_par_classes_sau_1988.loc[:, ("id_territoire", "sau_ha_par_classes_superficies.ensemble")].rename(
        columns={"sau_ha_par_classes_superficies.ensemble": "sau_ha_1988"}
    )

    df_sources_population_agricole = (
        df_actifs_agricoles_2010.merge(df_actifs_agricoles_1988, how="outer", on="id_territoire")
        .merge(df_exploitations_2010, how="outer", on="id_territoire")
        .merge(df_exploitations_1988, how="outer", on="id_territoire")
        .merge(df_sau_2010, how="outer", on="id_territoire")
        .merge(df_sau_1988, how="outer", on="id_territoire")
        .merge(
            df_exploitations_par_classes_age_exploitant_2010,
            how="outer",
            on="id_territoire",
        )
        .merge(
            df_exploitations_et_sau_par_classes_sau_2010,
            how="outer",
            on="id_territoire",
        )
    )

    return df_sources_population_agricole


def _construire_df_population_agricole(territoires: pd.DataFrame, sources_population_agricole: pd.DataFrame) -> pd.DataFrame:
    donnees_population_agricole = _assembler_df_territoire_et_population_agricole(territoires, sources_population_agricole)
    donnees_population_agricole = _remplir_valeurs_manquantes(donnees_population_agricole)
    donnees_population_agricole = _traiter_mouvements_communes(donnees_population_agricole)
    donnees_population_agricole = _consolider_indicateurs_sur_epcis_et_regroupements_communes(donnees_population_agricole)
    donnees_population_agricole = _ajouter_sau_moyenne_par_exploitation(donnees_population_agricole)
    donnees_population_agricole = trier_territoires(donnees_population_agricole)

    return donnees_population_agricole


def _assembler_df_territoire_et_population_agricole(territoires, sources_population_agricole):
    colonnes = (
        ["nom_territoire", "categorie_territoire"]
        + IDS_CATEGORIES_TERRITOIRES_SUPRACOMMUNAUX
        + [
            "annee_dernier_mouvement_commune",
            "population_totale_2010",
            "population_totale_1990",
        ]
    )
    donnees_population_agricole = territoires[colonnes]
    sources_population_agricole = sources_population_agricole.set_index("id_territoire")
    donnees_population_agricole = donnees_population_agricole.join(sources_population_agricole)

    return donnees_population_agricole


def _remplir_valeurs_manquantes(donnees_population_agricole):
    remplir_de_zeros_sous_colonnes_si_totaux_ok(
        donnees_population_agricole,
        "nb_exploitations_par_classes_ages_chef_exploitation",
        "moins_40_ans",
        "plus_60_ans",
    )
    remplir_de_zeros_sous_colonnes_si_totaux_ok(
        donnees_population_agricole,
        "nb_exploitations_par_classes_superficies",
        "moins_20_ha",
        "plus_200_ha",
    )
    remplir_de_zeros_sous_colonnes_si_totaux_ok(
        donnees_population_agricole,
        "sau_ha_par_classes_superficies",
        "moins_20_ha",
        "plus_200_ha",
    )

    return donnees_population_agricole


def _traiter_mouvements_communes(donnees_population_agricole):
    donnees_population_agricole.loc[
        donnees_population_agricole.annee_dernier_mouvement_commune > ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_AGRESTE_RA_2010,
        "actifs_agricoles_permanents_2010":"sau_ha_par_classes_superficies.plus_200_ha",
    ] = np.nan
    return donnees_population_agricole


def _consolider_indicateurs_sur_epcis_et_regroupements_communes(
    donnees_population_agricole,
):
    for nom_colonne in donnees_population_agricole.loc[
        :,
        "actifs_agricoles_permanents_2010":"sau_ha_par_classes_superficies.plus_200_ha",
    ].columns:
        donnees_population_agricole = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(
            donnees_population_agricole,
            nom_colonne,
            categorie_territoire_a_sommer="COMMUNE",
            categories_territoires_cibles=["EPCI", "REGROUPEMENT_COMMUNES"],
        )
    return donnees_population_agricole


def _ajouter_sau_moyenne_par_exploitation(donnees_population_agricole):
    mask_nb_exploitations_2010_positif = donnees_population_agricole.loc[:, "nb_exploitations_2010"] > 0
    donnees_population_agricole.loc[mask_nb_exploitations_2010_positif, "sau_moyenne_par_exploitation_ha_2010"] = round(
        donnees_population_agricole.loc[mask_nb_exploitations_2010_positif, "sau_ha_2010"]
        / donnees_population_agricole.loc[mask_nb_exploitations_2010_positif, "nb_exploitations_2010"],
        1,
    )

    mask_nb_exploitations_1988_positif = donnees_population_agricole.loc[:, "nb_exploitations_1988"] > 0
    donnees_population_agricole.loc[mask_nb_exploitations_1988_positif, "sau_moyenne_par_exploitation_ha_1988"] = round(
        donnees_population_agricole.loc[mask_nb_exploitations_1988_positif, "sau_ha_1988"]
        / donnees_population_agricole.loc[mask_nb_exploitations_1988_positif, "nb_exploitations_1988"],
        1,
    )
    return donnees_population_agricole
