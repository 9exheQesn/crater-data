from pandas import DataFrame

from crater.commun.config import ID_FRANCE
from crater.commun.logger import log
from crater.commun.notes_et_messages import calculer_note_par_interpolation_2_seuils


def ajouter_colonne_part_population_agricole_1988(
    donnees_population_agricole: DataFrame,
) -> DataFrame:
    log.info("    => Indicateur part population agricole 1988")

    donnees_population_agricole.loc[:, "part_population_agricole_1988_pourcent"] = (
        (
            donnees_population_agricole["actifs_agricoles_permanents_1988"]
            / donnees_population_agricole["population_totale_1990"]
            * 100
            #   on caste explicitement en float64 sinon le type attribué par pandas est Float64
            #   Float64 est expérimental et ne fonctionne pas encore avec to_csv (format %.2f non respecté)
            #   (voir https://pandas.pydata.org/pandas-docs/dev/user_guide/missing_data.html#missing-data-na) ou https://pandas.pydata.org/pandas-docs/dev/whatsnew/v1.2.0.html?highlight=float64dtype
        )
        .round(2)
        .astype("float64")
    )

    seuil_valeur_france = donnees_population_agricole.query(f"id_territoire=='{ID_FRANCE}'")["part_population_agricole_1988_pourcent"][0]
    donnees_population_agricole.loc[:, "part_population_agricole_1988_note"] = calculer_note_par_interpolation_2_seuils(
        donnees_population_agricole["part_population_agricole_1988_pourcent"],
        0,
        0,
        seuil_valeur_france,
        5,
    )

    return donnees_population_agricole
