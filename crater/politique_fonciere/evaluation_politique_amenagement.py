from pandas import DataFrame

from crater.commun.logger import log
from crater.politique_fonciere.parametres import (
    SEUIL_ARTIFICIALISATION_HA_QUASI_NULLE_OU_NEGATIVE,
    CODE_ARTIFICIALISATION_POSITIVE_ET_NB_MENAGES_EMPLOIS_EN_BAISSE,
    CODE_ARTIFICIALISATION_QUASI_NULLE_OU_NEGATIVE,
    CODE_ARTIFICIALISATION_POSITIVE_ET_NB_MENAGES_EMPLOIS_EN_HAUSSE,
    CODE_DONNEES_NON_DISPONIBLES,
)


def ajouter_evaluation_politique_amenagement(
    donnees_politique_fonciere: DataFrame,
) -> DataFrame:
    log.info("    => Indicateur évaluation politique d'aménagement")

    donnees_politique_fonciere["evaluation_politique_amenagement"] = CODE_DONNEES_NON_DISPONIBLES
    donnees_politique_fonciere["evaluation_politique_amenagement"] = (
        donnees_politique_fonciere["evaluation_politique_amenagement"]
        .mask(
            donnees_politique_fonciere["artificialisation_2011_2016_ha"] <= SEUIL_ARTIFICIALISATION_HA_QUASI_NULLE_OU_NEGATIVE,
            CODE_ARTIFICIALISATION_QUASI_NULLE_OU_NEGATIVE,
        )
        .mask(
            (donnees_politique_fonciere["artificialisation_2011_2016_ha"] > SEUIL_ARTIFICIALISATION_HA_QUASI_NULLE_OU_NEGATIVE)
            & (donnees_politique_fonciere["evolution_menages_emplois_2011_2016"] > 0),
            CODE_ARTIFICIALISATION_POSITIVE_ET_NB_MENAGES_EMPLOIS_EN_HAUSSE,
        )
        .mask(
            (donnees_politique_fonciere["artificialisation_2011_2016_ha"] > SEUIL_ARTIFICIALISATION_HA_QUASI_NULLE_OU_NEGATIVE)
            & (donnees_politique_fonciere["evolution_menages_emplois_2011_2016"] <= 0),
            CODE_ARTIFICIALISATION_POSITIVE_ET_NB_MENAGES_EMPLOIS_EN_BAISSE,
        )
    )

    return donnees_politique_fonciere
