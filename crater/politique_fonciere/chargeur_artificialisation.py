import pandas as pd
from pandas import DataFrame

from crater.commun.logger import log
from crater.commun.outils_territoires import traduire_code_insee_vers_id_commune_crater
from crater.commun.outils_verification import verifier_absence_doublons


def charger(chemin_fichier):
    log.info("    => Chargement artificialisation depuis %s", chemin_fichier)

    df = (
        pd.read_csv(
            chemin_fichier,
            sep=",",
            dtype={
                "DEPCOM": "str",
                "idcom": "str",
                "idcomtxt": "str",
                "idreg": "str",
                "idregtxt": "str",
                "iddep": "str",
                "iddeptxt": "str",
                "epci19": "str",
                "epci19txt": "str",
                "au10": "str",
                "typau": "str",
                "typpopau10": "str",
                "uu": "str",
                "naf09art10": "float",
                "art09act10": "float",
                "art09hab10": "float",
                "art09mix10": "float",
                "art09inc10": "float",
                "naf10art11": "float",
                "art10act11": "float",
                "art10hab11": "float",
                "art10mix11": "float",
                "art10inc11": "float",
                "naf11art12": "float",
                "art11act12": "float",
                "art11hab12": "float",
                "art11mix12": "float",
                "art11inc12": "float",
                "naf12art13": "float",
                "art12act13": "float",
                "art12hab13": "float",
                "art12mix13": "float",
                "art12inc13": "float",
                "naf13art14": "float",
                "art13act14": "float",
                "art13hab14": "float",
                "art13mix14": "float",
                "art13inc14": "float",
                "naf14art15": "float",
                "art14act15": "float",
                "art14hab15": "float",
                "art14mix15": "float",
                "art14inc15": "float",
                "naf15art16": "float",
                "art15act16": "float",
                "art15hab16": "float",
                "art15mix16": "float",
                "art15inc16": "float",
                "naf16art17": "float",
                "art16act17": "float",
                "art16hab17": "float",
                "art16mix17": "float",
                "art16inc17": "float",
                "naf17art18": "float",
                "art17act18": "float",
                "art17hab18": "float",
                "art17mix18": "float",
                "art17inc18": "float",
                "nafart0918": "float",
                "artact0918": "float",
                "arthab0918": "float",
                "artmix0918": "float",
                "artinc0918": "float",
                "artcom0918": "float",
                "pop11": "Int64",
                "pop16": "Int64",
                "pop1116": "Int64",
                "men11": "Int64",
                "men16": "Int64",
                "men1116": "Int64",
                "emp16": "Int64",
                "emp11": "Int64",
                "emp1116": "Int64",
                "mepart1116": "float",
                "menhab1116": "float",
                "artpop1116": "float",
                "surfcom18": "float",
            },
        )
        .assign(naf11art16=lambda x: (x["naf11art12"] + x["naf12art13"] + x["naf13art14"] + x["naf14art15"] + x["naf15art16"]) / 1e4)
        .assign(surfcom18=lambda x: round(x["surfcom18"] / 1e4, 0))
        .rename(
            columns={
                "idcom": "id_commune",
                "naf11art16": "artificialisation_2011_2016_ha",
                "men1116": "evolution_menages_2011_2016",
                "emp1116": "evolution_emplois_2011_2016",
            },
            errors="raise",
        )
        .reindex(
            columns=[
                "id_commune",
                "artificialisation_2011_2016_ha",
                "evolution_menages_2011_2016",
                "evolution_emplois_2011_2016",
            ]
        )
    )

    df = _traduire_code_insee_vers_id_crater(df)
    verifier_absence_doublons(df, "id_commune")

    return df


def _traduire_code_insee_vers_id_crater(df: DataFrame) -> DataFrame:
    df.id_commune = traduire_code_insee_vers_id_commune_crater(df.id_commune)
    return df
