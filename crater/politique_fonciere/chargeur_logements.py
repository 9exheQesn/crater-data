from pathlib import Path

import pandas as pd
from pandas import DataFrame

from crater.commun.logger import log
from crater.commun.outils_territoires import traduire_code_insee_vers_id_commune_crater
from crater.commun.outils_verification import verifier_absence_doublons


def charger(chemin_fichier: Path, nom_variable: "str") -> DataFrame:
    log.info("    => Chargement nombre de logements depuis %s", chemin_fichier)

    df = pd.read_excel(
        chemin_fichier,
        sheet_name="Data",
        usecols="A:D",
        skiprows=4,
        dtype={
            "codgeo": "str",
            "libgeo": "str",
            "an": "Int64",
            "p_log": "Int64",
            "p_logvac": "Int64",
        },
        na_values=[""],
    ).rename(
        columns={
            "codgeo": "id_commune",
            "libgeo": "nom_commune",
            "an": "annee",
            "p_log": nom_variable,
            "p_logvac": nom_variable,
        }
    )
    df = df.pivot(index="id_commune", columns="annee", values=nom_variable).reset_index()
    df = df.rename(
        columns={
            1968: nom_variable + "_1968",
            1975: nom_variable + "_1975",
            1982: nom_variable + "_1982",
            1990: nom_variable + "_1990",
            1999: nom_variable + "_1999",
            2008: nom_variable + "_2008",
            2013: nom_variable + "_2013",
            2018: nom_variable + "_2018",
        }
    )

    df = _traduire_code_insee_vers_id_crater(df)
    verifier_absence_doublons(df, "id_commune")

    return df


def _traduire_code_insee_vers_id_crater(df: DataFrame) -> DataFrame:
    df.id_commune = traduire_code_insee_vers_id_commune_crater(df.id_commune)
    return df
