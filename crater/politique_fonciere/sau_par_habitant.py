from pandas import DataFrame

from crater.commun.logger import log
from crater.commun.notes_et_messages import calculer_note_par_interpolation_3_seuils
from crater.politique_fonciere.parametres import (
    SEUIL_SAU_PAR_HAB_M2_REGIME_TRES_VEGETAL,
    SEUIL_SAU_PAR_HAB_M2_REGIME_ACTUEL,
)


def ajouter_sau_par_habitant(donnees_politique_fonciere: DataFrame) -> DataFrame:
    log.info("    => Indicateur sau par habitant")

    mask_donnees = donnees_politique_fonciere["population_totale_2017"] > 0
    donnees_politique_fonciere.loc[mask_donnees, "sau_par_habitant_valeur_m2_par_hab"] = (
        (
            donnees_politique_fonciere.loc[mask_donnees, "sau_productive_ha"]
            * 1e4
            / donnees_politique_fonciere.loc[mask_donnees, "population_totale_2017"]
        )
        .round(0)
        .astype("Int64")
    )

    donnees_politique_fonciere.loc[:, "sau_par_habitant_note"] = calculer_note_par_interpolation_3_seuils(
        donnees_politique_fonciere["sau_par_habitant_valeur_m2_par_hab"],
        0,
        0,
        SEUIL_SAU_PAR_HAB_M2_REGIME_TRES_VEGETAL,
        5,
        SEUIL_SAU_PAR_HAB_M2_REGIME_ACTUEL,
        10,
    )

    return donnees_politique_fonciere
