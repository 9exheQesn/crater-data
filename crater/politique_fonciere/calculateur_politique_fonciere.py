from pathlib import Path

import numpy as np
import pandas as pd
from pandas import DataFrame

import crater.politique_fonciere.chargeur_artificialisation as chargeur_artificialisation
import crater.politique_fonciere.chargeur_logements as chargeur_logements
from crater.commun.config import (
    IDS_CATEGORIES_TERRITOIRES_SUPRACOMMUNAUX,
    ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_ARTIFICIALISATION,
    ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_NB_LOGEMENTS,
)
from crater.commun.diagrammes import generer_histogrammes
from crater.commun.export_fichier import (
    reinitialiser_dossier,
    exporter_df_indicateurs_par_territoires,
)
from crater.commun.logger import log
from crater.commun.outils_dataframes import DictDataFrames
from crater.commun.outils_territoires import (
    ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales,
)
from crater.politique_fonciere.evaluation_politique_amenagement import (
    ajouter_evaluation_politique_amenagement,
)
from crater.politique_fonciere.rythme_artificialisation_sau import (
    ajouter_rythme_artificialisation_sau,
)
from crater.politique_fonciere.sau_par_habitant import ajouter_sau_par_habitant


def calculer_politique_fonciere(
    territoires: DataFrame,
    chemin_fichier_artificialisation: Path,
    chemin_fichier_nb_logements: Path,
    chemin_fichier_nb_logements_vacants: Path,
    chemin_dossier_output: Path,
) -> None:
    log.info("##### CALCUL POLITIQUE FONCIERE #####")

    reinitialiser_dossier(chemin_dossier_output)

    sources_politique_fonciere = _charger_sources(
        chemin_fichier_artificialisation,
        chemin_fichier_nb_logements,
        chemin_fichier_nb_logements_vacants,
    )

    donnees_politique_fonciere = _calculer_donnees_politique_fonciere(territoires, sources_politique_fonciere)
    donnees_politique_fonciere = ajouter_evaluation_politique_amenagement(donnees_politique_fonciere)
    donnees_politique_fonciere = ajouter_sau_par_habitant(donnees_politique_fonciere)
    donnees_politique_fonciere = ajouter_rythme_artificialisation_sau(donnees_politique_fonciere)
    donnees_politique_fonciere = _ajouter_note(donnees_politique_fonciere)

    exporter_df_indicateurs_par_territoires(
        donnees_politique_fonciere.drop(columns=["annee_dernier_mouvement_commune"] + IDS_CATEGORIES_TERRITOIRES_SUPRACOMMUNAUX),
        chemin_dossier_output,
        "politique_fonciere",
    )
    generer_histogrammes(
        donnees_politique_fonciere,
        [
            "note",
        ],
        chemin_dossier_output / "diagrammes",
    )


def _ajouter_note(
    donnees_politique_fonciere: DataFrame,
) -> DataFrame:
    log.info("    => Ajout note")

    donnees_politique_fonciere.loc[:, "note"] = (
        (donnees_politique_fonciere["sau_par_habitant_note"] + donnees_politique_fonciere["rythme_artificialisation_sau_note"]) / 2
    ).round(0)

    # Ressemble à un bug pandas : quand on somme sau_par_habitant_note (qui contient des pandas.NA)
    # et rythme_artificialisation_sau_note qui contient des numpy.nan,
    # on obtient dans la colonne note un mix des deux constantes,
    # et surtout les valeurs nan ne sont plus reconnues par pandas quand on fait isnan ou replace(to_replace=numpy.nan, value=pd.NA)
    # et la conversion en Int64Dtype donne des erreurs
    # contournement =  on utilise np.isnan() pour basculer toutes les valeurs sur pd.NA
    # TODO : il faudrait homogénéiser la gestion des NA dans l'app entre numpy et pandas (attendre que cela soit plus stable dans pandas)
    donnees_politique_fonciere.loc[np.isnan(donnees_politique_fonciere["note"]), ["note"]] = pd.NA  # type: ignore[call-overload] # voir ci-dessus
    donnees_politique_fonciere["note"] = donnees_politique_fonciere["note"].astype(pd.Int64Dtype())

    return donnees_politique_fonciere


def _calculer_donnees_politique_fonciere(territoires: DataFrame, sources_politique_fonciere: DictDataFrames) -> DataFrame:
    colonnes = (
        ["nom_territoire", "categorie_territoire"]
        + IDS_CATEGORIES_TERRITOIRES_SUPRACOMMUNAUX
        + [
            "annee_dernier_mouvement_commune",
            "superficie_ha",
            "sau_productive_ha",
            "population_totale_2017",
        ]
    )
    donnees_politique_fonciere = territoires[colonnes]

    donnees_politique_fonciere = _ajout_sources_donnees(donnees_politique_fonciere, sources_politique_fonciere)
    donnees_politique_fonciere = _traiter_mouvements_communes(donnees_politique_fonciere)

    donnees_politique_fonciere["evolution_menages_emplois_2011_2016"] = (
        donnees_politique_fonciere["evolution_menages_2011_2016"] + donnees_politique_fonciere["evolution_emplois_2011_2016"]
    )

    donnees_politique_fonciere = _ajouter_donnees_supracommunales(donnees_politique_fonciere)

    donnees_politique_fonciere["artificialisation_2011_2016_sur_superficie_pourcent"] = (
        donnees_politique_fonciere["artificialisation_2011_2016_ha"] / donnees_politique_fonciere["superficie_ha"] * 100
    )

    donnees_politique_fonciere["part_logements_vacants_2013_pourcent"] = round(
        donnees_politique_fonciere["nb_logements_vacants_2013"] / donnees_politique_fonciere["nb_logements_2013"] * 100,
        1,
    )

    donnees_politique_fonciere["part_logements_vacants_2018_pourcent"] = round(
        donnees_politique_fonciere["nb_logements_vacants_2018"] / donnees_politique_fonciere["nb_logements_2018"] * 100,
        1,
    )

    return donnees_politique_fonciere


def _ajout_sources_donnees(donnees_politique_fonciere: DataFrame, sources_politique_fonciere: DictDataFrames) -> DataFrame:
    donnees_artificialisation = (
        sources_politique_fonciere["artificialisation"].rename(columns={"id_commune": "id_territoire"}).set_index("id_territoire")
    )

    donnees_politique_fonciere = donnees_politique_fonciere.join(donnees_artificialisation)

    donnees_nb_logements = sources_politique_fonciere["nb_logements"].rename(columns={"id_commune": "id_territoire"}).set_index("id_territoire")
    donnees_nb_logements_vacants = (
        sources_politique_fonciere["nb_logements_vacants"].rename(columns={"id_commune": "id_territoire"}).set_index("id_territoire")
    )
    donnees_politique_fonciere = donnees_politique_fonciere.join(donnees_nb_logements.loc[:, slice("nb_logements_2013", "nb_logements_2018")])
    donnees_politique_fonciere = donnees_politique_fonciere.join(
        donnees_nb_logements_vacants.loc[:, slice("nb_logements_vacants_2013", "nb_logements_vacants_2018")]
    )

    return donnees_politique_fonciere


def _traiter_mouvements_communes(donnees_politique_fonciere: DataFrame) -> DataFrame:
    donnees_politique_fonciere.loc[
        donnees_politique_fonciere.annee_dernier_mouvement_commune > ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_ARTIFICIALISATION,
        "artificialisation_2011_2016_ha",
    ] = np.nan
    donnees_politique_fonciere.loc[
        donnees_politique_fonciere.annee_dernier_mouvement_commune > ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_ARTIFICIALISATION,
        "evolution_menages_2011_2016",
    ] = np.nan
    donnees_politique_fonciere.loc[
        donnees_politique_fonciere.annee_dernier_mouvement_commune > ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_ARTIFICIALISATION,
        "evolution_emplois_2011_2016",
    ] = np.nan

    donnees_politique_fonciere.loc[
        donnees_politique_fonciere.annee_dernier_mouvement_commune > ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_NB_LOGEMENTS,
        "nb_logements_2013",
    ] = np.nan
    donnees_politique_fonciere.loc[
        donnees_politique_fonciere.annee_dernier_mouvement_commune > ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_NB_LOGEMENTS,
        "nb_logements_2018",
    ] = np.nan
    donnees_politique_fonciere.loc[
        donnees_politique_fonciere.annee_dernier_mouvement_commune > ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_NB_LOGEMENTS,
        "nb_logements_vacants_2013",
    ] = np.nan
    donnees_politique_fonciere.loc[
        donnees_politique_fonciere.annee_dernier_mouvement_commune > ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_NB_LOGEMENTS,
        "nb_logements_vacants_2018",
    ] = np.nan

    return donnees_politique_fonciere


def _ajouter_donnees_supracommunales(
    donnees_politique_fonciere: DataFrame,
) -> DataFrame:
    donnees_politique_fonciere = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(
        donnees_politique_fonciere, "artificialisation_2011_2016_ha"
    )
    donnees_politique_fonciere = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(
        donnees_politique_fonciere, "evolution_menages_2011_2016"
    )
    donnees_politique_fonciere = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(
        donnees_politique_fonciere, "evolution_emplois_2011_2016"
    )
    donnees_politique_fonciere = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(
        donnees_politique_fonciere,
        "evolution_menages_emplois_2011_2016",
    )
    donnees_politique_fonciere = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(donnees_politique_fonciere, "nb_logements_2013")
    donnees_politique_fonciere = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(donnees_politique_fonciere, "nb_logements_2018")
    donnees_politique_fonciere = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(
        donnees_politique_fonciere, "nb_logements_vacants_2013"
    )
    donnees_politique_fonciere = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(
        donnees_politique_fonciere, "nb_logements_vacants_2018"
    )

    return donnees_politique_fonciere


def _charger_sources(
    chemin_fichier_artificialisation: Path,
    chemin_fichier_nb_logements: Path,
    chemin_fichier_nb_logements_vacants: Path,
) -> DictDataFrames:
    log.info("##### CHARGEMEMENT DES DONNEES SOURCES POLITIQUE FONCIERE #####")

    artificialisation = chargeur_artificialisation.charger(chemin_fichier_artificialisation)
    nb_logements = chargeur_logements.charger(chemin_fichier_nb_logements, "nb_logements")
    nb_logements_vacants = chargeur_logements.charger(chemin_fichier_nb_logements_vacants, "nb_logements_vacants")

    return {
        "artificialisation": artificialisation,
        "nb_logements": nb_logements,
        "nb_logements_vacants": nb_logements_vacants,
    }
