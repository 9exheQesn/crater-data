import argparse
import time
from datetime import datetime
from multiprocessing import Pool
from typing import Callable

import pandas

from crater.cartographie.calculateur_cartographie import calculer_cartes
from crater.collecteurs.hubeau.collecteur_prelevements_eau import collecter_donnees_prelevements_eau
from crater.collecteurs.parcel import collecteur_besoins_api_parcel
from crater.collecteurs.parcel.collecteur_besoins_api_parcel import ID_ASSIETTE_ACTUELLE, ID_ASSIETTE_MOINS_50_POURCENT
from crater.collecteurs.parcel.generateur_correspondance_id_territoires import generer_fichier_correspondance_territoires_parcel_crater
from crater.commun.config import (
    CHEMIN_SOURCE_USAGES_PRODUITS,
    CHEMINS_DOSSIERS_SOURCE_BNVD,
    CHEMIN_SOURCE_DU_2019,
    DOSSIER_CONFIG,
    CHEMIN_CRATER_DATA_SOURCES,
    CHEMIN_SOURCE_COMMUNES_ET_ARRONDISSEMENTS,
    CHEMIN_DOSSIER_SOURCE_GEOMETRIES_COMMUNES,
    CHEMIN_SOURCE_MOUVEMENTS_COMMUNES,
    CHEMIN_SOURCE_EPCIS,
    DOSSIER_PRODUCTION,
    DOSSIER_SITEMAPS,
    FICHIERS_REGROUPEMENTS_COMMUNES,
    CHEMIN_SOURCE_DEPARTEMENTS,
    CHEMIN_SOURCE_REGIONS,
    CHEMIN_SOURCE_CODES_POSTAUX,
    CHEMIN_SOURCE_POPULATION_TOTALE,
    CHEMIN_SOURCE_POPULATION_TOTALE_HISTORIQUE,
    CHEMIN_SOURCE_AGRESTE_RA_2010,
    CHEMIN_SOURCE_ARTIFICIALISATION,
    CHEMIN_SOURCE_NB_LOGEMENTS,
    CHEMIN_SOURCE_NB_LOGEMENTS_VACANTS,
    CHEMIN_SOURCE_HVN,
    CHEMIN_SOURCE_TAUX_PAUVRETE_COMMUNES,
    CHEMIN_SOURCE_TAUX_PAUVRETE_SUPRA_COMMUNES,
    DOSSIER_IGN,
    DOSSIER_INTRANTS_AGRICOLES,
    URL_SOURCE_RPG,
    NOMS_FICHIERS_RPG,
    CHEMIN_OUTPUT_DATA,
    DOSSIER_TERRITOIRES,
    DOSSIER_REFERENTIEL_TERRITOIRES,
    DOSSIER_DONNEES_TERRITOIRES,
    DOSSIER_SURFACE_AGRICOLE_UTILE,
    DOSSIER_RPG,
    DOSSIER_PARCEL,
    DOSSIER_POPULATION_AGRICOLE,
    DOSSIER_POLITIQUE_FONCIERE,
    DOSSIER_PRATIQUES_AGRICOLES,
    CHEMIN_SOURCE_SAU_BIO,
    DOSSIER_PRODUCTIONS_BESOINS,
    DOSSIER_CARTES,
    CHEMIN_SOURCE_GEOMETRIES_EPCIS,
    CHEMIN_SOURCE_GEOMETRIES_REGIONS,
    CHEMIN_SOURCE_GEOMETRIES_DEPARTEMENTS,
    CHEMIN_SOURCE_DU_2017,
    CLASSIFICATIONS_SUBSTANCES_RETENUES_TOUTES_HORS_AUTRE,
    CHEMIN_SOURCE_AGRESTE_RA_2020,
    CHEMIN_SOURCE_COMMUNES_PARCEL,
    DOSSIER_TERRITOIRES_A_VALIDER_PARCEL,
    DOSSIER_BESOINS_PARCEL,
    DOSSIER_TERRITOIRES_PARCEL,
    DOSSIER_PROXIMITE_COMMERCES,
    DOSSIER_CONSOMMATION,
    CHEMIN_SOURCE_CARROYAGE_INSEE,
    DOSSIER_CRATER,
    CHEMIN_SOURCE_COMMERCES_OSM,
    CHEMIN_SOURCE_COMMERCES_BPE,
    NOM_FICHIER_SOURCE_COMMERCES_BPE,
    DOSSIER_HUBEAU,
    CHEMIN_SOURCE_PRELEVEMENTS_EAU_IRRIGATION,
    DOSSIER_EAU,
    DOSSIER_NUTRIMENTS,
    CHEMIN_SOURCE_FLUX_AZOTE,
    DOSSIER_ENERGIE,
    CHEMIN_SOURCE_CTIFL_SERRES,
)
from crater.commun.export_fichier import reinitialiser_dossier
from crater.commun.logger import log
from crater.consommation.calculateur_taux_pauvrete import calculer_indicateur_taux_pauvrete
from crater.eau.calculateur_eau import calculer_indicateurs_eau
from crater.energie.calculateur_energie import calculer_indicateurs_energie
from crater.intrants_agricoles.calculateur_intrants_agricoles import calculer_intrants_agricoles
from crater.nutriments.calculateur_azote import calculer_indicateurs_azote
from crater.politique_fonciere.calculateur_politique_fonciere import (
    calculer_politique_fonciere,
)
from crater.population_agricole.calculateur_population_agricole import (
    calculer_population_agricole,
)
from crater.pratiques_agricoles.calculateur_pratiques_agricoles import (
    calculer_pratiques_agricoles,
)
from crater.production.calculateur_production import calculer_production
from crater.productions_besoins.calculateur_productions_besoins import (
    calculer_productions_besoins,
)
from crater.proximite_commerces.indicateurs_proximite_commerces.calculateur_indicateurs_proximite_commerces import (
    calculer_indicateurs_proximite_commerces,
)
from crater.proximite_commerces.referentiel_commerces.calculateur_referentiel_commerces import (
    calculer_referentiel_commerces,
)
from crater.sitemaps.calculateur_sitemap import generer_sitemaps
from crater.surface_agricole_utile import (
    collecteur_rpg,
    calculateur_surface_agricole_utile,
)
from crater.territoires.calculateur_donnees_territoires import (
    calculer_donnees_territoires,
)
from crater.territoires.calculateur_referentiel_territoires import (
    charger_referentiel_territoires,
    calculer_referentiel_territoires,
)
from crater.territoires.chargeur_territoires import charger_territoires

duree_etapes = []
pandas.set_option("mode.chained_assignment", "raise")


def stocker_duree_etape(nom_etape, duree_etape):
    duree_etapes.append({"nom_etape": nom_etape, "duree": duree_etape})


def loguer_duree_etapes():
    for e in duree_etapes:
        duree_etape = str(round(e["duree"] / 60, 2)) + " min"
        log.info("Temps d'éxecution traitement " + e["nom_etape"] + " = " + duree_etape)


def lancer_etape_crater(etape: Callable):
    debut = time.time()
    log.info(f"#### DEBUT DE L'ETAPE {etape.__name__} ####")
    etape()
    log.info(f"#### FIN DE L'ETAPE {etape.__name__} ####")
    return {"nom_etape": etape.__name__, "duree": (time.time() - debut)}


def lancer_telechargement_parcel_id_territoires():
    generer_fichier_correspondance_territoires_parcel_crater(
        CHEMIN_SOURCE_COMMUNES_PARCEL,
        CHEMIN_OUTPUT_DATA / DOSSIER_TERRITOIRES / "referentiel_territoires.csv",
        CHEMIN_OUTPUT_DATA / DOSSIER_TERRITOIRES_A_VALIDER_PARCEL,
    )


def lancer_telechargement_parcel_besoins_assiette_actuelle():
    collecteur_besoins_api_parcel.collecter_donnees_besoins_parcel(
        ID_ASSIETTE_ACTUELLE,
        CHEMIN_OUTPUT_DATA / DOSSIER_TERRITOIRES_PARCEL / "correspondance_territoires_parcel_crater_VALIDE.csv",
        DOSSIER_CONFIG / DOSSIER_PARCEL / "parcel_produits_et_categories_202008.csv",
        CHEMIN_OUTPUT_DATA / DOSSIER_BESOINS_PARCEL / "reponse_api_parcel_besoins_sau_par_produits_assiette_actuelle.csv",
    )


def lancer_telechargement_parcel_besoins_assiette_moins_50p():
    collecteur_besoins_api_parcel.collecter_donnees_besoins_parcel(
        ID_ASSIETTE_MOINS_50_POURCENT,
        CHEMIN_OUTPUT_DATA / DOSSIER_TERRITOIRES_PARCEL / "correspondance_territoires_parcel_crater_VALIDE.csv",
        DOSSIER_CONFIG / DOSSIER_PARCEL / "parcel_produits_et_categories_202008.csv",
        CHEMIN_OUTPUT_DATA / DOSSIER_BESOINS_PARCEL / "reponse_api_parcel_besoins_sau_par_produits_assiette_moins_50p.csv",
    )


def lancer_telechargement_rpg():
    collecteur_rpg.telecharger_rpg_toutes_les_regions(
        URL_SOURCE_RPG,
        NOMS_FICHIERS_RPG,
        2017,
        CHEMIN_CRATER_DATA_SOURCES / DOSSIER_IGN,
    )


def lancer_telechargement_prelevements_eau(annee: int) -> None:
    collecter_donnees_prelevements_eau(
        annee,
        CHEMIN_CRATER_DATA_SOURCES / DOSSIER_HUBEAU,
        datetime.today(),
    )


def lancer_calcul_traitement_crater(nombre_traitements_paralleles: int, option: str):

    if option == "complet":
        supprimer_donnees_et_caches()

    lancer_calcul_referentiel_territoires()

    if option == "complet":
        lancer_calcul_surface_agricole_utile(nombre_traitements_paralleles)

    lancer_calcul_donnees_territoires()
    lancer_calcul_sitemaps()
    #  Calcul de l'eau et des nutriments fait en amont des étapes parallelisables car nécessaire pour calcul energie
    lancer_calcul_eau()
    lancer_calcul_nutriments()

    if option == "complet":
        lancer_calcul_cartes()

    etapes_parallelisables = [
        lancer_calcul_population_agricole,
        lancer_calcul_politique_fonciere,
        lancer_calcul_pratiques_agricoles,
        lancer_calcul_productions_besoins,
        lancer_calcul_intrants_agricoles,
        lancer_calcul_proximite_commerces,
        lancer_calcul_energie,
        lancer_calcul_consommation,
    ]
    duree_etapes_pool = []
    with Pool(processes=nombre_traitements_paralleles) as pool:
        duree_etapes_pool = pool.map(lancer_etape_crater, etapes_parallelisables)
    duree_etapes.extend(duree_etapes_pool)

    lancer_calcul_production()


def supprimer_donnees_et_caches():
    reinitialiser_dossier(CHEMIN_OUTPUT_DATA / DOSSIER_CRATER, True)
    reinitialiser_dossier(CHEMIN_OUTPUT_DATA / DOSSIER_SURFACE_AGRICOLE_UTILE, True)


def lancer_calcul_referentiel_territoires():
    debut = time.time()

    calculer_referentiel_territoires(
        CHEMIN_SOURCE_COMMUNES_ET_ARRONDISSEMENTS,
        CHEMIN_SOURCE_MOUVEMENTS_COMMUNES,
        CHEMIN_SOURCE_EPCIS,
        CHEMIN_SOURCE_DEPARTEMENTS,
        CHEMIN_SOURCE_REGIONS,
        CHEMIN_SOURCE_CODES_POSTAUX,
        FICHIERS_REGROUPEMENTS_COMMUNES,
        CHEMIN_OUTPUT_DATA / DOSSIER_REFERENTIEL_TERRITOIRES,
    )
    stocker_duree_etape("CALCUL_REFERENTIEL_TERRITOIRES", time.time() - debut)


def lancer_calcul_surface_agricole_utile(nombre_traitements_paralleles: int):
    debut = time.time()

    referentiel_territoires = charger_referentiel_territoires(CHEMIN_OUTPUT_DATA / DOSSIER_REFERENTIEL_TERRITOIRES)

    calculateur_surface_agricole_utile.calculer_surface_agricole_utile_par_commune_et_culture(
        2017,
        referentiel_territoires,
        CHEMIN_DOSSIER_SOURCE_GEOMETRIES_COMMUNES,
        CHEMIN_CRATER_DATA_SOURCES / DOSSIER_IGN,
        NOMS_FICHIERS_RPG,
        CHEMIN_OUTPUT_DATA / DOSSIER_SURFACE_AGRICOLE_UTILE,
        nombre_traitements_paralleles,
    )

    stocker_duree_etape("CALCUL_SURFACES_AGRICOLES_UTILES", time.time() - debut)


def lancer_calcul_donnees_territoires():
    debut = time.time()

    referentiel_territoires = charger_referentiel_territoires(CHEMIN_OUTPUT_DATA / DOSSIER_REFERENTIEL_TERRITOIRES)

    calculer_donnees_territoires(
        referentiel_territoires,
        CHEMIN_DOSSIER_SOURCE_GEOMETRIES_COMMUNES,
        CHEMIN_SOURCE_POPULATION_TOTALE,
        CHEMIN_SOURCE_POPULATION_TOTALE_HISTORIQUE,
        CHEMIN_OUTPUT_DATA / DOSSIER_SURFACE_AGRICOLE_UTILE / "sau_par_commune_et_culture",
        CHEMIN_SOURCE_AGRESTE_RA_2010,
        CHEMIN_SOURCE_AGRESTE_RA_2020,
        DOSSIER_CONFIG / DOSSIER_RPG / "correspondance_nomenclature_rpg_vers_crater.csv",
        CHEMIN_OUTPUT_DATA / DOSSIER_TERRITOIRES_PARCEL / "correspondance_territoires_parcel_crater_VALIDE.csv",
        CHEMIN_OUTPUT_DATA / DOSSIER_DONNEES_TERRITOIRES,
    )

    stocker_duree_etape("CALCUL_DONNEES_TERRITOIRES", time.time() - debut)


def lancer_calcul_cartes():

    territoires = charger_territoires(CHEMIN_OUTPUT_DATA / DOSSIER_REFERENTIEL_TERRITOIRES, CHEMIN_OUTPUT_DATA / DOSSIER_DONNEES_TERRITOIRES)
    calculer_cartes(
        territoires,
        CHEMIN_SOURCE_GEOMETRIES_REGIONS,
        CHEMIN_SOURCE_GEOMETRIES_DEPARTEMENTS,
        CHEMIN_SOURCE_GEOMETRIES_EPCIS,
        CHEMIN_DOSSIER_SOURCE_GEOMETRIES_COMMUNES,
        CHEMIN_OUTPUT_DATA / DOSSIER_CARTES,
    )


def lancer_calcul_sitemaps():

    territoires = charger_referentiel_territoires(CHEMIN_OUTPUT_DATA / DOSSIER_REFERENTIEL_TERRITOIRES)

    generer_sitemaps(
        territoires,
        CHEMIN_OUTPUT_DATA / DOSSIER_SITEMAPS,
    )


def lancer_calcul_eau():
    debut = time.time()

    territoires = charger_territoires(CHEMIN_OUTPUT_DATA / DOSSIER_REFERENTIEL_TERRITOIRES, CHEMIN_OUTPUT_DATA / DOSSIER_DONNEES_TERRITOIRES)

    calculer_indicateurs_eau(
        territoires,
        CHEMIN_SOURCE_PRELEVEMENTS_EAU_IRRIGATION,
        CHEMIN_OUTPUT_DATA / DOSSIER_EAU,
    )
    stocker_duree_etape("CALCUL_INDICATEURS_EAU", time.time() - debut)


def lancer_calcul_nutriments():
    debut = time.time()

    territoires = charger_territoires(CHEMIN_OUTPUT_DATA / DOSSIER_REFERENTIEL_TERRITOIRES, CHEMIN_OUTPUT_DATA / DOSSIER_DONNEES_TERRITOIRES)

    calculer_indicateurs_azote(
        territoires,
        CHEMIN_SOURCE_FLUX_AZOTE,
        CHEMIN_OUTPUT_DATA / DOSSIER_NUTRIMENTS,
    )
    stocker_duree_etape("CALCUL_INDICATEURS_NUTRIMENTS", time.time() - debut)


def lancer_calcul_population_agricole():

    territoires = charger_territoires(CHEMIN_OUTPUT_DATA / DOSSIER_REFERENTIEL_TERRITOIRES, CHEMIN_OUTPUT_DATA / DOSSIER_DONNEES_TERRITOIRES)

    calculer_population_agricole(
        territoires,
        CHEMIN_SOURCE_AGRESTE_RA_2010,
        CHEMIN_OUTPUT_DATA / DOSSIER_POPULATION_AGRICOLE,
    )


def lancer_calcul_politique_fonciere():

    territoires = charger_territoires(CHEMIN_OUTPUT_DATA / DOSSIER_REFERENTIEL_TERRITOIRES, CHEMIN_OUTPUT_DATA / DOSSIER_DONNEES_TERRITOIRES)

    calculer_politique_fonciere(
        territoires,
        CHEMIN_SOURCE_ARTIFICIALISATION,
        CHEMIN_SOURCE_NB_LOGEMENTS,
        CHEMIN_SOURCE_NB_LOGEMENTS_VACANTS,
        CHEMIN_OUTPUT_DATA / DOSSIER_POLITIQUE_FONCIERE,
    )


def lancer_calcul_pratiques_agricoles():

    territoires = charger_territoires(CHEMIN_OUTPUT_DATA / DOSSIER_REFERENTIEL_TERRITOIRES, CHEMIN_OUTPUT_DATA / DOSSIER_DONNEES_TERRITOIRES)

    calculer_pratiques_agricoles(
        territoires,
        CHEMIN_SOURCE_HVN,
        CHEMIN_SOURCE_SAU_BIO,
        CHEMIN_OUTPUT_DATA / DOSSIER_PRATIQUES_AGRICOLES,
    )


def lancer_calcul_productions_besoins():

    territoires = charger_territoires(CHEMIN_OUTPUT_DATA / DOSSIER_REFERENTIEL_TERRITOIRES, CHEMIN_OUTPUT_DATA / DOSSIER_DONNEES_TERRITOIRES)

    calculer_productions_besoins(
        territoires,
        CHEMIN_OUTPUT_DATA / DOSSIER_BESOINS_PARCEL / "reponse_api_parcel_besoins_sau_par_produits_assiette_actuelle.csv",
        CHEMIN_OUTPUT_DATA / DOSSIER_BESOINS_PARCEL / "reponse_api_parcel_besoins_sau_par_produits_assiette_moins_50p.csv",
        DOSSIER_CONFIG / "cultures_et_groupes_cultures.csv",
        DOSSIER_CONFIG / DOSSIER_PARCEL / "produits_hors_elevage_vers_codes_cultures.csv",
        DOSSIER_CONFIG / DOSSIER_PARCEL / "produits_elevage_vers_codes_cultures.csv",
        CHEMIN_OUTPUT_DATA / DOSSIER_SURFACE_AGRICOLE_UTILE / "sau_par_commune_et_culture",
        DOSSIER_CONFIG / DOSSIER_RPG / "correspondance_nomenclature_rpg_vers_crater.csv",
        CHEMIN_OUTPUT_DATA / DOSSIER_PRODUCTIONS_BESOINS,
    )


def lancer_calcul_production():

    calculer_production(
        CHEMIN_OUTPUT_DATA / DOSSIER_PRATIQUES_AGRICOLES / "pratiques_agricoles.csv",
        CHEMIN_OUTPUT_DATA / DOSSIER_PRODUCTIONS_BESOINS / "productions_besoins.csv",
        CHEMIN_OUTPUT_DATA / DOSSIER_PRODUCTION,
    )


def lancer_calcul_intrants_agricoles():

    territoires = charger_territoires(CHEMIN_OUTPUT_DATA / DOSSIER_REFERENTIEL_TERRITOIRES, CHEMIN_OUTPUT_DATA / DOSSIER_DONNEES_TERRITOIRES)

    calculer_intrants_agricoles(
        territoires,
        CLASSIFICATIONS_SUBSTANCES_RETENUES_TOUTES_HORS_AUTRE,
        CHEMINS_DOSSIERS_SOURCE_BNVD,
        CHEMIN_SOURCE_DU_2019,
        CHEMIN_SOURCE_DU_2017,
        CHEMIN_SOURCE_USAGES_PRODUITS,
        CHEMIN_OUTPUT_DATA / DOSSIER_INTRANTS_AGRICOLES,
    )


def lancer_calcul_proximite_commerces():

    calculer_referentiel_commerces(
        CHEMIN_SOURCE_COMMERCES_OSM,
        CHEMIN_SOURCE_COMMERCES_BPE,
        NOM_FICHIER_SOURCE_COMMERCES_BPE,
        CHEMIN_OUTPUT_DATA / DOSSIER_PROXIMITE_COMMERCES,
    )

    territoires = charger_territoires(CHEMIN_OUTPUT_DATA / DOSSIER_REFERENTIEL_TERRITOIRES, CHEMIN_OUTPUT_DATA / DOSSIER_DONNEES_TERRITOIRES)

    calculer_indicateurs_proximite_commerces(
        territoires,
        CHEMIN_OUTPUT_DATA / DOSSIER_PROXIMITE_COMMERCES / "referentiel_commerces.csv",
        CHEMIN_SOURCE_CARROYAGE_INSEE,
        CHEMIN_DOSSIER_SOURCE_GEOMETRIES_COMMUNES,
        CHEMIN_OUTPUT_DATA / DOSSIER_PROXIMITE_COMMERCES,
    )


def lancer_calcul_energie():

    territoires = charger_territoires(CHEMIN_OUTPUT_DATA / DOSSIER_REFERENTIEL_TERRITOIRES, CHEMIN_OUTPUT_DATA / DOSSIER_DONNEES_TERRITOIRES)

    calculer_indicateurs_energie(
        territoires,
        CHEMIN_OUTPUT_DATA / DOSSIER_EAU / "eau.csv",
        CHEMIN_OUTPUT_DATA / DOSSIER_SURFACE_AGRICOLE_UTILE / "sau_par_commune_et_culture",
        DOSSIER_CONFIG / DOSSIER_RPG / "correspondance_nomenclature_rpg_vers_crater.csv",
        CHEMIN_SOURCE_CTIFL_SERRES,
        CHEMIN_OUTPUT_DATA / DOSSIER_NUTRIMENTS / "flux_azote.csv",
        CHEMIN_OUTPUT_DATA / DOSSIER_ENERGIE,
    )


def lancer_calcul_consommation():

    territoires = charger_territoires(CHEMIN_OUTPUT_DATA / DOSSIER_REFERENTIEL_TERRITOIRES, CHEMIN_OUTPUT_DATA / DOSSIER_DONNEES_TERRITOIRES)

    calculer_indicateur_taux_pauvrete(
        territoires,
        CHEMIN_SOURCE_TAUX_PAUVRETE_COMMUNES,
        CHEMIN_SOURCE_TAUX_PAUVRETE_SUPRA_COMMUNES,
        CHEMIN_OUTPUT_DATA / DOSSIER_CONSOMMATION,
    )


if __name__ == "__main__":
    debut = time.time()
    parser = argparse.ArgumentParser(
        usage="%(prog)s [OPTIONS] ETAPE_CALCUL\n"
        + "Permet de lancer une étape ou un groupe d'étapes de traitements de crater-data\n"
        + "Valeurs possible pour ETAPE_CALCUL :\n"
        + "\ttelecharger-parcel-id-territoires\t\tgénérer le fichier des correspondances d'id de territoires entre PARCEL et CRATer\n"
        + (
            "\ttelecharger-parcel-besoins-assiette-actuelle\tgénérer le fichier des besoins en SAU à partir de l'api PARCEL, "
            "pour la consommation actuelle\n"
        )
        + (
            "\ttelecharger-parcel-besoins-assiette-moins-50p\tgénérer le fichier des besoins en SAU à partir de l'api PARCEL, "
            "pour une consommation avec -50 pour cent de protéines animales\n"
        )
        + "\ttelecharger-rpg\t\t\t\t\ttélécharger les fichiers régionals du RPG\n"
        + "\ttelecharger-prelevements-eau\t\t\ttélécharger les fichiers de prélèvement d'eau pour l'irrigation\n"
        + "\tcrater-complet\t\t\t\t\tgénérer l'ensemble des fichiers résultat crater-data. Exécute toutes les étapes carter-* dans l'ordre\n"
        + (
            "\tcrater-incomplet-rapide\t\t\t\tgénérer certains fichiers résultat crater-data. "
            "Exécute uniquement une partie des étapes crater-* dans l'ordre\n"
        )
        + "\tcrater-referentiel-territoires\t\t\tgénérer le referentiel des territoires\n"
        + "\tcrater-calculer-sau\t\t\t\tgénérer les surfaces agricoles utiles\n"
        + "\tcrater-donnees-territoires\t\t\tgénérer les données de territoires\n"
        + "\tcrater-eau\t\t\t\tgénérer les indicateurs eau\n"
        + "\tcrater-population-agricole\t\t\tgénérer les indicateurs population agricole\n"
        + "\tcrater-politique-fonciere\t\t\tgénérer les indicateurs politique foncière\n"
        + "\tcrater-pratiques-agricoles\t\t\tgénérer les indicateurs pratiques agricoles\n"
        + "\tcrater-productions-besoins\t\t\tgénérer les indicateurs productions-besoins\n"
        + "\tcrater-intrants-agricoles\t\t\tgénérer les indicateurs intrants-agricoles\n"
        + "\tcrater-proximite-commerces\t\t\tgénérer les indicateurs proximite-commerces\n"
        + "\tcrater-energie\t\t\t\t\tgénérer les indicateurs energie\n"
        + "\tcrater-consommation\t\t\t\tgénérer les indicateurs consommation\n"
        + "\tcrater-cartes\t\t\t\t\tgénérer les fichiers geojson contenant les contours géographiques des territoires et certains indicateurs\n"
        + "\tcrater-sitemaps\t\t\t\t\tgénérer les fichiers sitemaps (utiles pour l'ui)\n"
        + "\n"
        + "Valeurs possible pour OPTIONS :\n"
        + (
            "\t-p=NB_TRAITEMENTS_PARALLELLES\t\t Permet de paralléliser certaines étapes de calcul pour réduire le durée totale du traitement. "
            "Il est conseillé d'utiliser pour NB_TRAITEMENTS_PARALLELLES une valeur correspondant au nombre de coeurs physiques sur la machine "
            "qui exécute le traitement (par ex -p=4 sur une machine à 4 coeurs physiques et 8 coeurs logiques). "
            "Par défaut la valeur est à 1 (=pas de parallélisation)\n"
        )
    )
    parser.add_argument(
        "ETAPE_CALCUL",
        help="Nom de l'étape à exécuter",
        choices=[
            "telecharger-parcel-id-territoires",
            "telecharger-parcel-besoins-assiette-actuelle",
            "telecharger-parcel-besoins-assiette-moins-50p",
            "telecharger-rpg",
            "telecharger-prelevements-eau",
            "crater-complet",
            "crater-incomplet-rapide",
            "crater-referentiel-territoires",
            "crater-calculer-sau",
            "crater-donnees-territoires",
            "crater-eau",
            "crater-population-agricole",
            "crater-politique-fonciere",
            "crater-pratiques-agricoles",
            "crater-productions-besoins",
            "crater-intrants-agricoles",
            "crater-proximite-commerces",
            "crater-energie",
            "crater-consommation",
            "crater-cartes",
            "crater-sitemaps",
        ],
    )

    parser.add_argument("-p", "--processeurs", action="store", default=1, type=int)
    parser.add_argument("-a", "--annee", action="store", default=None, type=int)
    args = parser.parse_args()

    nombre_traitements_paralleles = args.processeurs
    annee = args.annee

    if args.ETAPE_CALCUL == "telecharger-parcel-id-territoires":
        log.info("Téléchargement et génération du fichier de correspondance des IDs de territoires entre PARCEL et CRATer à partir de l'API PARCEL")
        lancer_telechargement_parcel_id_territoires()
    elif args.ETAPE_CALCUL == "telecharger-parcel-besoins-assiette-actuelle":
        log.info(
            "Téléchargement et génération du fichier des besoins SAUs pour les territoires à partir de l'API PARCEL, pour la consommation actuelle"
        )
        lancer_telechargement_parcel_besoins_assiette_actuelle()
    elif args.ETAPE_CALCUL == "telecharger-parcel-besoins-assiette-moins-50p":
        log.info(
            "Téléchargement et génération du fichier des besoins SAUs pour les territoires à partir de l'API PARCEL, "
            "pour une consommation avec -50 pour cent de proteines animales"
        )
        lancer_telechargement_parcel_besoins_assiette_moins_50p()
    elif args.ETAPE_CALCUL == "telecharger-rpg":
        log.info("Téléchargement des fichiers source RPG")
        lancer_telechargement_rpg()
    elif args.ETAPE_CALCUL == "telecharger-prelevements-eau":
        log.info("Téléchargement des fichiers source de prélèvement d'eau pour l'irrigation")
        if annee is None:
            log.error("Erreur : le paramètre --annee doit être spécifiée dans la ligne de commande pour cette étape")
            exit(1)
        lancer_telechargement_prelevements_eau(annee)
    elif args.ETAPE_CALCUL == "crater-complet":
        log.info("Génération de tous les fichiers résultats CRATER")
        lancer_calcul_traitement_crater(nombre_traitements_paralleles, "complet")
    elif args.ETAPE_CALCUL == "crater-incomplet-rapide":
        log.info("Génération de certains fichiers résultats CRATER")
        lancer_calcul_traitement_crater(nombre_traitements_paralleles, "rapide")
    elif args.ETAPE_CALCUL == "crater-referentiel-territoires":
        log.info("Génération des fichiers résultats CRATER pour le réferentiel de territoires")
        lancer_calcul_referentiel_territoires()
    elif args.ETAPE_CALCUL == "crater-calculer-sau":
        log.info("Génération des fichiers résultats CRATER des surfaces agricoles utiles")
        lancer_calcul_surface_agricole_utile(nombre_traitements_paralleles)
    elif args.ETAPE_CALCUL == "crater-donnees-territoires":
        log.info("Génération des fichiers résultats CRATER pour les données de territoires")
        lancer_calcul_donnees_territoires()
    elif args.ETAPE_CALCUL == "crater-eau":
        log.info("Génération des fichiers résultats CRATER pour les indicateurs eau")
        lancer_calcul_eau()
    elif args.ETAPE_CALCUL == "crater-cartes":
        print("Génération des fichiers résultats CRATER pour les cartes")
        lancer_calcul_cartes()
    elif args.ETAPE_CALCUL == "crater-sitemaps":
        print("Génération des fichiers résultats CRATER pour les sitemaps")
        lancer_calcul_sitemaps()
    elif args.ETAPE_CALCUL == "crater-population-agricole":
        print("Génération des fichiers résultats CRATER pour les indicateurs population agricole")
        lancer_calcul_population_agricole()
    elif args.ETAPE_CALCUL == "crater-politique-fonciere":
        log.info("Génération des fichiers résultats CRATER pour les indicateurs politique foncière")
        lancer_calcul_politique_fonciere()
    elif args.ETAPE_CALCUL == "crater-pratiques-agricoles":
        log.info("Génération des fichiers résultats CRATER pour les indicateurs pratiques agricoles")
        lancer_calcul_pratiques_agricoles()
    elif args.ETAPE_CALCUL == "crater-productions-besoins":
        log.info("Génération des fichiers résultats CRATER pour les indicateurs productions/besoins")
        lancer_calcul_productions_besoins()
    elif args.ETAPE_CALCUL == "crater-intrants-agricoles":
        log.info("Génération des fichiers résultats CRATER pour les indicateurs intrants agricoles")
        lancer_calcul_intrants_agricoles()
    elif args.ETAPE_CALCUL == "crater-proximite-commerces":
        log.info("Génération des fichiers résultats CRATER pour les indicateurs proximité des commerces")
        lancer_calcul_proximite_commerces()
    elif args.ETAPE_CALCUL == "crater-energie":
        log.info("Génération des fichiers résultats CRATER pour les indicateurs energie")
        lancer_calcul_energie()
    elif args.ETAPE_CALCUL == "crater-consommation":
        log.info("Génération des fichiers résultats CRATER pour les indicateurs consommation")
        lancer_calcul_consommation()
    else:
        parser.print_help()

    stocker_duree_etape("TRAITEMENT_TOTAL", time.time() - debut)
    loguer_duree_etapes()
