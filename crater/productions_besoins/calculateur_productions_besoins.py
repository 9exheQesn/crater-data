from pathlib import Path

import numpy
from pandas import DataFrame

from crater.commun.export_fichier import reinitialiser_dossier
from crater.commun.logger import log
from crater.commun.outils_dataframes import merge_strict
from crater.productions_besoins.besoins import calculer_besoins
from crater.productions_besoins.export_productions_besoins import (
    exporter_df_indicateur_par_territoires,
)
from crater.productions_besoins.productions import calculer_productions


def calculer_productions_besoins(
    territoires: DataFrame,
    fichier_besoins_parcel_assiette_actuelle: Path,
    fichier_besoins_parcel_assiette_moins_50p: Path,
    fichier_cultures_et_groupes_cultures: Path,
    fichier_produits_hors_elevage_vers_cultures: Path,
    fichier_produit_elevage_vers_cultures: Path,
    fichiers_sau_productions: Path,
    fichier_cultures_rpg_vers_groupes_cultures: Path,
    chemin_dossier_resultats: Path,
) -> None:
    log.info("##### CALCUL PRODUCTIONS et BESOINS #####")

    reinitialiser_dossier(chemin_dossier_resultats)

    besoins_par_culture = calculer_besoins(
        territoires,
        fichier_besoins_parcel_assiette_actuelle,
        fichier_besoins_parcel_assiette_moins_50p,
        fichier_cultures_et_groupes_cultures,
        fichier_produits_hors_elevage_vers_cultures,
        fichier_produit_elevage_vers_cultures,
        chemin_dossier_resultats,
    )

    productions_par_culture = calculer_productions(
        territoires,
        fichiers_sau_productions,
        fichier_cultures_et_groupes_cultures,
        fichier_cultures_rpg_vers_groupes_cultures,
        chemin_dossier_resultats,
    )

    _calculer_indicateurs_productions_besoins(besoins_par_culture, productions_par_culture, chemin_dossier_resultats)


def _aggreger_par_groupe_culture(df_besoins):
    return (
        df_besoins.drop(
            columns=["code_culture", "nom_culture", "alimentation_animale"],
            errors="ignore",
        )
        .groupby(
            [
                "id_territoire",
                "categorie_territoire",
                "nom_territoire",
                "code_groupe_culture",
                "nom_groupe_culture",
            ],
            as_index=False,
        )
        .sum()
    )


def _calculer_indicateurs_productions_besoins(besoins_par_culture, productions_par_culture, chemin_dossier_resultats):
    log.info("##### CALCUL DONNEES PRODUCTIONS BESOINS #####")
    log.info("    => Chargement et calcul des indicateurs 'productions/besoins par groupe culture'")

    besoins_par_groupe_culture = _aggreger_par_groupe_culture(besoins_par_culture)
    productions_par_groupe_culture = _extraitre_productions_par_groupe_culture(productions_par_culture)

    productions_besoins_par_groupe_culture = _calculer_df_productions_besoins_par_groupe_culture(
        besoins_par_groupe_culture, productions_par_groupe_culture
    )

    log.info("    => Export des fichiers résultats 'productions/besoins par groupe culture'")
    exporter_df_indicateur_par_territoires(
        productions_besoins_par_groupe_culture,
        chemin_dossier_resultats,
        "productions_besoins_par_groupe_culture",
        tri_complementaire=["code_groupe_culture"],
    )

    log.info("    => Chargement et calcul des indicateurs 'productions/besoins'")
    productions_besoins = _calculer_df_productions_besoins(productions_besoins_par_groupe_culture)

    log.info("    => Export des fichiers résultats 'productions/besoins'")
    exporter_df_indicateur_par_territoires(productions_besoins, chemin_dossier_resultats, "productions_besoins")


def _extraitre_productions_par_groupe_culture(productions_par_culture):
    return (
        productions_par_culture.loc[
            :,
            [
                "id_territoire",
                "categorie_territoire",
                "nom_territoire",
                "code_groupe_culture",
                "nom_groupe_culture",
                "production_groupe_culture_ha",
            ],
        ]
        .drop_duplicates()
        .rename(columns={"production_groupe_culture_ha": "productions_ha"})
    )


def _calculer_df_productions_besoins(productions_besoins_par_groupe_culture):
    productions_besoins = _initialiser_df_productions_besoins(productions_besoins_par_groupe_culture)

    productions_besoins = _ajouter_colonne_taux_adequation_brut(productions_besoins, "assiette_actuelle")
    productions_besoins = _ajouter_colonne_taux_adequation_brut(productions_besoins, "assiette_demitarienne")

    return productions_besoins.loc[
        :,
        [
            "id_territoire",
            "categorie_territoire",
            "nom_territoire",
            "besoins_assiette_actuelle_ha",
            "besoins_assiette_demitarienne_ha",
            "productions_ha",
            "taux_adequation_brut_assiette_actuelle_pourcent",
            "taux_adequation_moyen_pondere_assiette_actuelle_pourcent",
            "taux_adequation_brut_assiette_demitarienne_pourcent",
            "taux_adequation_moyen_pondere_assiette_demitarienne_pourcent",
        ],
    ]


def _initialiser_df_productions_besoins(productions_besoins_par_groupe_culture):
    productions_besoins = productions_besoins_par_groupe_culture.groupby(
        ["id_territoire", "categorie_territoire", "nom_territoire"],
        as_index=False,
    ).sum(min_count=1)
    productions_besoins = productions_besoins.rename(
        columns={
            "contribution_au_taux_adequation_moyen_pondere_assiette_actuelle": "taux_adequation_moyen_pondere_assiette_actuelle_pourcent",
            "contribution_au_taux_adequation_moyen_pondere_assiette_demitarienne": "taux_adequation_moyen_pondere_assiette_demitarienne_pourcent",
        }
    )
    return productions_besoins


def _calculer_df_productions_besoins_par_groupe_culture(besoins_par_groupe_culture, productions_par_groupes_cultures):
    productions_besoins_par_groupes_cultures = _initialiser_df_productions_besoins_par_groupe_culture(
        besoins_par_groupe_culture, productions_par_groupes_cultures
    )

    productions_besoins_par_groupes_cultures = _ajouter_colonne_taux_adequation_brut(productions_besoins_par_groupes_cultures, "assiette_actuelle")
    productions_besoins_par_groupes_cultures = _ajouter_colonne_taux_adequation_brut(
        productions_besoins_par_groupes_cultures, "assiette_demitarienne"
    )

    productions_besoins_par_groupes_cultures = _ajouter_colonne_contribution_au_taux_adequation_moyen_pondere(
        productions_besoins_par_groupes_cultures
    )

    return productions_besoins_par_groupes_cultures


def _ajouter_colonne_contribution_au_taux_adequation_moyen_pondere(
    productions_besoins_par_groupes_cultures,
):
    productions_besoins_par_groupes_cultures = _ajouter_colonne_besoins_totaux_par_territoire(
        productions_besoins_par_groupes_cultures, "assiette_actuelle"
    )
    productions_besoins_par_groupes_cultures = _ajouter_colonne_besoins_totaux_par_territoire(
        productions_besoins_par_groupes_cultures, "assiette_demitarienne"
    )

    productions_besoins_par_groupes_cultures = _calculer_contribution_au_taux_adequation_moyen_pondere(
        productions_besoins_par_groupes_cultures, "assiette_actuelle"
    )
    productions_besoins_par_groupes_cultures = _calculer_contribution_au_taux_adequation_moyen_pondere(
        productions_besoins_par_groupes_cultures, "assiette_demitarienne"
    )

    productions_besoins_par_groupes_cultures = productions_besoins_par_groupes_cultures.drop(
        columns=["besoins_totaux_assiette_actuelle_ha", "besoins_totaux_assiette_demitarienne_ha"]
    )
    return productions_besoins_par_groupes_cultures


def _calculer_contribution_au_taux_adequation_moyen_pondere(productions_besoins_par_groupes_cultures, infixe_type_assiette):
    # Pour chaque groupe de culture on calcule la "part du taux d'adequation pondere"
    # En sommant ces parts sur tous les groupes de culture on pourra ensuite obtenir le taux d'adéquation pondéré
    part_besoins_dans_besoins_totaux = (
        productions_besoins_par_groupes_cultures["besoins_" + infixe_type_assiette + "_ha"]
        / productions_besoins_par_groupes_cultures["besoins_totaux_" + infixe_type_assiette + "_ha"]
    )
    productions_besoins_par_groupes_cultures["contribution_au_taux_adequation_moyen_pondere_" + infixe_type_assiette] = round(
        productions_besoins_par_groupes_cultures["taux_adequation_brut_" + infixe_type_assiette + "_pourcent"].clip(0, 100)
        * part_besoins_dans_besoins_totaux,
        2,
    )
    return productions_besoins_par_groupes_cultures


def _ajouter_colonne_besoins_totaux_par_territoire(productions_besoins_par_groupes_cultures, infixe_type_assiette):
    df_besoins_totaux_par_territoire = (
        productions_besoins_par_groupes_cultures.groupby(["id_territoire"], as_index=False)["besoins_" + infixe_type_assiette + "_ha"]
        .sum()
        .rename(columns={"besoins_" + infixe_type_assiette + "_ha": "besoins_totaux_" + infixe_type_assiette + "_ha"})
    )
    productions_besoins_par_groupes_cultures = merge_strict(
        productions_besoins_par_groupes_cultures, df_besoins_totaux_par_territoire, how="left", on="id_territoire"
    )
    return productions_besoins_par_groupes_cultures


def _initialiser_df_productions_besoins_par_groupe_culture(besoins_par_groupe_culture, productions_par_groupes_cultures):
    productions_hors_surfaces_non_cultivees = productions_par_groupes_cultures.drop(
        productions_par_groupes_cultures[productions_par_groupes_cultures["code_groupe_culture"] == "SNC"].index
    )
    productions_besoins_par_groupes_cultures = besoins_par_groupe_culture.merge(
        productions_hors_surfaces_non_cultivees,
        how="outer",
        on=[
            "id_territoire",
            "categorie_territoire",
            "nom_territoire",
            "code_groupe_culture",
            "nom_groupe_culture",
        ],
    )
    # Fill NA uniquement pour la colonne productions (par construction, cela veut dire une SAU à 0)
    # Pour les besoins, on garde les NA (NA ne veut pas dire besoin à 0 dans ce cas)
    productions_besoins_par_groupes_cultures["productions_ha"] = productions_besoins_par_groupes_cultures["productions_ha"].fillna(0)
    return productions_besoins_par_groupes_cultures


def _ajouter_colonne_taux_adequation_brut(productions_besoins, infixe_type_assiette):
    colonne_taux_adequation = "taux_adequation_brut_" + infixe_type_assiette + "_pourcent"
    productions_besoins[colonne_taux_adequation] = round(
        productions_besoins["productions_ha"] / productions_besoins["besoins_" + infixe_type_assiette + "_ha"] * 100,
        2,
    )

    productions_besoins[colonne_taux_adequation] = productions_besoins[colonne_taux_adequation].replace([numpy.inf, -numpy.inf], numpy.nan)

    return productions_besoins
