from pathlib import Path

import pandas
from pandas import DataFrame

from crater.commun.logger import log
from crater.commun.outils_dataframes import merge_strict
from crater.commun.outils_territoires import (
    ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales,
)
from crater.commun.config import ANNEE_REFERENTIEL_TERRITOIRES_PARCEL
from crater.productions_besoins.chargeur_sources_productions_besoins import (
    charger_definition_cultures_et_groupes_cultures_besoins,
    charger_besoins_parcel,
    charger_repartition_produits_elevage_sur_groupes_cultures,
    charger_produits_hors_elevage_vers_cultures_besoins_humains,
)
from crater.productions_besoins.export_productions_besoins import (
    exporter_besoins_par_culture,
)


def calculer_besoins(
    territoires: DataFrame,
    fichier_besoins_parcel_assiette_actuelle: Path,
    fichier_besoins_parcel_assiette_demitarienne: Path,
    fichier_cultures_et_groupes_cultures: Path,
    fichier_produits_hors_elevage_vers_cultures_besoins_humains: Path,
    fichier_produit_elevage_vers_cultures_besoins_animaux: Path,
    chemin_dossier_resultats: Path,
) -> DataFrame:
    log.info("##### CALCUL BESOINS #####")
    log.info("    => Chargement et calcul des indicateurs 'besoins'")

    df_besoins = _calculer_besoins_par_culture(
        fichier_besoins_parcel_assiette_actuelle,
        fichier_besoins_parcel_assiette_demitarienne,
        fichier_cultures_et_groupes_cultures,
        fichier_produit_elevage_vers_cultures_besoins_animaux,
        fichier_produits_hors_elevage_vers_cultures_besoins_humains,
        territoires,
    )

    log.info("    => Export des fichiers résultats 'besoins'")
    exporter_besoins_par_culture(df_besoins, chemin_dossier_resultats)

    return df_besoins


def _calculer_besoins_par_culture(
    fichier_besoins_parcel_assiette_actuelle,
    fichier_besoins_parcel_assiette_demitarienne,
    fichier_cultures_et_groupes_cultures,
    fichier_produit_elevage_vers_cultures_besoins_animaux,
    fichier_produits_hors_elevage_vers_cultures_besoins_humains,
    territoires,
):
    (
        df_besoins_parcel_assiette_actuelle,
        df_id_produits_parcel_assiette_actuelle,
    ) = _initialiser_df_parcel(fichier_besoins_parcel_assiette_actuelle, "assiette_actuelle")
    (
        df_besoins_parcel_assiette_demitarienne,
        df_id_produits_parcel_assiette_demitarienne,
    ) = _initialiser_df_parcel(fichier_besoins_parcel_assiette_demitarienne, "assiette_demitarienne")

    df_besoins_par_produits_assiette_actuelle = _initialiser_df_besoins(territoires, df_id_produits_parcel_assiette_actuelle)
    df_besoins_par_produits_assiette_demitarienne = _initialiser_df_besoins(territoires, df_id_produits_parcel_assiette_demitarienne)

    df_besoins_par_produits_assiette_actuelle = _ajouter_colonne_besoins(
        df_besoins_par_produits_assiette_actuelle, df_besoins_parcel_assiette_actuelle, "assiette_actuelle"
    )
    df_besoins_par_produits_assiette_demitarienne = _ajouter_colonne_besoins(
        df_besoins_par_produits_assiette_demitarienne, df_besoins_parcel_assiette_demitarienne, "assiette_demitarienne"
    )

    df_besoins_par_produits = pandas.merge(
        df_besoins_par_produits_assiette_actuelle,
        df_besoins_par_produits_assiette_demitarienne,
        how="outer",
    )

    df_besoins_par_produits = _supprimer_communes_avec_mouvement(df_besoins_par_produits)

    df_besoins_par_produits = _ajouter_colonnes_cultures_besoins_humains(
        df_besoins_par_produits,
        fichier_produits_hors_elevage_vers_cultures_besoins_humains,
    )
    df_besoins_par_cultures = _ajouter_colonnes_cultures_besoins_animaux(
        df_besoins_par_produits, fichier_produit_elevage_vers_cultures_besoins_animaux
    )
    df_besoins_par_cultures = _calculer_colonne_besoins_par_culture(df_besoins_par_cultures)
    df_besoins_par_cultures = _ajouter_colonnes_groupes_cultures(df_besoins_par_cultures, fichier_cultures_et_groupes_cultures)

    return df_besoins_par_cultures


def _supprimer_communes_avec_mouvement(df_besoins):
    df_besoins = df_besoins.loc[
        (df_besoins["annee_dernier_mouvement_commune"].isnull())
        | (df_besoins["annee_dernier_mouvement_commune"] <= ANNEE_REFERENTIEL_TERRITOIRES_PARCEL),
        :,
    ]
    return df_besoins.drop(columns=["annee_dernier_mouvement_commune"])


def _ajouter_colonne_besoins(df_besoins, df_parcel, infixe_type_assiette):
    categories_territoires_calcul_par_somme = ["EPCI", "REGROUPEMENT_COMMUNES"]
    df_besoins_recuperes_de_parcel = df_besoins.loc[
        ~df_besoins["categorie_territoire"].isin(categories_territoires_calcul_par_somme),
        :,
    ].join(df_parcel, on=["id_territoire", "id_produit_parcel"], how="inner")
    df_besoins = pandas.concat(
        [
            df_besoins.loc[
                df_besoins["categorie_territoire"].isin(categories_territoires_calcul_par_somme),
                :,
            ],
            df_besoins_recuperes_de_parcel,
        ]
    )
    df_besoins = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(
        df_besoins,
        "besoins_parcel_" + infixe_type_assiette + "_ha",
        categories_territoires_cibles=categories_territoires_calcul_par_somme,
    )
    df_besoins = df_besoins.reset_index()
    return df_besoins


def _initialiser_df_besoins(territoires, df_id_produits_parcel):
    df_besoins = territoires.reset_index().loc[
        :,
        [
            "id_territoire",
            "categorie_territoire",
            "nom_territoire",
            "id_pays",
            "id_region",
            "id_departement",
            "id_epci",
            "ids_regroupements_communes",
            "annee_dernier_mouvement_commune",
        ],
    ]
    df_besoins = df_besoins.merge(df_id_produits_parcel, how="cross").set_index(["id_territoire", "id_produit_parcel"])
    return df_besoins


def _initialiser_df_parcel(fichier_besoins_parcel, infixe_type_assiette):
    nom_colonne_besoins_parcel = "besoins_parcel_" + infixe_type_assiette + "_ha"
    df_besoins_ha = charger_besoins_parcel(fichier_besoins_parcel).round(2).rename(columns={"id_territoire_crater": "id_territoire"})
    df_besoins_ha[nom_colonne_besoins_parcel] = df_besoins_ha["SurfaceAreaBio"] * df_besoins_ha["Curseur_Min_Bio"] + df_besoins_ha["SurfaceArea"] * (
        1 - df_besoins_ha["Curseur_Min_Bio"]
    )
    df_besoins_ha = df_besoins_ha.set_index(["id_territoire", "id_produit_parcel"]).loc[:, nom_colonne_besoins_parcel]
    df_id_produits = df_besoins_ha.reset_index().loc[:, "id_produit_parcel"].drop_duplicates()
    return df_besoins_ha, df_id_produits


def _ajouter_colonnes_groupes_cultures(df_besoins, fichier_cultures_et_groupes_cultures):
    definitions_cultures_et_groupes_cultures_besoins = charger_definition_cultures_et_groupes_cultures_besoins(fichier_cultures_et_groupes_cultures)

    df_besoins = merge_strict(df_besoins, definitions_cultures_et_groupes_cultures_besoins, how="left", on="code_culture").loc[
        :,
        [
            "id_territoire",
            "categorie_territoire",
            "nom_territoire",
            "code_groupe_culture",
            "nom_groupe_culture",
            "code_culture",
            "nom_culture",
            "alimentation_animale",
            "besoins_assiette_actuelle_ha",
            "besoins_assiette_demitarienne_ha",
        ],
    ]
    return df_besoins


def _renseigner_id_region_pour_regions_et_pays(donnees_besoins):
    donnees_besoins.loc[donnees_besoins.categorie_territoire == "PAYS", "id_region"] = donnees_besoins.loc[
        donnees_besoins.categorie_territoire == "PAYS", "id_territoire"
    ]
    donnees_besoins.loc[donnees_besoins.categorie_territoire == "REGION", "id_region"] = donnees_besoins.loc[
        donnees_besoins.categorie_territoire == "REGION", "id_territoire"
    ]

    return donnees_besoins


def _ajouter_colonnes_cultures_besoins_humains(donnees_besoins, fichier_produits_hors_elevage_vers_cultures_besoins_humains):
    correspondance_produits_hors_elevage = charger_produits_hors_elevage_vers_cultures_besoins_humains(
        fichier_produits_hors_elevage_vers_cultures_besoins_humains
    ).assign(ratio_affectation_culture=1.0)
    donnees_besoins = merge_strict(donnees_besoins, correspondance_produits_hors_elevage, how="left", on="id_produit_parcel", correspondance="lache")
    return donnees_besoins


def _ajouter_colonnes_cultures_besoins_animaux(df_besoins_par_produits, fichier_repartition_produit_elevage_sur_groupes_cultures):
    repartition_produits_elevage = _construire_df_repartition_produits_elevage_sur_cultures(fichier_repartition_produit_elevage_sur_groupes_cultures)
    df_besoins_par_produits = _renseigner_id_region_pour_regions_et_pays(df_besoins_par_produits)

    df_besoins_par_cultures = df_besoins_par_produits.merge(
        repartition_produits_elevage,
        how="left",
        left_on=["id_produit_parcel", "id_region"],
        right_on=["id_produit_parcel", "id_territoire_correspondance"],
    )

    df_besoins_par_cultures["code_culture_x"].fillna(df_besoins_par_cultures["code_culture_y"], inplace=True)
    df_besoins_par_cultures["ratio_affectation_culture_x"].fillna(df_besoins_par_cultures["ratio_affectation_culture_y"], inplace=True)
    df_besoins_par_cultures = df_besoins_par_cultures.rename(columns={"code_culture_x": "code_culture"})
    df_besoins_par_cultures = df_besoins_par_cultures.rename(columns={"ratio_affectation_culture_x": "ratio_affectation_culture"})

    return df_besoins_par_cultures


def _construire_df_repartition_produits_elevage_sur_cultures(
    fichier_repartition_produit_elevage_sur_groupes_cultures,
):
    correspondance_produits_elevage = charger_repartition_produits_elevage_sur_groupes_cultures(
        fichier_repartition_produit_elevage_sur_groupes_cultures
    )
    correspondance_produits_elevage = correspondance_produits_elevage.set_index(keys=["id_produit_parcel", "code_culture"])
    # preparer l'index des colonnes avant de faire le stack
    correspondance_produits_elevage.columns = pandas.MultiIndex.from_product(
        [["ratio_affectation_culture"], list(correspondance_produits_elevage.columns)]
    )
    correspondance_produits_elevage = correspondance_produits_elevage.stack().rename_axis(index={None: "id_territoire_correspondance"})
    correspondance_produits_elevage = correspondance_produits_elevage.reset_index()
    correspondance_produits_elevage["ratio_affectation_culture"] = (
        pandas.to_numeric(correspondance_produits_elevage["ratio_affectation_culture"]) / 100
    )
    return correspondance_produits_elevage


def _calculer_colonne_besoins_par_culture(df_besoins):
    df_besoins["besoins_assiette_actuelle_ha"] = df_besoins["besoins_parcel_assiette_actuelle_ha"] * df_besoins["ratio_affectation_culture"]
    df_besoins["besoins_assiette_demitarienne_ha"] = df_besoins["besoins_parcel_assiette_demitarienne_ha"] * df_besoins["ratio_affectation_culture"]
    df_besoins = (
        df_besoins.loc[
            :,
            [
                "id_territoire",
                "categorie_territoire",
                "nom_territoire",
                "code_culture",
                "besoins_assiette_actuelle_ha",
                "besoins_assiette_demitarienne_ha",
            ],
        ]
        .groupby(
            ["id_territoire", "categorie_territoire", "nom_territoire", "code_culture"],
            as_index=False,
        )
        .sum()
    )

    return df_besoins
