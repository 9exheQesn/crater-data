import pandas


def charger_definition_cultures_et_groupes_cultures_besoins(
    fichier_cultures_et_groupes_cultures,
):
    return (
        pandas.read_csv(fichier_cultures_et_groupes_cultures, sep=";")
        .query("type_culture != 'PRODUCTION'")
        .assign(alimentation_animale=lambda df: df.type_culture == "BESOIN_ANIMAUX")
        .drop("type_culture", axis=1)
    )


def charger_besoins_parcel(fichier_besoins_parcel):
    return pandas.read_csv(
        fichier_besoins_parcel,
        sep=";",
        dtype={
            "id_territoire_crater": "str",
            "id_produit_parcel": "str",
            "SurfaceArea": "float",
            "SurfaceAreaBio": "float",
            "Curseur_Min_Bio": "float",
        },
    ).loc[
        :,
        [
            "id_territoire_crater",
            "id_produit_parcel",
            "SurfaceArea",
            "SurfaceAreaBio",
            "Curseur_Min_Bio",
        ],
    ]


def charger_repartition_produits_elevage_sur_groupes_cultures(
    fichier_repartition_produit_elevage_sur_groupes_cultures,
):
    return pandas.read_csv(fichier_repartition_produit_elevage_sur_groupes_cultures, sep=";", dtype="str").drop(columns=["nom_produit_parcel"])


def charger_produits_hors_elevage_vers_cultures_besoins_humains(
    fichier_produits_hors_elevage_vers_cultures_besoins_humains,
):
    return pandas.read_csv(
        fichier_produits_hors_elevage_vers_cultures_besoins_humains,
        sep=";",
        dtype="str",
    )


def charger(fichier_sau_production):
    return pandas.read_csv(fichier_sau_production, sep=";", dtype="str")


def charger_definition_cultures_et_groupes_cultures_productions(
    fichier_cultures_et_groupes_cultures,
):
    nomenclature_cutlures_productions = (
        pandas.read_csv(fichier_cultures_et_groupes_cultures, sep=";").query("type_culture == 'PRODUCTION'").drop("type_culture", axis=1)
    )
    return nomenclature_cutlures_productions
