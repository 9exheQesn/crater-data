import pandas

from crater.commun.config import CATEGORIES_TERRITOIRES
from crater.commun.export_fichier import exporter_df_indicateurs_par_territoires


def exporter_sau_par_culture(df_production, chemin_dossier_resultats):
    exporter_df_indicateur_par_territoires(
        df_production,
        chemin_dossier_resultats,
        "sau_par_culture",
        tri_complementaire=["code_groupe_culture", "code_culture"],
    )
    return df_production


def exporter_besoins_par_culture(df_besoins, chemin_dossier_resultats):
    exporter_df_indicateur_par_territoires(
        df_besoins,
        chemin_dossier_resultats,
        "besoins_par_culture",
        tri_complementaire=["code_culture"],
    )


def exporter_df_indicateur_par_territoires(
    df_donnees_indicateur_par_territoire,
    chemin_dossier_output,
    nom_fichier_sans_extension,
    tri_complementaire=[],
):
    df_a_exporter = df_donnees_indicateur_par_territoire.copy(deep=False)
    colonnes_pour_tri = [
        "categorie_territoire_pour_tri",
        "id_territoire",
    ] + tri_complementaire

    df_a_exporter["categorie_territoire_pour_tri"] = pandas.Categorical(
        df_a_exporter["categorie_territoire"],
        categories=CATEGORIES_TERRITOIRES[::-1],
        ordered=True,
    )

    df_a_exporter = df_a_exporter.sort_values(by=colonnes_pour_tri).drop(columns="categorie_territoire_pour_tri").set_index("id_territoire")

    exporter_df_indicateurs_par_territoires(df_a_exporter, chemin_dossier_output, nom_fichier_sans_extension)
