from pathlib import Path

import pandas
from pandas import DataFrame

from crater.commun.config import (
    ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_PRELEVEMENTS_EAU_IRRIGATION,
)
from crater.commun.export_fichier import (
    reinitialiser_dossier,
    exporter_df_indicateurs_par_territoires,
)
from crater.commun.logger import log
from crater.commun.outils_territoires import (
    traduire_code_insee_vers_id_commune_crater,
    ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales,
)


def calculer_indicateurs_eau(
    territoires: DataFrame,
    fichier_source_prelevements_eau: Path,
    chemin_dossier_output: Path,
) -> None:
    log.info("##### CALCUL EAU #####")

    reinitialiser_dossier(chemin_dossier_output)

    df_eau = territoires.loc[:, ["nom_territoire", "categorie_territoire"]].copy()
    df_eau = _ajouter_colonne_volume_m3(df_eau, territoires, fichier_source_prelevements_eau)

    exporter_df_indicateurs_par_territoires(df_eau, chemin_dossier_output, "eau")


def _ajouter_colonne_volume_m3(df_eau, territoires, fichier_source_prelevements_eau):
    df_prelevements_eau = _charger_source_prelevements_eau(fichier_source_prelevements_eau)
    df_prelevements_eau = df_prelevements_eau.loc[:, ["id_territoire", "volume"]].groupby("id_territoire").sum()
    df_prelevements_eau = df_prelevements_eau.rename(columns={"volume": "volume_m3"})
    df_prelevements_eau["volume_m3"] = df_prelevements_eau["volume_m3"].astype("Float64")

    df_irrigation = territoires.copy().join(df_prelevements_eau)
    df_irrigation = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(df_irrigation, "volume_m3")
    df_irrigation.loc[
        df_irrigation.annee_dernier_mouvement_commune > ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_PRELEVEMENTS_EAU_IRRIGATION,
        "volume_m3",
    ] = pandas.NA
    df_eau = df_eau.join(df_irrigation.loc[:, ["volume_m3"]])
    return df_eau


def _charger_source_prelevements_eau(
    fichier_source_prelevements_eau: Path,
) -> DataFrame:
    df_prelevements_eau = pandas.read_csv(
        fichier_source_prelevements_eau,
        skiprows=6,
        sep=";",
        dtype={"code_commune_insee": "str", "volume": "Float64"},
    )
    df_prelevements_eau["id_territoire"] = traduire_code_insee_vers_id_commune_crater(df_prelevements_eau["code_commune_insee"])
    return df_prelevements_eau
