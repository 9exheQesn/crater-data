# Pour éviter le warning lié à l'utilisation du format feather qui est encore en version beta
# voir https://github.com/geopandas/geo-arrow-spec
# TODO : a supprimer quand version non beta disponible
import warnings

from pathlib import Path
import geopandas

from crater.commun.export_fichier import reinitialiser_dossier
from crater.commun.logger import log

warnings.filterwarnings("ignore", message=".*Parquet.*")


def generer_et_charger_carroyage_insee_feather(chemin_fichier_carroyage_insee: Path, chemin_dossier_cache: Path):
    nom_fichier_feather = "carroyage_insee_simplifie.feather"
    chemin_fichier_feather = chemin_dossier_cache / nom_fichier_feather
    if not chemin_fichier_feather.is_file():
        log.info(
            f"    - Le fichier {chemin_fichier_feather} n'est pas présent dans le cache, génération à partir de {chemin_fichier_carroyage_insee}"
        )
        _generer_carroyage_insee_feather(
            chemin_fichier_carroyage_insee,
            chemin_dossier_cache,
            nom_fichier_feather,
        )

    log.info(f"    => Chargement carroyages à partir du fichier {chemin_fichier_feather}")
    return geopandas.read_feather(chemin_fichier_feather)


def _charger_carroyage_insee(chemin_fichier_carroyage_insee):
    log.info(f"    => Chargement carroyages population à partir du fichier {chemin_fichier_carroyage_insee}")
    gdf = geopandas.read_file(chemin_fichier_carroyage_insee).rename(
        columns={
            "Ind": "population_carroyage",
            "IdINSPIRE": "id_carreau",
        }
    )
    gdf.geometry = gdf.geometry.centroid
    gdf = gdf.loc[
        :,
        [
            "id_carreau",
            "population_carroyage",
            "geometry",
        ],
    ]
    return gdf


def _generer_carroyage_insee_feather(
    chemin_fichier_carroyage_insee: Path,
    chemin_dossier_cache: Path,
    nom_fichier_feather: str,
):
    gdf = _charger_carroyage_insee(chemin_fichier_carroyage_insee)
    reinitialiser_dossier(chemin_dossier_cache)
    chemin_fichier_feather = chemin_dossier_cache / nom_fichier_feather
    log.info(f"    - Generation du fichier {chemin_fichier_feather}")
    gdf.to_feather(chemin_fichier_feather)
