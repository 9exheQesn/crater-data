from pathlib import Path

from geopandas import GeoDataFrame
from pandas import DataFrame

from crater.commun.config import IDS_CATEGORIES_TERRITOIRES_SUPRACOMMUNAUX
from crater.commun.logger import log
from crater.commun.outils_territoires import (
    ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales,
)
from crater.proximite_commerces.config import CODES_COMMERCES
from crater.territoires.chargeur_geometries_communes import charger_geometries_communes


def calculer_indicateurs_proximite_commerces_par_type_commerce_et_territoire(
    territoires: DataFrame,
    chemin_dossier_geometries_communes: Path,
    gdf_carreaux: GeoDataFrame,
) -> DataFrame:
    df_indicateurs = _calculer_indicateurs_proximite_commerces_par_communes(territoires, gdf_carreaux, chemin_dossier_geometries_communes)
    df_indicateurs = _ajouter_colonnes_indicateurs_proximite_commerces_par_niveaux_supracommunaux(df_indicateurs)
    return df_indicateurs


def _calculer_indicateurs_proximite_commerces_par_communes(
    territoires: DataFrame,
    gdf_indicateurs_par_carreaux: GeoDataFrame,
    chemin_dossier_geometries_communes: Path,
) -> DataFrame:
    log.info("    => Calcul des indicateurs par communes")

    df_par_carreaux_et_communes = _calculer_df_par_carreaux_et_communes(chemin_dossier_geometries_communes, gdf_indicateurs_par_carreaux)

    df_par_communes = _calculer_df_par_communes(df_par_carreaux_et_communes)

    df_indicateurs = _initialiser_df_par_territoires_et_codes_commerces(territoires)
    df_indicateurs = df_indicateurs.join(df_par_communes)
    return df_indicateurs


def _calculer_df_par_communes(df_par_carreaux_et_communes):

    df_par_carreaux_et_communes["nb_carreaux"] = 1
    df_par_communes = df_par_carreaux_et_communes.groupby(["id_commune", "code_type_commerce"]).sum().reset_index()
    df_par_communes = df_par_communes.rename(columns={"carreau_dependant_voiture": "nb_carreaux_dependants_voiture"})

    df_par_communes["commune_dependante_voiture"] = (
        df_par_communes["population_dependante_voiture"] / df_par_communes["population_carroyage"]
    ) >= 0.5
    df_par_communes = df_par_communes.rename(columns={"id_commune": "id_territoire"}).set_index(["id_territoire", "code_type_commerce"])
    return df_par_communes


def _calculer_df_par_carreaux_et_communes(chemin_dossier_geometries_communes, gdf_indicateurs_par_carreaux):
    geometries_communes = charger_geometries_communes(chemin_dossier_geometries_communes)
    log.info("    - Intersection des carreaux avec les communes")
    gdf_carreaux_par_communes = geometries_communes.sjoin(gdf_indicateurs_par_carreaux, predicate="contains")
    df_par_carreaux_et_communes = DataFrame(
        gdf_carreaux_par_communes.loc[
            :,
            [
                "id_commune",
                "code_type_commerce",
                "population_carroyage",
                "distance_m",
                "population_acces_a_pied",
                "population_acces_a_velo",
                "population_dependante_voiture",
                "carreau_dependant_voiture",
            ],
        ]
    )
    df_par_carreaux_et_communes["distance_m_ponderee_par_population"] = (
        df_par_carreaux_et_communes["distance_m"] * df_par_carreaux_et_communes["population_carroyage"]
    )
    df_par_carreaux_et_communes = df_par_carreaux_et_communes.drop(columns=["distance_m"])
    return df_par_carreaux_et_communes


def _initialiser_df_par_territoires_et_codes_commerces(territoires):
    df_codes_types_commerces = DataFrame({"code_type_commerce": CODES_COMMERCES})
    df_indicateurs = (
        territoires.reset_index()
        .loc[
            :,
            ["id_territoire", "categorie_territoire", "nom_territoire"] + IDS_CATEGORIES_TERRITOIRES_SUPRACOMMUNAUX,
        ]
        .merge(df_codes_types_commerces, how="cross")
        .set_index(["id_territoire", "code_type_commerce"])
    )
    return df_indicateurs


def _ajouter_colonnes_indicateurs_proximite_commerces_par_niveaux_supracommunaux(
    df_indicateurs: DataFrame,
):
    log.info("    - Calcul des indicateurs par niveau supracommunal")

    df_indicateurs = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(df_indicateurs, "population_carroyage")
    df_indicateurs = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(df_indicateurs, "population_acces_a_pied")
    df_indicateurs = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(df_indicateurs, "population_acces_a_velo")
    df_indicateurs = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(df_indicateurs, "population_dependante_voiture")
    df_indicateurs = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(df_indicateurs, "distance_m_ponderee_par_population")

    df_indicateurs["distance_plus_proche_commerce_m"] = df_indicateurs["distance_m_ponderee_par_population"] / df_indicateurs["population_carroyage"]

    df_indicateurs = _ajouter_colonne_part_territoire_dependant_voiture_pourcent(df_indicateurs)

    df_indicateurs = _ajouter_colonnes_part_population(df_indicateurs)

    df_indicateurs = (
        df_indicateurs.loc[
            :,
            [
                "nom_territoire",
                "categorie_territoire",
                "population_carroyage",
                "distance_plus_proche_commerce_m",
                "population_acces_a_pied",
                "population_acces_a_velo",
                "population_dependante_voiture",
                "part_territoire_dependant_voiture_pourcent",
                "part_population_acces_a_pied_pourcent",
                "part_population_acces_a_velo_pourcent",
                "part_population_dependante_voiture_pourcent",
            ],
        ]
        .reset_index()
        .set_index("id_territoire")
    )
    return df_indicateurs


def _ajouter_colonne_part_territoire_dependant_voiture_pourcent(df_indicateurs):
    # calcul nb_communes_dependantes_voiture sur territoires supra communaux
    df_indicateurs["nb_communes_dependantes_voiture"] = 0
    df_indicateurs.loc[
        df_indicateurs["commune_dependante_voiture"] == True,  # noqa: E712
        "nb_communes_dependantes_voiture",
    ] = 1
    df_indicateurs = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(df_indicateurs, "nb_communes_dependantes_voiture")
    # calcul nb_communes sur territoires supra communaux, on exclut les communes sans données
    df_indicateurs["nb_communes"] = 0
    df_indicateurs.loc[~df_indicateurs["commune_dependante_voiture"].isna(), "nb_communes"] = 1
    df_indicateurs = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(df_indicateurs, "nb_communes")

    df_indicateurs["part_territoire_dependant_voiture_pourcent"] = round(
        100 * df_indicateurs["nb_communes_dependantes_voiture"] / df_indicateurs["nb_communes"]
    ).astype("Int64")
    df_indicateurs.loc[df_indicateurs["categorie_territoire"] == "COMMUNE", "part_territoire_dependant_voiture_pourcent",] = round(
        100 * df_indicateurs["nb_carreaux_dependants_voiture"] / df_indicateurs["nb_carreaux"]
    ).astype("Int64")
    return df_indicateurs


def _ajouter_colonnes_part_population(df_indicateurs):
    df_indicateurs["part_population_acces_a_pied_pourcent"] = round(
        100 * df_indicateurs["population_acces_a_pied"] / df_indicateurs["population_carroyage"]
    ).astype("Int64")
    df_indicateurs["part_population_acces_a_velo_pourcent"] = round(
        100 * df_indicateurs["population_acces_a_velo"] / df_indicateurs["population_carroyage"]
    ).astype("Int64")
    df_indicateurs["part_population_dependante_voiture_pourcent"] = round(
        100 * df_indicateurs["population_dependante_voiture"] / df_indicateurs["population_carroyage"]
    ).astype("Int64")
    return df_indicateurs
