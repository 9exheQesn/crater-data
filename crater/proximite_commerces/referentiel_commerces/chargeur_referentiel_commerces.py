import geopandas as gpd
import pandas as pd

from crater.commun.logger import log


def charger_referentiel_commerces(chemin_fichier_referentiel_commerces):
    log.info(f"    => Chargement fichier referentiel des commerces {chemin_fichier_referentiel_commerces}")
    df_commerces = pd.read_csv(
        chemin_fichier_referentiel_commerces,
        sep=";",
        dtype={"qualite_geolocalisation": str},
    )
    gdf_commerces = gpd.GeoDataFrame(
        df_commerces,
        geometry=gpd.points_from_xy(df_commerces.longitude, df_commerces.latitude, crs="EPSG:4326"),
    )
    gdf_commerces.to_crs("EPSG:2154", inplace=True)

    return gdf_commerces
