from pathlib import Path

import pandas as pd

from crater.commun.logger import log
from crater.proximite_commerces.config import (
    MAPPING_CODES_COMMERCES_ALIMENTAIRES_OSM,
)


def charger_donnees_osm(chemin_fichier_commerces_osm: Path):
    log.info(f"    => Chargement commerces OSM depuis {chemin_fichier_commerces_osm}")
    df_osm = _charger_source_osm(chemin_fichier_commerces_osm)
    return _normaliser_donnees_osm(df_osm)


def _charger_source_osm(chemin_fichier_commerces_osm: Path):
    df_osm = pd.read_csv(
        chemin_fichier_commerces_osm,
        sep=";",
        dtype={
            "code_commune": str,
            "com_insee": str,
            "code_departement": str,
            "code_region": str,
        },
    )
    return df_osm


def _normaliser_donnees_osm(df_osm):
    log.info("    => Normalisation données source OSM")

    df_osm = _exclure_commerces_non_alimentaires(df_osm)
    df_osm = df_osm.rename(columns={"name": "nom_commerce"})

    df_osm = _calculer_longitude_latitude(df_osm)
    df_osm = _calculer_type_commerce(df_osm)

    df_osm = df_osm.loc[
        :,
        [
            "nom_commerce",
            "code_type_commerce",
            "libelle_type_commerce",
            "longitude",
            "latitude",
        ],
    ]

    # Suppression des doublons pour certains cas de commerces déclarés dans 2 communes
    # Voir par exemple la boucherie F.Gueydon qui est en 2 exemplaires
    # 1 à Talence (code_commune=33522 et com_insee=33522)
    # 1 à Talence selon code_commune mais a bordeaux selon com_insee (code_commune=33522, mais com_insee=33063)
    # Vu que l'on ne prend pas en compte les champs code_commune et com_insee, on supprime les doublons
    df_osm = df_osm.drop_duplicates(
        subset=df_osm.columns.difference(["nom_commerce"]),
        ignore_index=True,
    )

    return df_osm


def _exclure_commerces_non_alimentaires(df_osm):
    return df_osm.loc[df_osm.type.isin(MAPPING_CODES_COMMERCES_ALIMENTAIRES_OSM.keys())].copy()


def _calculer_type_commerce(df_osm):
    dict_mapping_codes_commerces = {
        code_osm: dict_mapping_osm_crater["code"]
        for (
            code_osm,
            dict_mapping_osm_crater,
        ) in MAPPING_CODES_COMMERCES_ALIMENTAIRES_OSM.items()
    }
    dict_mapping_libelle_commerces = {
        code_osm: dict_mapping_osm_crater["libelle"]
        for (
            code_osm,
            dict_mapping_osm_crater,
        ) in MAPPING_CODES_COMMERCES_ALIMENTAIRES_OSM.items()
    }
    df_osm["code_type_commerce"] = df_osm.type.map(dict_mapping_codes_commerces)
    df_osm["libelle_type_commerce"] = df_osm.type.map(dict_mapping_libelle_commerces)
    return df_osm


def _calculer_longitude_latitude(df_osm):
    df_osm_ll = df_osm["Geo Point"].str.split(",", expand=True)
    df_osm["longitude"] = df_osm_ll.iloc[:, 1]
    df_osm["latitude"] = df_osm_ll.iloc[:, 0]
    return df_osm
