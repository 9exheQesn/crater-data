import zipfile
from pathlib import Path

import geopandas as gpd
import pandas as pd
from geopandas import GeoDataFrame
from pandas import DataFrame

from crater.commun.logger import log
from crater.proximite_commerces.config import (
    MAPPING_CODES_COMMERCES_ALIMENTAIRES_BPE,
)


def charger_donnees_bpe(chemin_fichier_zip_commerces_bpe: Path, nom_fichier: str):
    df_bpe = _charger_source_bpe(chemin_fichier_zip_commerces_bpe, nom_fichier)
    df_bpe = _normaliser_donnees_bpe(df_bpe)
    return df_bpe


def _charger_source_bpe(chemin_fichier_zip_commerces_bpe: Path, nom_fichier: str) -> DataFrame:
    log.info(f"    => Chargement commerces BPE depuis {chemin_fichier_zip_commerces_bpe}")
    data_bpe = pd.read_csv(
        zipfile.ZipFile(chemin_fichier_zip_commerces_bpe, "r").open(nom_fichier),
        sep=";",
        dtype={"AAV2020": str, "BV2012": str, "UU2020": str, "DEPCOM": str, "DEP": str},
    )
    return data_bpe


def _normaliser_donnees_bpe(df_bpe: DataFrame) -> DataFrame:
    log.info("    => Normalisation données BPE")
    df_bpe = _exclure_commerces_non_alimentaires_ou_non_geolocalises(df_bpe)
    df_bpe = _calculer_longitude_latitude(df_bpe)

    df_bpe = df_bpe.rename(
        columns={
            "LAMBERT_Y": "lambert_y",
            "LAMBERT_X": "lambert_x",
            "QUALITE_XY": "qualite_geolocalisation",
        }
    )

    df_bpe = _calculer_type_commerce(df_bpe)

    df_bpe = df_bpe.loc[
        :,
        [
            "nom_commerce",
            "code_type_commerce",
            "libelle_type_commerce",
            "qualite_geolocalisation",
            "longitude",
            "latitude",
        ],
    ]

    #  Il y a des doublons dans la BPE => le meme commmerce declaré sur plusieurs lignes
    df_bpe = df_bpe.drop_duplicates(
        subset=df_bpe.columns.difference(["nom_commerce"]),
        ignore_index=True,
    )
    return df_bpe


def _exclure_commerces_non_alimentaires_ou_non_geolocalises(df_bpe):
    return df_bpe.loc[df_bpe.TYPEQU.isin(MAPPING_CODES_COMMERCES_ALIMENTAIRES_BPE.keys()), :].loc[df_bpe.QUALITE_XY != "Non géolocalisé", :]


def _calculer_longitude_latitude(df_bpe: DataFrame) -> GeoDataFrame:
    gdf_bpe = gpd.GeoDataFrame(
        df_bpe,
        geometry=gpd.points_from_xy(df_bpe.LAMBERT_X, df_bpe.LAMBERT_Y, crs="EPSG:2154"),
    )
    gdf_bpe.to_crs("EPSG:4326", inplace=True)
    gdf_bpe["longitude"] = gdf_bpe.geometry.x
    gdf_bpe["latitude"] = gdf_bpe.geometry.y
    return gdf_bpe


def _calculer_type_commerce(df_bpe):
    dict_mapping_codes_commerces = {
        code_bpe: dict_mapping_bpe_crater["code"]
        for (
            code_bpe,
            dict_mapping_bpe_crater,
        ) in MAPPING_CODES_COMMERCES_ALIMENTAIRES_BPE.items()
    }
    dict_mapping_libelle_commerces = {
        code_bpe: dict_mapping_bpe_crater["libelle"]
        for (
            code_bpe,
            dict_mapping_bpe_crater,
        ) in MAPPING_CODES_COMMERCES_ALIMENTAIRES_BPE.items()
    }
    df_bpe["code_type_commerce"] = df_bpe.TYPEQU.map(dict_mapping_codes_commerces)
    df_bpe["libelle_type_commerce"] = df_bpe.TYPEQU.map(dict_mapping_libelle_commerces)
    # La BPE n'a pas le nom des commerces, on met un nom générique
    df_bpe["nom_commerce"] = df_bpe["libelle_type_commerce"] + " BPE"
    return df_bpe
