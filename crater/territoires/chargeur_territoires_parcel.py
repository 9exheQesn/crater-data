from pathlib import Path

import pandas as pd
from pandas import DataFrame

from crater.commun.logger import log


def charger(chemin_fichier: Path) -> DataFrame:
    log.info("    => Chargement territoires PARCEL %s", chemin_fichier)

    return pd.read_csv(chemin_fichier, sep=";", dtype="str")
