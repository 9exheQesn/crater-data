from crater.commun.logger import log
from crater.commun.outils_verification import verifier_absence_doublons
from crater.commun.chargeur_agreste import (
    filtrer_et_ajouter_colonne_id_territoire,
    charger_agreste,
)

DICTIONNAIRE_CHEPTELS = {
    "nb_animaux": "Elevages (total hors apiculture)(1)",
    "nb_herbivores": "Herbivores(1)",
    "nb_granivores": "Granivores(1)",
    "nb_bovins": "Total Bovins",
    "nb_bovins_vaches_laitieres": "Vaches laitières",
    "nb_bovins_vaches_allaitantes": "Vaches allaitantes",
    "nb_equides": "Total Equidés",
    "nb_caprins": "Total Caprins",
    "nb_caprins_chevres": "Chèvres",
    "nb_ovins": "Total Ovins",
    "nb_ovins_brebis_laitieres": "Brebis laitières",
    "nb_ovins_brebis_nourrices": "Brebis nourrices",
    "nb_porcins": "Total Porcins",
    "nb_porcins_truies_reproductrices": "Truies reproductrices de 50 kg ou plus",
    "nb_volailles": "Volailles",
    "nb_lapines_meres": "Lapines-mères",
}

CHEPTELS = list(DICTIONNAIRE_CHEPTELS.keys())


def charger_cheptels_agreste(chemin_dossier_zip, fichier):
    chemin_fichier_zip = chemin_dossier_zip / "FDS_G_2141.zip"

    log.info(f" => Chargement fichier AGRESTE sur la taille des cheptels depuis {chemin_fichier_zip}/{fichier}")

    df = (
        charger_agreste(chemin_fichier_zip, fichier, "G_2141")
        .rename(
            columns={
                "G_2141_LIB_DIM1": "taille_exploitation",
                "G_2141_LIB_DIM2": "cheptel",
                "G_2141_LIB_DIM3": "indicateur",
                "VALEUR": "valeur",
                "QUALITE": "qualite",
            },
            errors="raise",
        )
        .query('taille_exploitation == "Ensemble des exploitations (hors pacages collectifs)"' + ' & indicateur == "Cheptel correspondant (têtes)"')
        .reindex(
            columns=[
                "FRANCE",
                "FRDOM",
                "REGION",
                "DEP",
                "COM",
                "taille_exploitation",
                "cheptel",
                "indicateur",
                "valeur",
                "qualite",
            ]
        )
        .astype({"valeur": "Int64"})
    )

    df = filtrer_et_ajouter_colonne_id_territoire(df)
    df = df.pivot(index="id_territoire", columns=["indicateur", "cheptel"], values="valeur").reset_index()
    df.columns = df.columns.droplevel(0)
    df.rename(columns={"": "id_territoire"}, inplace=True)
    for i in DICTIONNAIRE_CHEPTELS:
        df.rename(columns={DICTIONNAIRE_CHEPTELS[i]: i}, inplace=True)
    verifier_absence_doublons(df, "id_territoire")

    return df
