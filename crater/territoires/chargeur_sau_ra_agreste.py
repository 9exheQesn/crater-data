from pathlib import Path

import pandas

from crater.commun.chargeur_agreste import charger_agreste
from crater.commun.logger import log
from crater.commun.outils_territoires import traduire_code_insee_vers_id_commune_crater
from crater.commun.outils_verification import verifier_absence_doublons
from crater.population_agricole.chargeur_sources_population_agricole import (
    filtrer_et_ajouter_colonne_id_territoire,
)


def charger_sau_ra_2010(chemin_dossier_agreste_2010: Path):
    log.info(
        "    => Chargement fichier AGRESTE des SAU depuis %s/FDS_G_1013.zip/FDS_G_1013_2010.txt",
        chemin_dossier_agreste_2010,
    )

    df = charger_agreste(chemin_dossier_agreste_2010 / "FDS_G_1013.zip", "FDS_G_1013_2010.txt", "G_1013")
    df = (
        df.rename(
            columns={
                "G_1013_LIB_DIM1": "taille_exploitation",
                "G_1013_LIB_DIM2": "type_de_culture",
                # pas de colonnes xxx_DIM3 dans le fichier FDS_G_1013_2010, les indicateurs sont dans les colonnes xxx_DIM4
                "G_1013_LIB_DIM4": "indicateur",
                "VALEUR": "valeur",
                "QUALITE": "qualite",
            },
            errors="raise",
        )
        .query(
            'taille_exploitation  == "Ensemble des exploitations (hors pacages collectifs)"'
            + ' & type_de_culture  in ["Superficie agricole utilisée (1)", "Superficie toujours en herbe (STH)"]'
            + ' & indicateur == "Superficie correspondante (hectares)"'
        )
        .reindex(
            columns=[
                "FRANCE",
                "FRDOM",
                "REGION",
                "DEP",
                "COM",
                "taille_exploitation",
                "type_de_culture",
                "indicateur",
                "valeur",
                "qualite",
            ]
        )
        .astype({"valeur": "float64"})
    )

    df = filtrer_et_ajouter_colonne_id_territoire(df)
    df = df.pivot(
        index="id_territoire",
        columns=["indicateur", "type_de_culture"],
        values="valeur",
    ).reset_index()
    df = df.set_index("id_territoire")
    df.columns = df.columns.droplevel()
    df = df.rename(
        columns={
            "Superficie agricole utilisée (1)": "sau_ra_2010_ha",
            "Superficie toujours en herbe (STH)": "sau_prairies_permanentes_ra_2010_ha",
        }
    )

    df = df.reset_index("id_territoire")
    verifier_absence_doublons(df, "id_territoire")

    return df


def charger_sau_ra_2020(chemin_fichier_agreste_2020: Path):
    log.info(f"    => Chargement fichier AGRESTE des SAU depuis {chemin_fichier_agreste_2020}")
    df = pandas.read_csv(chemin_fichier_agreste_2020, sep=";", skiprows=2, dtype={"Code": "str"}).rename(columns={"SAU en 2020": "sau_ra_2020_ha"})
    df["id_territoire"] = traduire_code_insee_vers_id_commune_crater(df["Code"])
    return df.loc[:, ["id_territoire", "sau_ra_2020_ha"]]
