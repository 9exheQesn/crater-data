from pathlib import Path

import pandas as pd
from pandas import DataFrame

from crater.commun.logger import log
from crater.commun.outils_territoires import traduire_code_insee_vers_id_region_crater
from crater.commun.outils_verification import verifier_absence_doublons


def charger(chemin_fichier: Path) -> DataFrame:
    log.info("    => Chargement régions depuis %s", chemin_fichier)

    df = (
        pd.read_csv(
            chemin_fichier,
            sep=",",
            dtype={
                "reg": "str",
                "libelle": "str",
                "ncc": "str",
                # Noms de colonnes en majuscule dans le fichier source à partir de 2021 => on prend les 2 formats pour retro compatibilité
                "REG": "str",
                "LIBELLE": "str",
                "NCC": "str",
            },
            na_values=[""],
        )
        .rename(
            columns={
                "reg": "id_region",
                "libelle": "nom_region",
                "ncc": "nom_region_majuscules",
                "REG": "id_region",
                "LIBELLE": "nom_region",
                "NCC": "nom_region_majuscules",
            },
        )
        .reindex(columns=["id_region", "nom_region", "nom_region_majuscules"])
    )

    df = _traduire_code_insee_vers_id_crater(df)
    verifier_absence_doublons(df, "id_region")

    return df


def _traduire_code_insee_vers_id_crater(df: DataFrame) -> DataFrame:
    df.id_region = traduire_code_insee_vers_id_region_crater(df.id_region)
    return df
