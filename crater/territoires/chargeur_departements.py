from pathlib import Path

import pandas as pd
from pandas import DataFrame

from crater.commun.logger import log
from crater.commun.outils_territoires import (
    traduire_code_insee_vers_id_departement_crater,
    traduire_code_insee_vers_id_region_crater,
)
from crater.commun.outils_verification import verifier_absence_doublons


def charger(chemin_fichier: Path) -> DataFrame:
    log.info("    => Chargement départements depuis %s", chemin_fichier)
    df = (
        pd.read_csv(
            chemin_fichier,
            sep=",",
            dtype={
                "dep": "str",
                "reg": "str",
                "libelle": "str",
                # Noms de colonnes en majuscule dans le fichier source à partir de 2021 => on prend les 2 formats pour retro compatibilité
                "DEP": "str",
                "REG": "str",
                "LIBELLE": "str",
            },
            na_values=[""],
        )
        .rename(
            columns={
                "dep": "id_departement",
                "reg": "id_region",
                "libelle": "nom_departement",
                "DEP": "id_departement",
                "REG": "id_region",
                "LIBELLE": "nom_departement",
            },
        )
        .reindex(columns=["id_departement", "nom_departement", "id_region"])
    )

    df = _traduire_code_insee_vers_id_crater(df)
    verifier_absence_doublons(df, "id_departement")

    return df


def _traduire_code_insee_vers_id_crater(df: DataFrame) -> DataFrame:
    df.id_departement = traduire_code_insee_vers_id_departement_crater(df.id_departement)
    df.id_region = traduire_code_insee_vers_id_region_crater(df.id_region)
    return df
