from pathlib import Path

import pandas as pd
from pandas import DataFrame

from crater.commun.logger import log
from crater.commun.outils_territoires import traduire_code_insee_vers_id_epci_crater
from crater.commun.outils_verification import verifier_absence_doublons


def charger(chemin_fichier: Path) -> DataFrame:
    log.info("    => Chargement EPCIs depuis %s", chemin_fichier)

    df = (
        pd.read_excel(
            chemin_fichier,
            sheet_name="EPCI",
            usecols="A:C",
            skiprows=5,
            dtype={"EPCI": "str", "LIBEPCI": "str", "NATURE_EPCI": "str"},
            na_values=[""],
        )
        .rename(
            columns={
                "EPCI": "id_epci",
                "LIBEPCI": "nom_epci",
                "NATURE_EPCI": "categorie_epci",
            },
            errors="raise",
        )
        .reindex(columns=["id_epci", "nom_epci", "categorie_epci"])
        .replace(
            {
                "categorie_epci": {
                    "CC": "COMMUNAUTE_COMMUNES",
                    "CA": "COMMUNAUTE_AGGLOMERATION",
                    "CU": "COMMUNAUTE_URBAINE",
                    "ME": "METROPOLE",
                    "ZZ": "SANS_EPCI",
                }
            }
        )
    )

    df = _traduire_code_insee_vers_id_crater(df)
    verifier_absence_doublons(df, "id_epci")

    return df


def _traduire_code_insee_vers_id_crater(df: DataFrame) -> DataFrame:
    df.id_epci = traduire_code_insee_vers_id_epci_crater(df.id_epci)
    return df
