from pathlib import Path
import geopandas as gpd
import numpy as np
import pandas as pd
from pandas import DataFrame

from crater.commun.config import (
    IDS_CATEGORIES_TERRITOIRES_SUPRACOMMUNAUX,
    NUMEROS_DEPARTEMENTS_DROM,
    ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_AGRESTE_RA_2010,
    ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_AGRESTE_RA_2020,
    ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_POPULATION_TOTALE,
    ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_POPULATION_TOTALE_HISTORIQUE,
)
from crater.commun.export_fichier import reinitialiser_dossier, exporter_df_indicateurs_par_territoires
from crater.commun.logger import log
from crater.commun.outils_dataframes import DictDataFrames, merge_strict
from crater.commun.outils_territoires import (
    ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales,
    obtenir_arrondissements_depuis_territoires,
    calculer_donnees_infraterritoriales_par_repartition_donnees_supraterritoriales,
)
from crater.commun.outils_verification import (
    verifier_coherence_referentiel_avec_geometries_communes,
)
from crater.commun.outils_dataframes import update_colonne_depuis_series_incluant_NAs
from crater.surface_agricole_utile import chargeur_sau_par_commune_et_culture
from crater.territoires import (
    chargeur_geometries_communes,
    chargeur_population_totale,
    chargeur_population_totale_historique,
    chargeur_territoires_parcel,
    chargeur_cheptels_agreste,
)
from crater.territoires.calculateur_referentiel_territoires import (
    ajouter_id_commune_de_arrondissement,
)
from crater.territoires.chargeur_sau_ra_agreste import (
    charger_sau_ra_2010,
    charger_sau_ra_2020,
)
from crater.territoires.otex.calculateur_otex_territoires import calculer_otex_territoires


CODE_CATEGORIE_SAU_PRODUCTIVE = "PRODUCTIVE"
CODE_CATEGORIE_SAU_PEU_PRODUCTIVE = "PEU_PRODUCTIVE"


def calculer_donnees_territoires(
    referentiel_territoires: DataFrame,
    chemin_fichiers_geometries_communes: Path,
    chemin_fichier_population_totale: Path,
    chemin_fichier_population_totale_historique: Path,
    chemin_fichiers_sau: Path,
    chemin_dossier_agreste_2010: Path,
    chemin_fichier_agreste_2020: Path,
    chemin_fichier_correspondance_nomenclature_rpg_vers_crater: Path,
    chemin_fichier_correspondance_territoires_parcel_crater: Path,
    chemin_dossier_output: Path,
) -> None:
    sources_donnees_territoires = _charger_sources_donnees_territoires(
        chemin_fichiers_geometries_communes,
        chemin_fichier_population_totale,
        chemin_fichier_population_totale_historique,
        chemin_fichiers_sau,
        chemin_dossier_agreste_2010,
        chemin_fichier_agreste_2020,
        chemin_fichier_correspondance_nomenclature_rpg_vers_crater,
        chemin_fichier_correspondance_territoires_parcel_crater,
    )
    log.info("##### CALCUL DES DONNEES TERRITOIRES #####")

    reinitialiser_dossier(chemin_dossier_output)

    verifier_coherence_referentiel_avec_geometries_communes(referentiel_territoires, sources_donnees_territoires["geometries_communes"])

    territoires_enrichi = _ajouter_colonnes_donnees_territoires(referentiel_territoires, sources_donnees_territoires)
    territoires_enrichi = territoires_enrichi.drop(columns=territoires_enrichi.loc[:, slice("id_pays", "type_mouvement_commune")].columns)

    exporter_df_indicateurs_par_territoires(territoires_enrichi, chemin_dossier_output, "donnees_territoires")

    calculer_otex_territoires(referentiel_territoires, chemin_fichier_agreste_2020, chemin_dossier_output)


def charger_donnees_territoires(dossier_donnees_territoires: Path) -> DataFrame:
    log.info("##### CHARGEMENT DES DONNEES TERRITOIRES #####")

    donnees_territoires = pd.read_csv(
        dossier_donnees_territoires / "donnees_territoires.csv",
        sep=";",
        dtype={
            "id_territoire": "str",
            "nom_territoire": "str",
            "categorie_territoire": "str",
            "nb_communes": "int",
            "superficie_ha": "float",
            "sau_ha": "float",
            "population_totale_2017": "Int64",
            "population_totale_2010": "Int64",
            "population_totale_1990": "Int64",
            "id_territoire_parcel": "str",
        },
        na_values=[""],
    ).round({"sau_ha": 2})

    return donnees_territoires


def _ajouter_colonnes_donnees_territoires(referentiel_territoires: DataFrame, sources: DictDataFrames) -> DataFrame:
    log.info("##### AJOUT DES DONNEES COMPLÉMENTAIRES DES TERRITOIRES #####")

    territoires = _ajouter_nombre_de_communes_par_territoire(referentiel_territoires)

    territoires = _ajouter_colonne_superficie(referentiel_territoires, sources["geometries_communes"])

    territoires = _ajouter_colonnes_sau(
        territoires,
        sources["sau"],
        sources["correspondance_nomenclature_rpg_vers_crater"],
    )

    territoires = _ajouter_colonnes_sau_ra_2010(territoires, sources["sau_ra_2010"])

    territoires = _ajouter_colonne_sau_ra_2020(territoires, sources["sau_ra_2020"])

    territoires = _ajouter_colonnes_population(territoires, sources)

    territoires = _ajouter_colonne_id_territoire_parcel(territoires, sources["territoires_parcel"])

    noms_cheptels = sources["cheptels"].columns.to_list()[1:]
    territoires = _ajouter_colonnes_cheptels(territoires, sources["cheptels"], noms_cheptels)
    territoires = _completer_colonnes_cheptels(territoires, noms_cheptels)

    return territoires


def _ajouter_nombre_de_communes_par_territoire(territoires: DataFrame) -> DataFrame:
    territoires["nb_communes"] = 0

    territoires.loc[territoires["categorie_territoire"] == "COMMUNE", "nb_communes"] = 1

    for id_echelle in IDS_CATEGORIES_TERRITOIRES_SUPRACOMMUNAUX:
        territoires.update(territoires.loc[territoires["categorie_territoire"] == "COMMUNE"].groupby(id_echelle).size().rename("nb_communes"))

    regroupements_communes = pd.DataFrame(territoires.ids_regroupements_communes.str.split("|"))
    regroupements_communes = regroupements_communes.explode("ids_regroupements_communes")
    territoires.update(regroupements_communes.groupby("ids_regroupements_communes").size().rename("nb_communes"))

    territoires["nb_communes"] = territoires["nb_communes"].astype("int")

    return territoires


def _completer_colonnes_cheptels(territoires: DataFrame, noms_cheptels: list[str]) -> DataFrame:

    cheptels_estimes = territoires.copy()

    for nom_cheptel in noms_cheptels:
        cheptel = territoires.loc[
            :,
            [
                "categorie_territoire",
                "id_pays",
                "id_region",
                "id_departement",
                "sau_ra_2020_ha",
                nom_cheptel,
            ],
        ]
        cheptel = calculer_donnees_infraterritoriales_par_repartition_donnees_supraterritoriales(
            cheptel, nom_cheptel, "sau_ra_2020_ha", "REGION", "PAYS"
        )
        cheptel = calculer_donnees_infraterritoriales_par_repartition_donnees_supraterritoriales(
            cheptel,
            nom_cheptel,
            "sau_ra_2020_ha",
            "DEPARTEMENT",
            "REGION",
        )
        cheptel = calculer_donnees_infraterritoriales_par_repartition_donnees_supraterritoriales(
            cheptel,
            nom_cheptel,
            "sau_ra_2020_ha",
            "COMMUNE",
            "DEPARTEMENT",
        )
        cheptels_estimes = update_colonne_depuis_series_incluant_NAs(cheptels_estimes, nom_cheptel, cheptel[nom_cheptel])
        cheptels_estimes = cheptels_estimes.join(cheptel[nom_cheptel + "_est_estime"])

        cheptels_estimes.loc[
            (cheptels_estimes[nom_cheptel + "_part_na_pourcent"] > 0)
            & cheptels_estimes["categorie_territoire"].isin(["EPCI", "REGROUPEMENT_COMMUNES"]),
            nom_cheptel + "_est_estime",
        ] = True
        cheptels_estimes = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(
            territoires=cheptels_estimes,
            colonne_a_sommer=nom_cheptel,
            categorie_territoire_a_sommer="COMMUNE",
            categories_territoires_cibles=["EPCI", "REGROUPEMENT_COMMUNES"],
        )
        cheptels_estimes.drop(columns=nom_cheptel + "_part_na_pourcent", inplace=True)

    noms_cheptels_ordonnes = sorted(noms_cheptels, key=chargeur_cheptels_agreste.CHEPTELS.index)
    colonnes_cheptels_ordonnees: list[str] = sum([[i, i + "_est_estime"] for i in noms_cheptels_ordonnes], [])
    colonnes_hors_cheptels = cheptels_estimes.loc[:, slice("nom_territoire", "id_territoire_parcel")].columns
    cheptels_estimes = cheptels_estimes.reindex(columns=[*colonnes_hors_cheptels, *colonnes_cheptels_ordonnees])

    return cheptels_estimes


def _ajouter_colonnes_cheptels(territoires: DataFrame, cheptels: DataFrame, noms_cheptels: list[str]) -> DataFrame:
    cheptels = cheptels.set_index("id_territoire")
    territoires = territoires.join(cheptels)

    for nom_cheptel in noms_cheptels:

        territoires.loc[
            territoires.annee_dernier_mouvement_commune > ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_AGRESTE_RA_2010,
            nom_cheptel,
        ] = np.nan

        territoires = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(
            territoires=territoires,
            colonne_a_sommer=nom_cheptel,
            categorie_territoire_a_sommer="COMMUNE",
            categories_territoires_cibles=["EPCI", "REGROUPEMENT_COMMUNES"],
            compter_part_valeurs_na=True,
        )
    return territoires


def _ajouter_colonnes_population(territoires, sources) -> DataFrame:
    territoires = _ajouter_colonne_population_totale(
        territoires, sources["population_totale"], ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_POPULATION_TOTALE
    )

    territoires = _ajouter_colonne_population_totale_historique(
        territoires, sources["population_totale_historique"], ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_POPULATION_TOTALE_HISTORIQUE
    )

    return territoires


def _ajouter_colonne_population_totale(territoires: DataFrame, population_totale: DataFrame, annee: int) -> DataFrame:
    # les populations totales sont donnés par arrondissements ou communes quand celle-ci n'a pas d'arrondissements
    # on doit donc remplacer l'id arrondissement par l'id de sa commune le cas échéant
    population_totale_final = population_totale.copy()
    arrondissements = obtenir_arrondissements_depuis_territoires(territoires)
    population_totale_final = (
        ajouter_id_commune_de_arrondissement(population_totale_final, arrondissements)
        .groupby(["id_commune"], as_index=False)
        .agg({"population_totale_2017": "sum"})
        .rename(columns={"id_commune": "id_territoire"})
        .set_index("id_territoire")
    )

    territoires = territoires.join(population_totale_final)

    territoires.loc[territoires.annee_dernier_mouvement_commune > annee, "population_totale_2017"] = np.nan

    territoires = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(territoires, "population_totale_2017")

    return territoires


def _ajouter_colonne_population_totale_historique(territoires: DataFrame, population_totale_historique: DataFrame, annee: int) -> DataFrame:
    population_totale_historique_final = (
        population_totale_historique.rename(columns={"id_commune": "id_territoire"}).set_index("id_territoire").drop(columns="nom_commune")
    )

    territoires = territoires.join(population_totale_historique_final)

    territoires.loc[territoires.annee_dernier_mouvement_commune > annee, "population_totale_2010"] = np.nan
    territoires.loc[territoires.annee_dernier_mouvement_commune > annee, "population_totale_1990"] = np.nan

    territoires = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(territoires, "population_totale_2010")
    territoires = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(territoires, "population_totale_1990")

    return territoires


def _ajouter_colonne_superficie(territoires: DataFrame, geometries_communes: gpd.GeoDataFrame) -> DataFrame:
    geometries_communes = geometries_communes.rename(columns={"id_commune": "id_territoire", "surf_ha": "superficie_ha"}).set_index("id_territoire")
    territoires = territoires.join(geometries_communes[["superficie_ha"]])

    territoires = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(territoires, "superficie_ha")

    return territoires


def _ajouter_colonne_id_territoire_parcel(territoires: DataFrame, territoires_parcel: DataFrame) -> DataFrame:
    id_territoires_parcel = territoires_parcel.loc[:, ["id_territoire", "id_territoire_parcel"]].set_index("id_territoire")

    territoires = territoires.join(id_territoires_parcel)

    return territoires


def _ajouter_colonnes_sau(
    territoires: DataFrame,
    sau_par_commune_et_code_rpg: DataFrame,
    correspondance_nomenclature_rpg_vers_crater: DataFrame,
) -> DataFrame:
    sau_par_commune_et_categorie = merge_strict(
        sau_par_commune_et_code_rpg.loc[:, ["id_commune", "code_culture_rpg", "sau_ha"]],
        correspondance_nomenclature_rpg_vers_crater.loc[:, ["code_culture_rpg", "categorie_sau"]],
        on="code_culture_rpg",
        how="left",
    )

    sau_par_commune_et_categorie["sau_productive_ha"] = sau_par_commune_et_categorie.loc[
        sau_par_commune_et_categorie["categorie_sau"] == CODE_CATEGORIE_SAU_PRODUCTIVE,
        "sau_ha",
    ]

    sau_par_commune_et_categorie["sau_peu_productive_ha"] = sau_par_commune_et_categorie.loc[
        sau_par_commune_et_categorie["categorie_sau"] == CODE_CATEGORIE_SAU_PEU_PRODUCTIVE,
        "sau_ha",
    ]

    territoires = _ajouter_colonne_sau(territoires, sau_par_commune_et_categorie, "sau_ha")
    territoires = _ajouter_colonne_sau(territoires, sau_par_commune_et_categorie, "sau_productive_ha")
    territoires = _ajouter_colonne_sau(territoires, sau_par_commune_et_categorie, "sau_peu_productive_ha")

    return territoires


def _ajouter_colonne_sau(territoires, sau_enrichie_nomenclature_crater, nom_colonne_sau_resultat) -> DataFrame:
    sau_par_commune = (
        sau_enrichie_nomenclature_crater.loc[:, ["id_commune", nom_colonne_sau_resultat]]
        .groupby(["id_commune"], as_index=False)
        .sum()
        .rename(columns={"id_commune": "id_territoire"})
        .set_index("id_territoire")
    )

    territoires = territoires.join(sau_par_commune)

    territoires.update(territoires.loc[territoires.categorie_territoire == "COMMUNE", nom_colonne_sau_resultat].fillna(0))

    territoires = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(territoires, nom_colonne_sau_resultat)

    return territoires


def _ajouter_colonnes_sau_ra_2010(territoires, sau_ra_2010):
    sau_ra_2010["sau_hors_prairies_permanentes_ra_2010_ha"] = sau_ra_2010["sau_ra_2010_ha"] - sau_ra_2010["sau_prairies_permanentes_ra_2010_ha"]
    territoires = territoires.join(sau_ra_2010.set_index("id_territoire").loc[:, ["sau_ra_2010_ha", "sau_hors_prairies_permanentes_ra_2010_ha"]])

    territoires = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(
        territoires,
        "sau_ra_2010_ha",
        categorie_territoire_a_sommer="COMMUNE",
        categories_territoires_cibles=["EPCI", "REGROUPEMENT_COMMUNES"],
    )

    territoires = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(
        territoires,
        "sau_hors_prairies_permanentes_ra_2010_ha",
        categorie_territoire_a_sommer="COMMUNE",
        categories_territoires_cibles=["EPCI", "REGROUPEMENT_COMMUNES"],
    )

    territoires.loc[
        territoires.annee_dernier_mouvement_commune > ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_AGRESTE_RA_2010,
        "sau_ra_2010_ha",
    ] = np.nan
    territoires.loc[
        territoires.annee_dernier_mouvement_commune > ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_AGRESTE_RA_2010,
        "sau_hors_prairies_permanentes_ra_2010_ha",
    ] = np.nan

    return territoires


def _ajouter_colonne_sau_ra_2020(territoires, sau_ra_2020):
    territoires = territoires.join(sau_ra_2020.set_index("id_territoire"))

    territoires = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(
        territoires,
        "sau_ra_2020_ha",
    )
    territoires.loc[territoires.annee_dernier_mouvement_commune > ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_AGRESTE_RA_2020, "sau_ra_2020_ha"] = np.nan

    return territoires


def _charger_sources_donnees_territoires(
    chemin_fichiers_geometries_communes: Path,
    chemin_fichier_population_totale: Path,
    chemin_fichier_population_totale_historique: Path,
    chemin_fichiers_sau: Path,
    chemin_dossier_agreste_2010: Path,
    chemin_fichier_agreste_2020: Path,
    chemin_fichier_correspondance_nomenclature_rpg_vers_crater: Path,
    chemin_fichier_correspondance_territoires_parcel_crater: Path,
) -> DictDataFrames:
    log.info("##### CHARGEMEMENT DES DONNEES COMPLEMENTAIRES DES TERRITOIRES #####")

    geometries_communes = chargeur_geometries_communes.charger_geometries_communes(chemin_fichiers_geometries_communes)

    sau_rpg = chargeur_sau_par_commune_et_culture.charger(chemin_fichiers_sau)
    correspondance_nomenclature_rpg_vers_crater = pd.read_csv(chemin_fichier_correspondance_nomenclature_rpg_vers_crater, sep=";", dtype="str")

    sau_ra_2010 = charger_sau_ra_2010(chemin_dossier_agreste_2010)
    sau_ra_2020 = charger_sau_ra_2020(chemin_fichier_agreste_2020)

    population_totale = chargeur_population_totale.charger(chemin_fichier_population_totale)
    population_totale_historique = chargeur_population_totale_historique.charger(chemin_fichier_population_totale_historique)

    territoires_parcel = chargeur_territoires_parcel.charger(chemin_fichier_correspondance_territoires_parcel_crater)

    population_totale, population_totale_historique = _supprimer_DROMs(population_totale, population_totale_historique)

    cheptels = chargeur_cheptels_agreste.charger_cheptels_agreste(chemin_dossier_agreste_2010, "FDS_G_2141_2010.txt")

    return {
        "geometries_communes": geometries_communes,
        "population_totale": population_totale,
        "population_totale_historique": population_totale_historique,
        "sau": sau_rpg,
        "sau_ra_2010": sau_ra_2010,
        "sau_ra_2020": sau_ra_2020,
        "correspondance_nomenclature_rpg_vers_crater": correspondance_nomenclature_rpg_vers_crater,
        "territoires_parcel": territoires_parcel,
        "cheptels": cheptels,
    }


def _supprimer_DROMs(population_totale: DataFrame, population_totale_historique: DataFrame) -> tuple[DataFrame, DataFrame]:
    # TODO: both type issues "passing Callable to .loc[]" solved in 1.5.3 https://github.com/pandas-dev/pandas-stubs/issues/256
    population_totale = population_totale.loc[lambda df: ~df["id_commune_ou_arrondissement"].str[0:2].isin(NUMEROS_DEPARTEMENTS_DROM)]  # type: ignore
    population_totale_historique = population_totale_historique.loc[
        lambda df: ~df["id_commune"].str[0:2].isin(NUMEROS_DEPARTEMENTS_DROM)
    ]  # type: ignore[call-overload]

    return population_totale, population_totale_historique
