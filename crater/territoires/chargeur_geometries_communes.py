from pathlib import Path

import geopandas as gpd

from crater.commun.config import CRS_PROJET
from crater.commun.logger import log
from crater.commun.outils_territoires import traduire_code_insee_vers_id_commune_crater
from crater.commun.outils_verification import verifier_absence_doublons


def charger_geometries_communes(chemin_dossier: Path, crs=CRS_PROJET) -> gpd.GeoDataFrame:
    log.info("    => Chargement des géométries des communes depuis %s", chemin_dossier)

    geometries_communes = gpd.read_file(chemin_dossier).to_crs(crs)

    geometries_communes = _traduire_code_insee_vers_id_crater(geometries_communes)
    verifier_absence_doublons(geometries_communes, "id_commune")

    if chemin_dossier.name == "communes-20200101":
        geometries_communes = _corriger_code_insee_Bernwiller(geometries_communes)
    elif chemin_dossier.name == "communes-20210101":
        geometries_communes = _corriger_code_insee_Les_Trois_Lacs(geometries_communes)
    elif chemin_dossier.name == "communes-20220101":
        pass
    elif chemin_dossier.name == "geometries_communes_test":
        pass
    else:
        raise ValueError(
            "ERREUR lors du chargement des géométries des communes. Le fichier source n'est pas connu. "
            "Il faut l'ajouter en renseignant si besoin les traitements à effectuer."
        )

    return geometries_communes


def _traduire_code_insee_vers_id_crater(
    geometries_communes: gpd.GeoDataFrame,
) -> gpd.GeoDataFrame:
    geometries_communes.rename(columns={"insee": "id_commune"}, inplace=True)
    geometries_communes.id_commune = traduire_code_insee_vers_id_commune_crater(geometries_communes.id_commune)

    return geometries_communes


def _corriger_code_insee_Bernwiller(
    geometries_communes: gpd.GeoDataFrame,
) -> gpd.GeoDataFrame:
    log.info("CORRECTION d'une erreur sur l'id de la commune de Bernwiller dans le fichier source")
    geometries_communes.loc[(geometries_communes.nom == "Bernwiller"), "id_commune"] = "C-68006"

    return geometries_communes


def _corriger_code_insee_Les_Trois_Lacs(
    geometries_communes: gpd.GeoDataFrame,
) -> gpd.GeoDataFrame:
    log.info("CORRECTION d'une erreur sur l'id de la commune des Trois Lacs dans le fichier source")
    geometries_communes.loc[(geometries_communes.id_commune == "C-27676"), "id_commune"] = "C-27058"

    return geometries_communes
