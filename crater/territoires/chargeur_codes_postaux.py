from pathlib import Path

import pandas as pd
from pandas import DataFrame

from crater.commun.logger import log
from crater.commun.outils_territoires import traduire_code_insee_vers_id_commune_crater


def charger(chemin_fichier: Path) -> DataFrame:
    log.info("    => Chargement codes postaux depuis %s", chemin_fichier)

    df = (
        pd.read_csv(
            chemin_fichier,
            sep=";",
            dtype={
                "Code_commune_INSEE": "str",
                "Nom_commune": "str",
                "Code_postal": "str",
            },
            na_values=[""],
        )
        .rename(
            columns={
                "Code_commune_INSEE": "id_commune_ou_arrondissement",
                "Nom_commune": "nom_commune_ou_arrondissement",
                "Code_postal": "code_postal",
            },
            errors="raise",
        )
        .reindex(
            columns=[
                "id_commune_ou_arrondissement",
                "nom_commune_ou_arrondissement",
                "code_postal",
            ]
        )
    )

    df = _traduire_code_insee_vers_id_crater(df)

    return df


def _traduire_code_insee_vers_id_crater(df: DataFrame) -> DataFrame:
    df.id_commune_ou_arrondissement = traduire_code_insee_vers_id_commune_crater(df.id_commune_ou_arrondissement)
    return df
