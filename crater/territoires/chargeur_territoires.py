from pathlib import Path
from pandas import DataFrame

from crater.commun.logger import log
from crater.territoires.calculateur_referentiel_territoires import charger_referentiel_territoires
from crater.territoires.calculateur_donnees_territoires import charger_donnees_territoires


def charger_territoires(dossier_referentiel_territoires: Path, dossier_donnees_territoires: Path) -> DataFrame:
    log.info("##### CHARGEMENT DES TERRITOIRES #####")

    referentiel_territoires = charger_referentiel_territoires(dossier_referentiel_territoires)
    donnees_territoires = (
        charger_donnees_territoires(dossier_donnees_territoires).set_index("id_territoire").drop(columns=["nom_territoire", "categorie_territoire"])
    )

    territoires = referentiel_territoires.join(donnees_territoires)

    return territoires
