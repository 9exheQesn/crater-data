from pathlib import Path

import pandas as pd
from pandas import DataFrame

from crater.commun.logger import log
from crater.commun.outils_territoires import traduire_code_insee_vers_id_commune_crater
from crater.commun.outils_verification import verifier_absence_doublons


def charger(chemin_fichier: Path) -> DataFrame:
    log.info("    => Chargement population totale depuis %s", chemin_fichier)

    df = (
        pd.read_csv(
            chemin_fichier,
            sep=";",
            dtype={"DEPCOM": "str", "COM": "str", "PMUN": "Int64"},
            na_values=[""],
        )
        .rename(
            columns={
                "DEPCOM": "id_commune_ou_arrondissement",
                "COM": "nom_commune_ou_arrondissement",
                "PMUN": "population_totale_2017",
            },
            errors="raise",
        )
        .reindex(
            columns=[
                "id_commune_ou_arrondissement",
                "nom_commune_ou_arrondissement",
                "population_totale_2017",
            ]
        )
    )

    df = _traduire_code_insee_vers_id_crater(df)
    verifier_absence_doublons(df, "id_commune_ou_arrondissement")

    return df


def _traduire_code_insee_vers_id_crater(df: DataFrame) -> DataFrame:
    df.id_commune_ou_arrondissement = traduire_code_insee_vers_id_commune_crater(df.id_commune_ou_arrondissement)
    return df
