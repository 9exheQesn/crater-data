import pandas as pd
from pandas import DataFrame
from pathlib import Path
from typing import TypedDict

from crater.commun.config import CATEGORIES_REGROUPEMENTS_COMMUNES
from crater.commun.logger import log
from crater.commun.outils_territoires import traduire_code_insee_vers_id_commune_crater
from crater.commun.outils_verification import (
    verifier_absence_na,
    verifier_nommage_ids_territoires,
    verifier_colonne2_unique_par_colonne1,
    verifier_absence_doublons,
)

# Typed dict pour les éléments de FICHIERS_REGROUPEMENTS_COMMUNES
class FichierRegroupementsCommunes(TypedDict):
    code: str
    fichiers_sources: list[Path]


def charger_composition_regroupements_communes(fichiers_regroupements_communes: list[FichierRegroupementsCommunes]) -> DataFrame:
    df = pd.DataFrame(
        columns=[
            "id_commune",
            "nom_commune",
            "id_regroupement_communes",
            "nom_regroupement_communes",
            "categorie_regroupement_communes",
        ]
    )
    df2 = pd.DataFrame(columns=["id_regroupement_communes"])

    for rc in fichiers_regroupements_communes:
        categorie_regroupement_communes = CATEGORIES_REGROUPEMENTS_COMMUNES[rc["code"]]["categorie"]
        prefixe_id_regroupement_communes = CATEGORIES_REGROUPEMENTS_COMMUNES[rc["code"]]["prefixe_id"]
        for f in rc["fichiers_sources"]:
            log.info(
                "    => Chargement de la composition des regroupements de communes %s depuis %s",
                categorie_regroupement_communes,
                f,
            )
            df_a_ajouter = pd.read_excel(f, sheet_name=0, usecols="A:D", skiprows=4, dtype="str", na_values=[""])
            df_a_ajouter.columns = [  # type: ignore[assignment]  # TODO: open issue https://github.com/pandas-dev/pandas-stubs/issues/73
                "id_commune",
                "nom_commune",
                "id_regroupement_communes",
                "nom_regroupement_communes",
            ]
            df_a_ajouter = _dupliquer_communes_appartenant_a_plusieurs_regroupements(f, df_a_ajouter)
            df_a_ajouter = df_a_ajouter.loc[~df_a_ajouter["id_regroupement_communes"].isnull()]
            df_a_ajouter["id_regroupement_communes"] = prefixe_id_regroupement_communes + "-" + df_a_ajouter["id_regroupement_communes"]
            df_a_ajouter["categorie_regroupement_communes"] = categorie_regroupement_communes

            if categorie_regroupement_communes == CATEGORIES_REGROUPEMENTS_COMMUNES["BV2012"]["categorie"]:
                df_a_ajouter = _ajouter_le_departement_de_la_commune_principale_aux_noms_des_regroupements_communes_non_uniques(df_a_ajouter)
                df_a_ajouter["nom_regroupement_communes"] = "Bassin de vie de " + df_a_ajouter["nom_regroupement_communes"]

            if categorie_regroupement_communes == CATEGORIES_REGROUPEMENTS_COMMUNES["PAYS_PETR"]["categorie"]:
                mask_pays_petr_a_renommer = df_a_ajouter["nom_regroupement_communes"].str.contains("COMMUNAUTE", regex=False)
                df_a_ajouter.loc[mask_pays_petr_a_renommer, "nom_regroupement_communes"] = (
                    "PAYS/PETR " + df_a_ajouter.loc[mask_pays_petr_a_renommer, "nom_regroupement_communes"]
                )

            verifier_absence_na(df_a_ajouter, "id_commune", "id_regroupement_communes")
            verifier_absence_na(df_a_ajouter, "id_commune", "nom_regroupement_communes")
            verifier_nommage_ids_territoires(df_a_ajouter, "id_regroupement_communes")
            verifier_colonne2_unique_par_colonne1(df_a_ajouter, "id_regroupement_communes", "nom_regroupement_communes")
            verifier_colonne2_unique_par_colonne1(
                df_a_ajouter,
                "nom_regroupement_communes",
                "id_regroupement_communes",
            )

            df = pd.concat([df, df_a_ajouter])
            df2 = pd.concat(
                [
                    df2,
                    df_a_ajouter.loc[:, ["id_regroupement_communes"]].drop_duplicates(),
                ]
            )

    verifier_absence_doublons(df2, "id_regroupement_communes")
    df = _traduire_code_insee_vers_id_crater(df)

    return df


def _ajouter_le_departement_de_la_commune_principale_aux_noms_des_regroupements_communes_non_uniques(df):
    df = df.copy()
    df["departement_commune"] = df["id_commune"].str[0:2]

    regroupements_communes = df.loc[df.nom_commune == df.nom_regroupement_communes]
    regroupements_communes = regroupements_communes.rename(columns={"departement_commune": "departement_commune_principale"})
    regroupements_communes = regroupements_communes.loc[:, ["id_regroupement_communes", "departement_commune_principale"]]

    df = pd.merge(df.drop(columns=["departement_commune"]), regroupements_communes, on="id_regroupement_communes", how="left")
    df["nom_regroupement_communes_avec_departement_commune_principale"] = (
        df["nom_regroupement_communes"] + " (" + df["departement_commune_principale"] + ")"
    )
    regroupements_communes_avec_nom_non_unique = (
        df.loc[:, ["id_regroupement_communes", "nom_regroupement_communes"]]
        .drop_duplicates()
        .groupby("nom_regroupement_communes")
        .count()
        .rename(columns={"id_regroupement_communes": "nb"})
        .query("nb > 1")
        .index.to_list()
    )
    mask_regroupements_communes_avec_nom_non_unique = df.nom_regroupement_communes.isin(regroupements_communes_avec_nom_non_unique)
    df.loc[mask_regroupements_communes_avec_nom_non_unique, "nom_regroupement_communes"] = df.loc[
        mask_regroupements_communes_avec_nom_non_unique, "nom_regroupement_communes_avec_departement_commune_principale"
    ]
    df.drop(columns=["nom_regroupement_communes_avec_departement_commune_principale", "departement_commune_principale"], inplace=True)

    return df


def _dupliquer_communes_appartenant_a_plusieurs_regroupements(fichier_source, df):
    if str(fichier_source).find("parcs_naturels_regionaux") >= 0:
        if fichier_source.name == "com2021.xlsx":
            df = _dupliquer_commune_Porte_de_Savoie_appartenant_a_2_PNR(df)
        else:
            raise ValueError(
                "ERREUR lors du chargement des PNRs. Le fichier source n'est pas connu. "
                "Il faut l'ajouter en renseignant si besoin les traitements à effectuer."
            )
    return df


def _dupliquer_commune_Porte_de_Savoie_appartenant_a_2_PNR(df) -> DataFrame:
    # dans le fichier des PNR de l'observatoire des territoires,
    # la commune Porte-de-Savoie (73151) appartient à 2 PNR et n'apparait que sur une ligne (PNR = 'PNR du Massif des Bauges et PNR de Chartreuse')
    mask_commune = df.id_commune == "73151"
    df.loc[mask_commune, "id_regroupement_communes"] = df.loc[mask_commune].id_regroupement_communes.str.split(" et ")
    df.loc[mask_commune, "nom_regroupement_communes"] = df.loc[mask_commune].nom_regroupement_communes.str.split(" et ")
    df = df.explode(["id_regroupement_communes", "nom_regroupement_communes"])

    return df


def _traduire_code_insee_vers_id_crater(df: DataFrame) -> DataFrame:
    df.id_commune = traduire_code_insee_vers_id_commune_crater(df.id_commune)
    return df
