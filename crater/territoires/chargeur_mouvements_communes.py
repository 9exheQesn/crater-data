from pathlib import Path

import pandas as pd
from pandas import DataFrame

from crater.commun.logger import log
from crater.commun.outils_territoires import traduire_code_insee_vers_id_commune_crater

TYPES = {
    "annee_modif": "Int64",
    "COM_FIN": "str",
    "COM_INI": "str",
    "LIB_COM_FIN": "str",
    "LIB_COM_INI": "str",
}

NOUVEAUX_NOMS_COLONNES = {
    "annee_modif": "annee_modification",
    "COM_FIN": "id_commune_final",
    "COM_INI": "id_commune_initial",
    "LIB_COM_FIN": "nom_commune_final",
    "LIB_COM_INI": "nom_commune_initial",
}

NOMS_COLONNES = [
    "annee_modification",
    "id_commune_final",
    "id_commune_initial",
    "nom_commune_final",
    "nom_commune_initial",
]


def charger_mouvements(chemin_fichier: Path) -> DataFrame:
    return pd.concat(
        [
            charger_fusions(chemin_fichier).assign(type="fusion"),
            charger_scissions(chemin_fichier).assign(type="scission"),
        ]
    ).sort_values(by=["annee_modification"])


def charger_fusions(chemin_fichier: Path) -> DataFrame:
    log.info("    => Chargement fusions communes depuis %s", chemin_fichier)

    df = (
        pd.read_excel(
            chemin_fichier,
            sheet_name="Liste des fusions",
            usecols="A:E",
            skiprows=5,
            dtype=TYPES,  # type: ignore[arg-type] # TODO: solved > 1.5.3, see https://github.com/pandas-dev/pandas-stubs/issues/440
            na_values=[""],
        )
        .rename(columns=NOUVEAUX_NOMS_COLONNES, errors="raise")
        .reindex(columns=NOMS_COLONNES)
    )

    df = _traduire_code_insee_vers_id_crater(df)

    return df


def charger_scissions(chemin_fichier: Path) -> DataFrame:
    log.info("    => Chargement scissions communes depuis %s", chemin_fichier)

    df = (
        pd.read_excel(
            chemin_fichier,
            sheet_name="Liste des scissions",
            usecols="A:E",
            skiprows=5,
            dtype=TYPES,  # type: ignore[arg-type] # TODO: solved > 1.5.3, see https://github.com/pandas-dev/pandas-stubs/issues/440
            na_values=[""],
        )
        .rename(columns=NOUVEAUX_NOMS_COLONNES, errors="raise")
        .reindex(columns=NOMS_COLONNES)
    )

    df = _traduire_code_insee_vers_id_crater(df)

    return df


def _traduire_code_insee_vers_id_crater(df: DataFrame) -> DataFrame:
    df.id_commune_final = traduire_code_insee_vers_id_commune_crater(df.id_commune_final)
    df.id_commune_initial = traduire_code_insee_vers_id_commune_crater(df.id_commune_initial)
    return df
