import pandas as pd

from crater.commun.logger import log


def charger_groupes_cultures(chemin_fichier) -> pd.DataFrame:
    log.info("=> Chargement groupes de culture")

    return (
        pd.read_csv(
            chemin_fichier,
            sep=";",
            encoding="utf-8",
        )
        .rename(
            columns={
                "Code Culture": "code_culture",
                "Libellé Culture": "libelle_culture",
                "Code Groupe Culture": "code_groupe_culture",
                "Libellé Groupe Culture": "libelle_groupe_culture",
            },
            errors="raise",
        )
        .reindex(
            columns=[
                "code_culture",
                "libelle_culture",
                "code_groupe_culture",
                "libelle_groupe_culture",
            ]
        )
    )
