import shutil
import uuid
from pathlib import Path

import geopandas as gpd
import py7zr

from crater.commun.config import CRS_PROJET
from crater.commun.logger import log


def charger_rpg(emplacement_fichier: Path, crs=CRS_PROJET) -> gpd.GeoDataFrame:
    log.info("=> Chargement du RPG " + str(emplacement_fichier))
    dossier_tmp_unique = f"tmp/{uuid.uuid4().hex}"

    with py7zr.SevenZipFile(emplacement_fichier, mode="r") as z:
        z.extractall(dossier_tmp_unique)

    fichiers = list(Path(dossier_tmp_unique).glob("**/PARCELLES_GRAPHIQUES.shp"))
    if len(fichiers) == 0:
        raise ValueError("ERREUR: PARCELLES_GRAPHIQUES.shp non présent dans le dossier!")
    if len(fichiers) > 1:
        raise ValueError("ERREUR: plusieurs fichiers PARCELLES_GRAPHIQUES.shp dans le dossier !")
    else:
        rpg = gpd.read_file(fichiers[0]).to_crs(crs)
        shutil.rmtree(dossier_tmp_unique)

    return rpg
