from pathlib import Path

import geopandas
import pandas
import topojson
from crater.commun.config import NUMEROS_DEPARTEMENTS_DROM
from crater.commun.export_fichier import reinitialiser_dossier

from crater.commun.logger import log
from crater.commun.outils_territoires import (
    traduire_code_insee_vers_id_departement_crater,
    traduire_code_insee_vers_id_epci_crater,
    traduire_code_insee_vers_id_region_crater,
)
from crater.territoires.chargeur_geometries_communes import charger_geometries_communes


def calculer_cartes(
    territoires: pandas.DataFrame,
    chemin_fichier_contours_regions: Path,
    chemin_fichier_contours_departements: Path,
    chemin_fichier_contours_epcis: Path,
    chemin_dossier_contours_communes: Path,
    dossier_resultat: Path,
):
    log.info("##### CALCUL DES CARTES #####")

    reinitialiser_dossier(dossier_resultat)

    _calculer_carte_regions(
        territoires,
        chemin_fichier_contours_regions,
        dossier_resultat / "regions-crater.geojson",
    )
    _calculer_carte_departements(
        territoires,
        chemin_fichier_contours_departements,
        dossier_resultat / "departements-crater.geojson",
    )
    _calculer_carte_epcis(
        territoires,
        chemin_fichier_contours_epcis,
        dossier_resultat / "epcis-crater.geojson",
    )
    _calculer_carte_communes(
        territoires,
        chemin_dossier_contours_communes,
        dossier_resultat / "communes-crater",
    )


def _calculer_carte_regions(
    territoires: pandas.DataFrame,
    chemin_fichier_contours_regions: Path,
    chemin_fichier_resultat_regions: Path,
):
    log.info("  => régions")
    regions_gdf = geopandas.read_file(chemin_fichier_contours_regions)
    regions_gdf["id_territoire"] = traduire_code_insee_vers_id_region_crater(regions_gdf.code)
    regions_gdf = regions_gdf.loc[:, ["id_territoire", "geometry"]]
    regions_gdf = _selectionner_et_simplifier_geometries_territoires(territoires, "REGION", regions_gdf)
    regions_gdf.loc[:, ["id_territoire", "id_nom_territoire", "nom_territoire", "geometry"]].to_file(
        chemin_fichier_resultat_regions, driver="GeoJSON"
    )


def _calculer_carte_departements(
    territoires: pandas.DataFrame,
    chemin_fichier_contours_departements: Path,
    chemin_fichier_resultat_departements: Path,
):
    log.info("  => départements")
    departements_gdf = geopandas.read_file(chemin_fichier_contours_departements)
    departements_gdf["id_territoire"] = traduire_code_insee_vers_id_departement_crater(departements_gdf.code)
    departements_gdf = departements_gdf.loc[:, ["id_territoire", "geometry"]]
    departements_gdf = _selectionner_et_simplifier_geometries_territoires(territoires, "DEPARTEMENT", departements_gdf)
    departements_gdf.loc[:, ["id_territoire", "id_nom_territoire", "nom_territoire", "geometry"]].to_file(
        chemin_fichier_resultat_departements, driver="GeoJSON"
    )


def _calculer_carte_epcis(
    territoires: pandas.DataFrame,
    chemin_fichier_contours_epcis: Path,
    chemin_fichier_resultat_epcis: Path,
):
    log.info("  => epcis")
    epcis_gdf = geopandas.read_file(chemin_fichier_contours_epcis)
    epcis_gdf = epcis_gdf.astype({"SIREN": "int32"})
    epcis_gdf["id_territoire"] = traduire_code_insee_vers_id_epci_crater(epcis_gdf.SIREN.map(str))
    epcis_gdf = epcis_gdf.loc[:, ["id_territoire", "geometry"]]
    epcis_gdf = _selectionner_et_simplifier_geometries_territoires(territoires, "EPCI", epcis_gdf)
    epcis_gdf.loc[:, ["id_territoire", "id_nom_territoire", "nom_territoire", "geometry"]].to_file(chemin_fichier_resultat_epcis, driver="GeoJSON")


def _calculer_carte_communes(
    territoires: pandas.DataFrame,
    chemin_dossier_contours_communes: Path,
    chemin_dossier_resultat_communes: Path,
):
    log.info("  => communes")

    reinitialiser_dossier(chemin_dossier_resultat_communes)

    communes_gdf = charger_geometries_communes(chemin_dossier_contours_communes)
    communes_gdf.rename(columns={"id_commune": "id_territoire"}, inplace=True)
    communes_gdf["id_departement"] = traduire_code_insee_vers_id_departement_crater(communes_gdf.id_territoire.str[2:4])
    communes_gdf = communes_gdf.loc[:, ["id_territoire", "id_departement", "geometry"]]
    communes_gdf = communes_gdf.loc[~communes_gdf["id_departement"].str[2:4].isin(NUMEROS_DEPARTEMENTS_DROM)]
    communes_gdf = _selectionner_et_simplifier_geometries_territoires(territoires, "COMMUNE", communes_gdf)

    for d in sorted(communes_gdf["id_departement"].unique()):
        id_nom_d = territoires.loc[d].id_nom_territoire
        communes_gdf[communes_gdf.id_departement == d].loc[:, ["id_territoire", "id_nom_territoire", "nom_territoire", "geometry"]].to_file(
            chemin_dossier_resultat_communes / f"communes_du_departement_{id_nom_d}.geojson",
            driver="GeoJSON",
        )


def _selectionner_et_simplifier_geometries_territoires(
    territoires: pandas.DataFrame,
    categorie_territoire: str,
    gdf: geopandas.GeoDataFrame,
) -> geopandas.GeoDataFrame:
    gdf_resultat = gdf.merge(
        territoires.loc[
            territoires.categorie_territoire == categorie_territoire,
            ["id_nom_territoire", "nom_territoire", "categorie_territoire"],
        ],
        on="id_territoire",
        how="inner",
    )

    gdf_resultat = topojson.Topology(gdf_resultat.to_crs(epsg=4326)).toposimplify(0.02).to_gdf().set_crs(epsg=4326, allow_override=True)

    if gdf_resultat.shape[0] != territoires.loc[territoires.categorie_territoire == categorie_territoire].shape[0]:
        raise ValueError(
            f"""Attention, problème de cohérence entre les géométries et le référentiel de territoires : 
            taille géométries : {gdf.shape[0]}
            taille territoires : {territoires.loc[territoires.categorie_territoire == categorie_territoire].shape[0]}
            taille obtenue : {gdf_resultat.shape[0]}"""
        )

    return gdf_resultat
