from datetime import datetime
from io import StringIO
from pathlib import Path

import pandas

from crater.commun.export_fichier import exporter_csv, reinitialiser_dossier
from crater.commun.logger import log
from crater.commun.requetes_api import get_retry

URL_API_PRELEVEMETS = "https://hubeau.eaufrance.fr/api/v1/prelevements/chroniques.csv?code_usage=IRR"


def collecter_donnees_prelevements_eau(
    annee: int,
    dossier_output: Path,
    date_extraction: datetime,
    nb_items_par_page: int = 5000,
    nb_max_pages=-1,
) -> None:
    BASE_URL = f"{URL_API_PRELEVEMETS}&annee={annee}&size={nb_items_par_page}"

    log.info("#### Collecte des données de prélèvement d'eau pour l'irrigation depuis l'api HUBEAU ####")
    log.info(f"    - Base de l'url pour le get : {BASE_URL}")

    reinitialiser_dossier(dossier_output / str(annee))

    indice_page = 1
    r = get_retry(f"{BASE_URL}&page={indice_page}")
    df = pandas.read_csv(StringIO(r.text), sep=";", dtype={"code_commune_insee": "str"})

    while ("next" in r.links.keys()) & ((nb_max_pages == -1) | (indice_page < nb_max_pages)):
        indice_page += 1
        r = get_retry(f"{BASE_URL}&page={indice_page}")
        df = pandas.concat(
            [
                df,
                pandas.read_csv(StringIO(r.text), sep=";", dtype={"code_commune_insee": "str"}),
            ]
        )

    entete = f"""\
Données extraites depuis le site hubeau
API : Prelevements
URL : {BASE_URL}
Date extraction : {date_extraction.strftime('%d/%m/%Y')}
Nb pages collectées : {indice_page}
"""

    fichier_resultats = dossier_output / str(annee) / f"prelevements_eau_par_communes_{date_extraction.strftime('%Y%m%d')}.csv"
    log.info(f"    - Export des résultats dans {fichier_resultats}")
    exporter_csv(df, fichier_resultats, entete=entete)
