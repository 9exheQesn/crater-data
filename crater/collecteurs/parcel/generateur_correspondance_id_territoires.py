from pathlib import Path

import pandas as pd

from crater.collecteurs.parcel.collecteur_territoires_api_parcel import collecter_territoire_par_nom
from crater.commun.export_fichier import reinitialiser_dossier
from crater.commun.logger import log
from crater.commun.outils_dataframes import merge_strict

ORDRE_COLONNES = [
    "id_territoire",
    "nom_territoire",
    "categorie_territoire",
    "id_territoire_parcel",
    "nom_territoire_parcel",
    "pourcentage_confiance_resultat",
    "code_retour",
    "autres_resultats",
]


def generer_fichier_correspondance_territoires_parcel_crater(
    fichier_communes_parcel: Path,
    fichier_donnees_territoires_crater: Path,
    dossier_output: Path,
):
    log.info("#### Génération du fichier de correspondance entre les IDs de territoire PARCEL et les IDs CRATer")

    reinitialiser_dossier(dossier_output)

    communes = _creer_df_communes_parcel_crater(fichier_communes_parcel, fichier_donnees_territoires_crater)
    communes_recuperees_via_fichier = communes.loc[~communes["id_territoire_parcel"].isna(), :]

    log.info("    => Export du fichier communes_recuperees_via_fichier_parcel.csv")
    communes_recuperees_via_fichier.to_csv(
        dossier_output / "communes_recuperees_via_fichier_parcel.csv",
        sep=";",
        index=False,
        float_format="%.0f",
    )

    log.info("    => Récupération des ID communes manquantes via l'api PARCEL")
    communes_recuperees_via_api = communes.loc[communes["id_territoire_parcel"].isna(), :]
    communes_recuperees_via_api = collecter_territoire_par_nom(communes_recuperees_via_api)
    log.info("    => Export du fichier communes_recuperees_via_api_parcel.csv")
    communes_recuperees_via_api.to_csv(
        dossier_output / "communes_recuperees_via_api_parcel_A_VALIDER.csv",
        sep=";",
        index=False,
        float_format="%.0f",
    )

    log.info("    => Récupération des ID pays, départements, et région via l'api PARCEL")
    territoires_hors_communes_recuperes_via_api_parcel = (
        pd.read_csv(fichier_donnees_territoires_crater, sep=";")
        .loc[:, ["id_territoire", "nom_territoire", "categorie_territoire"]]
        .query("categorie_territoire in ['PAYS', 'REGION', 'DEPARTEMENT']")
    )
    territoires_hors_communes_recuperes_via_api_parcel = collecter_territoire_par_nom(territoires_hors_communes_recuperes_via_api_parcel)
    log.info("    => Export du fichier territoires_hors_communes_recuperes_via_api_parcel_A_VALIDER.csv")
    territoires_hors_communes_recuperes_via_api_parcel.to_csv(
        dossier_output / "territoires_hors_communes_recuperes_via_api_parcel_A_VALIDER.csv",
        sep=";",
        index=False,
        float_format="%.0f",
    )

    log.info("    => Export du fichier correspondance_territoires_parcel_crater_A_VALIDER.csv")
    territoires_hors_communes_recuperes_via_api_parcel = pd.concat(
        [
            territoires_hors_communes_recuperes_via_api_parcel,
            pd.concat([communes_recuperees_via_api, communes_recuperees_via_fichier]),
        ]
    )

    territoires_hors_communes_recuperes_via_api_parcel.to_csv(
        dossier_output / "correspondance_territoires_parcel_crater_A_VALIDER.csv",
        sep=";",
        index=False,
        float_format="%.0f",
    )

    return None


def _creer_df_communes_parcel_crater(fichier_communes_parcel, fichier_donnees_territoires_crater):
    communes_parcel = (
        pd.read_csv(fichier_communes_parcel, sep=",", dtype=object)
        .loc[:, ["Code_insee", "Locale_Key", "Locale_Name"]]
        .drop_duplicates()
        .rename(columns={"Locale_Name": "nom_territoire_parcel"})
        .rename(columns={"Locale_Key": "id_territoire_parcel"})
        .assign(Code_insee=lambda x: x["Code_insee"].str.zfill(5))
        .assign(id_territoire=lambda x: ("C-" + x["Code_insee"]))
        .assign(pourcentage_confiance_resultat=100)
        .assign(code_retour="OK_FICHIER")
        .assign(autres_resultats="")
        .drop(columns=["Code_insee"])
    )
    communes_crater = (
        pd.read_csv(fichier_donnees_territoires_crater, sep=";")
        .loc[:, ["id_territoire", "nom_territoire", "categorie_territoire"]]
        .query("categorie_territoire == 'COMMUNE'")
        .reset_index(drop=True)
    )
    communes_crater = merge_strict(communes_crater, communes_parcel, on="id_territoire", how="left", correspondance="lache")

    return communes_crater.loc[:, ORDRE_COLONNES]
