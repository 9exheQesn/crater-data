from pathlib import Path

import numpy as np
from pandas import DataFrame

from crater.commun.config import IDS_CATEGORIES_TERRITOIRES_SUPRACOMMUNAUX, ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_FLUX_AZOTE
from crater.commun.export_fichier import (
    reinitialiser_dossier,
    exporter_df_indicateurs_par_territoires,
)
from crater.commun.logger import log
from crater.commun.outils_territoires import ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales
from crater.nutriments.chargeur_flux_azote import charger as chargeur_flux_azote


def calculer_indicateurs_azote(
    territoires: DataFrame,
    chemin_fichier_flux_azote: Path,
    chemin_dossier_output: Path,
) -> None:
    log.info("##### CALCUL FLUX AZOTE #####")

    reinitialiser_dossier(chemin_dossier_output)

    df_flux_azote = chargeur_flux_azote(chemin_fichier_flux_azote).rename(columns={"id_commune": "id_territoire"}).set_index("id_territoire")

    colonnes = (
        ["nom_territoire", "categorie_territoire"]
        + IDS_CATEGORIES_TERRITOIRES_SUPRACOMMUNAUX
        + [
            "annee_dernier_mouvement_commune",
        ]
    )
    df_flux_azote = territoires[colonnes].join(df_flux_azote)

    for colonne in df_flux_azote.loc[:, slice("bovins_ugb", "superficie_prairies_permanentes_ha")].columns:
        df_flux_azote.loc[
            df_flux_azote.annee_dernier_mouvement_commune > ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_FLUX_AZOTE,
            colonne,
        ] = np.nan
        df_flux_azote = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(df_flux_azote, colonne)

    exporter_df_indicateurs_par_territoires(
        df_flux_azote.drop(columns=["annee_dernier_mouvement_commune"] + IDS_CATEGORIES_TERRITOIRES_SUPRACOMMUNAUX),
        chemin_dossier_output,
        "flux_azote",
    )
