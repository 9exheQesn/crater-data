from pathlib import Path

import pandas
from pandas import DataFrame

from crater.commun.logger import log
from crater.commun.outils_dataframes import merge_strict
from crater.commun.outils_territoires import (
    calculer_donnees_infraterritoriales_par_repartition_donnees_supraterritoriales,
    ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales,
)
from crater.energie.config_energie import (
    COEFFICIENT_ENERGIE_FINALE_VERS_PRIMAIRE_ELECTRICITE_MIX_FRANCE,
    FACTEUR_CONVERSION_GJ_PAR_kWh,
    COEFFICIENT_ENERGIE_FINALE_VERS_PRIMAIRE_GAZ,
    COEFFICIENT_ENERGIE_FINALE_VERS_PRIMAIRE_BIOMASSE,
)

M2_PAR_HA = 10000


def calculer_indicateurs_energie_serres(territoires: DataFrame, chemin_fichier_indicateurs_chauffage_serres: Path) -> DataFrame:
    log.info("##### CALCUL ENERGIE CHAUFFAGE SERRES #####")

    df_surface_serres_par_departements = pandas.read_excel(
        chemin_fichier_indicateurs_chauffage_serres,
        sheet_name="surface_serres_par_departement",
        usecols="A:C",
    )

    df_energie_serres = _calculer_surface_serres(territoires, df_surface_serres_par_departements)
    df_energie_serres = _positionner_zone_geographique(df_energie_serres)

    df_coefficients_par_zone_geographique = _charger_coefficients_par_zone_geographique(chemin_fichier_indicateurs_chauffage_serres)

    df_energie_serres = _ajouter_colonnes_indicateurs_energie(df_energie_serres, df_coefficients_par_zone_geographique)

    df_energie_serres = _calculer_indicateurs_niveaux_region_pays(df_energie_serres)

    return df_energie_serres.loc[
        :,
        [
            "surface_serres_ha",
            "serres_electricite_EP_GJ",
            "serres_gaz_EP_GJ",
            "serres_biomasse_EP_GJ",
            "serres_EP_GJ",
        ],
    ]


def _charger_coefficients_par_zone_geographique(chemin_fichier_indicateurs_chauffage_serres):
    df_coefficients_par_zone_geographique = pandas.read_excel(
        chemin_fichier_indicateurs_chauffage_serres,
        sheet_name="coefficients_et_mix_energetique",
        usecols="A:F",
    )

    somme_coefficients = df_coefficients_par_zone_geographique.chauffage_biomasse_part + df_coefficients_par_zone_geographique.chauffage_gaz_part
    if (somme_coefficients != 1).any():
        raise ValueError(
            f"ERREUR calcul indicateur energie serres : la somme des coefficients pour les différents types de "
            f"chauffage est différente de 1 (fichier {chemin_fichier_indicateurs_chauffage_serres})"
        )

    return df_coefficients_par_zone_geographique


def _verifier_somme_coefficients_egale_1(df_coefficients_par_zone_geographique):
    somme_coefficients = df_coefficients_par_zone_geographique.chauffage_biomasse_part + df_coefficients_par_zone_geographique.chauffage_gaz_part
    if (somme_coefficients != 1).any():
        raise ValueError(
            "ERREUR calcul indicateur energie serres : la somme des coefficients pour les différents types de chauffage est différente de 1"
        )


def _calculer_surface_serres(territoires: DataFrame, df_surface_serres_par_departements: DataFrame):
    df_energie_serres = territoires.copy().join(df_surface_serres_par_departements.set_index("id_departement"))
    df_energie_serres.loc[df_energie_serres["categorie_territoire"] == "DEPARTEMENT", ["surface_serres_ha"]] = df_energie_serres.loc[
        df_energie_serres["categorie_territoire"] == "DEPARTEMENT", ["surface_serres_ha"]
    ].fillna(0)
    df_energie_serres = calculer_donnees_infraterritoriales_par_repartition_donnees_supraterritoriales(
        df_energie_serres,
        "surface_serres_ha",
        "sau_productive_ha",
        "COMMUNE",
        "DEPARTEMENT",
    )
    df_energie_serres = calculer_donnees_infraterritoriales_par_repartition_donnees_supraterritoriales(
        df_energie_serres,
        "surface_serres_ha",
        "sau_productive_ha",
        "EPCI",
        "DEPARTEMENT",
    )
    df_energie_serres = calculer_donnees_infraterritoriales_par_repartition_donnees_supraterritoriales(
        df_energie_serres,
        "surface_serres_ha",
        "sau_productive_ha",
        "REGROUPEMENT_COMMUNES",
        "DEPARTEMENT",
    )

    return df_energie_serres


def _positionner_zone_geographique(df_energie_serres):
    df_zone_geographique_departements = df_energie_serres.loc[df_energie_serres["categorie_territoire"] == "DEPARTEMENT", ["zone_geographique"]]
    df_energie_serres["zone_geographique"].fillna(
        df_energie_serres["id_departement"].map(df_zone_geographique_departements["zone_geographique"]), inplace=True
    )
    df_energie_serres["zone_geographique"].fillna("france", inplace=True)
    return df_energie_serres


def _ajouter_colonnes_indicateurs_energie(df_energie_serres, df_coefficients_par_zone_geographique):
    df_energie_serres = merge_strict(
        df_energie_serres.reset_index(), df_coefficients_par_zone_geographique, on="zone_geographique", how="left"
    ).set_index("id_territoire")

    df_energie_serres["serre_EF_GJ"] = (
        df_energie_serres["surface_serres_ha"] * M2_PAR_HA * df_energie_serres["chauffage_et_electricite_kWh_m2"] * FACTEUR_CONVERSION_GJ_PAR_kWh
    )

    df_energie_serres["serre_electricite_EF_GJ"] = (
        df_energie_serres["surface_serres_ha"] * M2_PAR_HA * df_energie_serres["electricite_hors_chauffage_kWh_m2"] * FACTEUR_CONVERSION_GJ_PAR_kWh
    )

    df_energie_serres["serres_electricite_EP_GJ"] = (
        df_energie_serres["serre_electricite_EF_GJ"] * COEFFICIENT_ENERGIE_FINALE_VERS_PRIMAIRE_ELECTRICITE_MIX_FRANCE
    )

    df_energie_serres["serres_chauffage_EF_GJ"] = df_energie_serres["serre_EF_GJ"] - df_energie_serres["serre_electricite_EF_GJ"]

    df_energie_serres["serres_gaz_EP_GJ"] = (
        df_energie_serres["serres_chauffage_EF_GJ"] * df_energie_serres["chauffage_gaz_part"] * COEFFICIENT_ENERGIE_FINALE_VERS_PRIMAIRE_GAZ
    )

    df_energie_serres["serres_biomasse_EP_GJ"] = (
        df_energie_serres["serres_chauffage_EF_GJ"] * df_energie_serres["chauffage_biomasse_part"] * COEFFICIENT_ENERGIE_FINALE_VERS_PRIMAIRE_BIOMASSE
    )

    df_energie_serres["serres_EP_GJ"] = (
        df_energie_serres["serres_electricite_EP_GJ"] + df_energie_serres["serres_gaz_EP_GJ"] + df_energie_serres["serres_biomasse_EP_GJ"]
    )
    return df_energie_serres


def _calculer_indicateurs_niveaux_region_pays(df_energie_serres):
    for colonne in [
        "surface_serres_ha",
        "serres_electricite_EP_GJ",
        "serres_gaz_EP_GJ",
        "serres_biomasse_EP_GJ",
        "serres_EP_GJ",
    ]:
        df_energie_serres = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(
            df_energie_serres, colonne, "DEPARTEMENT", ["REGION", "PAYS"]
        )
    return df_energie_serres
