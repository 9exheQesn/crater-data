from pathlib import Path

import pandas
from pandas import DataFrame

from crater.commun.logger import log
from crater.energie.config_energie import (
    FACTEUR_CONVERSION_GJ_PAR_kWh,
    COEFFICIENT_ENERGIE_FINALE_VERS_PRIMAIRE_CARBURANTS,
    COEFFICIENT_ENERGIE_FINALE_VERS_PRIMAIRE_ELECTRICITE_MIX_FRANCE,
)

COEFFICIENT_COUT_ENERGETIQUE_IRRIGATION_kWh_PAR_M3 = 0.5  # Feuille A7 Climagri, valeur moyenne retenue tous types d'irrgation
COEFFICIENT_COUT_ENERGETIQUE_IRRIGATION_GJ_PAR_M3 = COEFFICIENT_COUT_ENERGETIQUE_IRRIGATION_kWh_PAR_M3 * FACTEUR_CONVERSION_GJ_PAR_kWh
PART_CARBURANTS_DANS_MIX_ENERGETIQUE_IRRIGATION = 0.15  # Feuille A7 Climagri
PART_ELECTRICITE_DANS_MIX_ENERGETIQUE_IRRIGATION = 0.85  # Feuille A7 Climagri


def calculer_indicateurs_energie_irrigation(
    territoires: DataFrame,
    chemin_fichier_indicateurs_eau: Path,
) -> DataFrame:
    log.info("##### CALCUL ENERGIE IRRIGATION #####")

    df_eau = pandas.read_csv(chemin_fichier_indicateurs_eau, sep=";").set_index("id_territoire").loc[:, ["volume_m3"]]

    df_energie_irrigation = territoires.loc[:, ["nom_territoire", "categorie_territoire"]].join(df_eau)

    df_energie_irrigation["irrigation_EF_GJ"] = df_energie_irrigation["volume_m3"] * COEFFICIENT_COUT_ENERGETIQUE_IRRIGATION_GJ_PAR_M3
    df_energie_irrigation["irrigation_EF_GJ"] = df_energie_irrigation["irrigation_EF_GJ"].fillna(0)

    df_energie_irrigation["irrigation_carburants_EP_GJ"] = (
        df_energie_irrigation["irrigation_EF_GJ"]
        * PART_CARBURANTS_DANS_MIX_ENERGETIQUE_IRRIGATION
        * COEFFICIENT_ENERGIE_FINALE_VERS_PRIMAIRE_CARBURANTS
    )
    df_energie_irrigation["irrigation_electricite_EP_GJ"] = (
        df_energie_irrigation["irrigation_EF_GJ"]
        * PART_ELECTRICITE_DANS_MIX_ENERGETIQUE_IRRIGATION
        * COEFFICIENT_ENERGIE_FINALE_VERS_PRIMAIRE_ELECTRICITE_MIX_FRANCE
    )
    df_energie_irrigation["irrigation_EP_GJ"] = (
        df_energie_irrigation["irrigation_carburants_EP_GJ"] + df_energie_irrigation["irrigation_electricite_EP_GJ"]
    )

    df_energie_irrigation = df_energie_irrigation.loc[
        :, ["nom_territoire", "categorie_territoire", "irrigation_EP_GJ", "irrigation_carburants_EP_GJ", "irrigation_electricite_EP_GJ"]
    ]

    return df_energie_irrigation
