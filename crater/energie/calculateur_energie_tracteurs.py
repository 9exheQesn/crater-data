from pathlib import Path

import pandas
from pandas import DataFrame

from crater.commun.logger import log
from crater.commun.outils_dataframes import merge_strict
from crater.commun.outils_territoires import ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales
from crater.surface_agricole_utile import chargeur_sau_par_commune_et_culture

# Source tableur climagri, feuille A4
DF_COEFFICIENTS_CONSOMMATION_CARBURANT_LITRES_PAR_HA = pandas.DataFrame(
    columns=["categorie_culture_climagri", "coefficient_consommation_carburant_litres_par_ha"],
    data=[
        ["culture_annuelle", 99.62],
        ["prairie_temporaire", 65],
        ["prairie_naturelle", 65],
        ["prairie_naturelle_peu_productive", 5],
        ["culture_permanente", 190],
    ],
)

# Source tableur ClimAgri, feuille A4, feuille C5
COEFFICIENT_ENERGIE_PRIMAIRE_CARBURANT_GJ_PAR_LITRE = 0.046


def calculer_indicateurs_energie_tracteurs(
    territoires: DataFrame,
    chemin_fichiers_sau_par_cultures_rpg: Path,
    chemin_fichier_correspondance_nomenclature_rpg_vers_crater: Path,
) -> DataFrame:
    log.info("##### CALCUL ENERGIE TRACTEURS #####")

    df_sau_communes = chargeur_sau_par_commune_et_culture.charger(chemin_fichiers_sau_par_cultures_rpg)
    df_correspondance_nomenclature_rpg_vers_crater = pandas.read_csv(
        chemin_fichier_correspondance_nomenclature_rpg_vers_crater, sep=";", encoding="utf-8"
    )
    df_energie_tracteurs = merge_strict(
        df_sau_communes,
        df_correspondance_nomenclature_rpg_vers_crater.loc[:, ["code_culture_rpg", "categorie_culture_climagri"]],
        on="code_culture_rpg",
        how="left",
    )

    df_energie_tracteurs = merge_strict(
        df_energie_tracteurs, DF_COEFFICIENTS_CONSOMMATION_CARBURANT_LITRES_PAR_HA, on="categorie_culture_climagri", how="left"
    )

    df_energie_tracteurs["tracteurs_carburants_EP_GJ"] = (
        df_energie_tracteurs["sau_ha"]
        * df_energie_tracteurs["coefficient_consommation_carburant_litres_par_ha"]
        * COEFFICIENT_ENERGIE_PRIMAIRE_CARBURANT_GJ_PAR_LITRE
    )

    df_energie_tracteurs = df_energie_tracteurs.groupby("id_commune", as_index=False)["tracteurs_carburants_EP_GJ"].sum()
    df_energie_tracteurs = df_energie_tracteurs.loc[:, ["id_commune", "tracteurs_carburants_EP_GJ"]]

    df_energie_tracteurs = (
        territoires.reset_index().merge(df_energie_tracteurs, how="left", left_on="id_territoire", right_on="id_commune").set_index("id_territoire")
    )
    df_energie_tracteurs["tracteurs_carburants_EP_GJ"] = df_energie_tracteurs["tracteurs_carburants_EP_GJ"].fillna(0)
    df_energie_tracteurs = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(df_energie_tracteurs, "tracteurs_carburants_EP_GJ")

    df_energie_tracteurs["tracteurs_EP_GJ"] = df_energie_tracteurs["tracteurs_carburants_EP_GJ"]

    df_energie_tracteurs = df_energie_tracteurs.loc[:, ["nom_territoire", "categorie_territoire", "tracteurs_EP_GJ", "tracteurs_carburants_EP_GJ"]]

    return df_energie_tracteurs
