from pathlib import Path

from pandas import DataFrame

from crater.commun.export_fichier import exporter_df_indicateurs_par_territoires, reinitialiser_dossier
from crater.commun.logger import log
from crater.energie.calculateur_energie_engrais_azotes import calculer_indicateurs_energie_engrais_azotes
from crater.energie.calculateur_energie_irrigation import calculer_indicateurs_energie_irrigation
from crater.energie.calculateur_energie_serres import calculer_indicateurs_energie_serres
from crater.energie.calculateur_energie_tracteurs import calculer_indicateurs_energie_tracteurs


def calculer_indicateurs_energie(
    territoires: DataFrame,
    chemin_fichier_indicateurs_eau: Path,
    chemin_fichiers_sau_par_cultures_rpg: Path,
    chemin_fichier_correspondance_nomenclature_rpg_vers_crater: Path,
    chemin_fichier_source_serres: Path,
    chemin_fichier_flux_azote: Path,
    chemin_dossier_output: Path,
) -> None:
    log.info("##### CALCUL ENERGIE #####")

    reinitialiser_dossier(chemin_dossier_output)

    df_energie = territoires.loc[:, ["nom_territoire", "categorie_territoire"]].copy()

    df_energie_tracteurs = calculer_indicateurs_energie_tracteurs(
        territoires, chemin_fichiers_sau_par_cultures_rpg, chemin_fichier_correspondance_nomenclature_rpg_vers_crater
    )
    df_energie = df_energie.join(df_energie_tracteurs.loc[:, ["tracteurs_EP_GJ", "tracteurs_carburants_EP_GJ"]])

    df_energie_irrigation = calculer_indicateurs_energie_irrigation(territoires, chemin_fichier_indicateurs_eau)
    df_energie = df_energie.join(df_energie_irrigation.loc[:, ["irrigation_EP_GJ", "irrigation_carburants_EP_GJ", "irrigation_electricite_EP_GJ"]])

    df_energie_serres = calculer_indicateurs_energie_serres(
        territoires,
        chemin_fichier_source_serres,
    )
    df_energie = df_energie.join(df_energie_serres.loc[:, ["serres_EP_GJ", "serres_electricite_EP_GJ", "serres_gaz_EP_GJ", "serres_biomasse_EP_GJ"]])

    df_energie_engrais_azotes = calculer_indicateurs_energie_engrais_azotes(
        territoires,
        chemin_fichier_flux_azote,
    )
    df_energie = df_energie.join(df_energie_engrais_azotes.loc[:, ["engrais_azotes_EP_GJ"]])

    exporter_df_indicateurs_par_territoires(df_energie, chemin_dossier_output, "energie")
