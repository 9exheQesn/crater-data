from pathlib import Path

import numpy as np
import pandas as pd
from pandas import DataFrame

from crater.commun.calcul_indicateurs import calculer_moyenne_glissante
from crater.commun.export_fichier import reinitialiser_dossier
from crater.commun.logger import log, chronometre
from crater.commun.outils_dataframes import produit_cartesien
from crater.commun.outils_territoires import (
    traduire_code_insee_vers_id_departement_crater,
    ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales,
    trier_territoires,
)
from crater.intrants_agricoles import chargeur_donnees_pesticides


@chronometre
def calculer_indicateurs_pesticides(
    df_intrants: DataFrame,
    classifications_substances_retenues: str,
    chemins_dossiers_bnvd: list[Path],
    chemin_fichier_du_2019: Path,
    chemin_fichier_du_2017: Path,
    chemin_dossier_output: Path,
) -> tuple[DataFrame, DataFrame]:
    log.info("    => Indicateurs pesticides")

    reinitialiser_dossier(chemin_dossier_output)

    du = _calculer_et_exporter_donnees_du(chemin_fichier_du_2019, chemin_fichier_du_2017, chemin_dossier_output)

    bnvd = _calculer_donnees_bnvd(chemins_dossiers_bnvd)

    nodu_substances = _calculer_nodu_substances(bnvd, du)

    nodu_substances = _ajouter_colonne_substance_retenue(nodu_substances, classifications_substances_retenues)

    nodu_par_territoires = _calculer_nodu_par_territoires(df_intrants, nodu_substances)

    df_intrants = _ajouter_colonnes_nodu_par_territoires(df_intrants, nodu_par_territoires)
    df_intrants = _calculer_valeurs_supracommunales(df_intrants)
    df_intrants = _calculer_nodu_normalise(df_intrants)
    df_intrants = trier_territoires(df_intrants)

    return df_intrants, nodu_substances


@chronometre
def calculer_indicateurs_intrants_moyennes_triennales(donnees_intrants_agricoles):
    df_intrants_moyennes_triennales = _calculer_moyennes_triennales_indicateurs_intrants(donnees_intrants_agricoles.drop(columns="NODU_normalise"))
    return _calculer_nodu_normalise(df_intrants_moyennes_triennales)


@chronometre
def _calculer_et_exporter_donnees_du(
    chemin_fichier_du_2019: Path,
    chemin_fichier_du_2017: Path,
    chemin_dossier_output: Path,
) -> DataFrame:
    reinitialiser_dossier(chemin_dossier_output)

    donnees_du_2019 = chargeur_donnees_pesticides.charger_source_du_2016_2018(chemin_fichier_du_2019)
    donnees_du_2019 = _nettoyer_noms_substances(donnees_du_2019)
    donnees_du_2019["dose_unite_kg_par_ha"] = donnees_du_2019["dose_unite_kg_par_ha_2018"]
    donnees_du_2019["annee_dose_unite"] = 2018

    donnees_du_2017 = chargeur_donnees_pesticides.charger_source_du_2011_2015(chemin_fichier_du_2017)
    donnees_du_2017 = _nettoyer_noms_substances(donnees_du_2017)
    donnees_du_2017["dose_unite_kg_par_ha"] = donnees_du_2017["dose_unite_kg_par_ha_2015"]
    donnees_du_2017["annee_dose_unite"] = 2015

    donnees_du = pd.concat([donnees_du_2019, donnees_du_2017])
    donnees_du = donnees_du.loc[:, ["substance", "dose_unite_kg_par_ha", "annee_dose_unite"]]
    donnees_du = donnees_du.sort_values("annee_dose_unite", ascending=False).groupby(["substance"]).first().reset_index()
    donnees_du = donnees_du.sort_values("substance")
    donnees_du.to_csv(
        chemin_dossier_output / "doses_unites.csv",
        sep=";",
        index=False,
        float_format="%.9f",
    )

    return donnees_du


@chronometre
def _calculer_donnees_bnvd(chemins_dossiers_bnvd: list[Path]) -> DataFrame:
    donnees_bnvd = chargeur_donnees_pesticides.charger_source_bnvd(chemins_dossiers_bnvd)
    donnees_bnvd = _nettoyer_noms_substances(donnees_bnvd)
    return donnees_bnvd


@chronometre
def _calculer_nodu_substances(donnees_bnvd: DataFrame, donnees_du: DataFrame) -> DataFrame:
    donnees_nodu = pd.merge(
        donnees_bnvd,
        donnees_du.loc[:, ["substance", "dose_unite_kg_par_ha"]],
        on="substance",
        how="left",
        validate="many_to_one",
    )
    donnees_nodu["NODU_ha"] = donnees_nodu["quantite_substance_kg"] / donnees_nodu["dose_unite_kg_par_ha"]

    return donnees_nodu


def _calculer_et_exporter_usages_produits(chemin_fichier_usages_produits: Path, chemin_dossier_output: Path) -> DataFrame:
    usages_produits = chargeur_donnees_pesticides.charger_usages_produits(chemin_fichier_usages_produits)
    usages_produits["usage_culture"] = usages_produits["usage"].str.split("*").str[0]
    usages_produits["usage_traitement"] = usages_produits["usage"].str.split("*").str[1]
    usages_produits["usage_cible"] = usages_produits["usage"].str.split("*").str[2]

    USAGES_CULTURES_HORS_AGRICOLE = [
        "Adjuvants",
        "Arbres et arbustes",
        "Gazons de graminées",
        "Jardin d'amateur",
        "Plantes d'intérieur",
        "Plantes d'intérieur et balcons",
    ]
    USAGES_TRAITEMENTS_HORS_AGRICOLE = ["Trt bois abattus", "Trt Troncs Charp. Branch."]
    USAGES_CIBLES_HORS_AGRICOLE = [
        "All. PJT, Abords non plant. (1)",
        "All. PJT, Cimet., Voies (1)",
        "Bord. Plans d'eau",
        "Locx Ordures Déchets",
        "Locx Struct. Matér. (POA)",
        "Locx Struct. Matér. (POV...)",
        "Matér. Transp. Ordures Déchets",
        "Sites Indust. (1)",
        "Voies ferrées",
    ]

    USAGES_CIBLES_SNCF = ["Voies ferrées"]

    USAGES_TRAITEMENTS_POST_RECOLTE = ["Trt Prod. Réc.", "Trt Prod.Réc."]

    usages_produits["usage_agricole"] = False
    usages_produits["usage_hors_agricole"] = False
    usages_produits["usage_sncf"] = False
    usages_produits["usage_post_recolte"] = False

    usages_produits["usage_hors_agricole"].mask(
        usages_produits["usage_culture"].isin(USAGES_CULTURES_HORS_AGRICOLE)
        | usages_produits["usage_traitement"].isin(USAGES_TRAITEMENTS_HORS_AGRICOLE)
        | usages_produits["usage_cible"].isin(USAGES_CIBLES_HORS_AGRICOLE),
        True,
        inplace=True,
    )
    usages_produits["usage_agricole"] = ~usages_produits["usage_hors_agricole"]

    usages_produits["usage_sncf"].mask(usages_produits["usage_cible"].isin(USAGES_CIBLES_SNCF), True, inplace=True)

    usages_produits["usage_post_recolte"].mask(
        usages_produits["usage_traitement"].isin(USAGES_TRAITEMENTS_POST_RECOLTE),
        True,
        inplace=True,
    )

    usages_produits["usage"] = True
    usages_produits = usages_produits.groupby(by=["amm", "produit"]).sum().reset_index()
    for i in [
        "usage_agricole",
        "usage_hors_agricole",
        "usage_sncf",
        "usage_post_recolte",
    ]:
        usages_produits[i] = round(usages_produits[i] / usages_produits["usage"] * 100, 0)
    usages_produits.drop(columns=["usage"], inplace=True)

    usages_produits.to_csv(
        chemin_dossier_output / "usages_produits.csv",
        sep=";",
        index=False,
        float_format="%.2f",
    )

    return usages_produits


@chronometre
def _ajouter_colonne_substance_retenue(donnees_nodu, classifications_substances_retenues):
    donnees_nodu["substance_retenue"] = False
    donnees_nodu["substance_retenue"].mask(
        donnees_nodu.classification.str.contains(classifications_substances_retenues, regex=True, case=False, na=False),
        True,
        inplace=True,
    )
    return donnees_nodu


@chronometre
def _calculer_nodu_par_territoires(donnees_intrants_agricoles: DataFrame, nodu_substances: DataFrame) -> DataFrame:
    nodu_substances_retenues = nodu_substances.loc[nodu_substances.substance_retenue & nodu_substances.fichier_source.str.contains("ACHAT")]
    nodu_substances_retenues = _calculer_quantite_substance_avec_et_sans_du(nodu_substances_retenues)

    nodu_substances_retenues = nodu_substances_retenues.loc[
        :,
        [
            "annee",
            "code_departement",
            "code_postal_acheteur",
            "quantite_substance_sans_du_kg",
            "quantite_substance_avec_du_kg",
            "NODU_ha",
        ],
    ]

    nodu_par_departements = (
        nodu_substances_retenues.groupby(by=["annee", "code_departement"]).sum().reset_index().rename(columns={"code_departement": "id_territoire"})
    )

    nodu_par_departements.id_territoire = traduire_code_insee_vers_id_departement_crater(nodu_par_departements.id_territoire)

    nodu_par_communes = _calculer_donnees_nodu_par_commune_acheteur(nodu_substances_retenues, donnees_intrants_agricoles)

    return pd.concat([nodu_par_departements, nodu_par_communes]).set_index(["id_territoire", "annee"])


def _calculer_quantite_substance_avec_et_sans_du(nodu_substances):
    nodu_substances = nodu_substances.assign(quantite_substance_sans_du_kg=0)
    nodu_substances = nodu_substances.assign(quantite_substance_avec_du_kg=0)

    nodu_substances["quantite_substance_sans_du_kg"].mask(
        nodu_substances.dose_unite_kg_par_ha.isna(),
        nodu_substances.quantite_substance_kg,
        inplace=True,
    )
    nodu_substances["quantite_substance_avec_du_kg"].mask(
        ~nodu_substances.dose_unite_kg_par_ha.isna(),
        nodu_substances.quantite_substance_kg,
        inplace=True,
    )

    return nodu_substances


@chronometre
def _ajouter_colonnes_nodu_par_territoires(donnees_intrants_agricoles: DataFrame, nodu_par_territoires: DataFrame) -> DataFrame:
    # on duplique chaque territoire pour chaque annee
    donnees_intrants_agricoles = produit_cartesien(
        donnees_intrants_agricoles.reset_index(),
        nodu_par_territoires.reset_index().loc[:, ["annee"]].drop_duplicates(),
    ).set_index(["id_territoire", "annee"])
    donnees_intrants_agricoles["quantite_substance_sans_du_kg"] = 0.0
    donnees_intrants_agricoles["quantite_substance_avec_du_kg"] = 0.0
    donnees_intrants_agricoles["NODU_ha"] = 0.0
    donnees_intrants_agricoles.update(nodu_par_territoires)
    return donnees_intrants_agricoles


def exporter_statistiques(
    nodu_substances: DataFrame,
    chemin_fichier_usages_produits: Path,
    chemin_dossier_output: Path,
) -> None:
    reinitialiser_dossier(chemin_dossier_output)

    usages_produits = _calculer_et_exporter_usages_produits(chemin_fichier_usages_produits, chemin_dossier_output)

    donnees_nodu = nodu_substances.copy()

    donnees_nodu["fichier_source"].mask(
        donnees_nodu.fichier_source == "DPT",
        "DPT-" + donnees_nodu.code_departement,
        inplace=True,
    )

    donnees_nodu["du_manquant"] = False
    donnees_nodu.loc[
        donnees_nodu.dose_unite_kg_par_ha.isnull() & ~donnees_nodu.quantite_substance_kg.isnull(),
        "du_manquant",
    ] = True
    donnees_nodu.loc[donnees_nodu.du_manquant, "quantite_substance_sans_du_kg"] = donnees_nodu.loc[donnees_nodu.du_manquant, "quantite_substance_kg"]
    donnees_nodu["du_present"] = ~donnees_nodu["du_manquant"]

    donnees_nodu_achats = donnees_nodu.loc[donnees_nodu.fichier_source.str.contains("ACHAT")]

    donnees_nodu_achats_avec_usages = donnees_nodu_achats.merge(usages_produits, on="amm", how="left")
    donnees_nodu_achats_avec_usages["usage_manquant"] = False
    donnees_nodu_achats_avec_usages.loc[donnees_nodu_achats_avec_usages["produit"].isnull(), "usage_manquant"] = True
    donnees_nodu_achats_avec_usages["usage_present"] = ~donnees_nodu_achats_avec_usages["usage_manquant"]

    _exporter_statistiques(donnees_nodu, chemin_dossier_output)
    _exporter_statistiques_par_departements(donnees_nodu, chemin_dossier_output)
    _exporter_statistiques_codes_postaux(donnees_nodu_achats, chemin_dossier_output)
    _exporter_classement_substances(donnees_nodu, chemin_dossier_output)

    _exporter_statistiques_substances_sans_du(donnees_nodu_achats, chemin_dossier_output)
    _exporter_substances_sans_du(donnees_nodu_achats, chemin_dossier_output)

    _exporter_statistiques_usages(donnees_nodu_achats_avec_usages, chemin_dossier_output)
    _exporter_substances_sans_usages(donnees_nodu_achats_avec_usages, chemin_dossier_output)


def _exporter_statistiques_usages(donnees_nodu_avec_usages, chemin_dossier_output) -> None:
    donnees_nodu_avec_usages.reset_index().loc[:, ["annee", "amm", "produit", "usage_present", "usage_manquant"]].drop_duplicates().groupby(
        by=["annee"], dropna=False
    ).sum().reset_index().to_csv(
        chemin_dossier_output / "produits_sans_usages_statistiques.csv",
        sep=";",
        index=False,
    )

    donnees_nodu_avec_usages["usage"] = "agricole uniquement"
    donnees_nodu_avec_usages["usage"].mask(donnees_nodu_avec_usages.usage_hors_agricole > 0, "mixte", inplace=True)
    donnees_nodu_avec_usages["usage"].mask(
        donnees_nodu_avec_usages.usage_hors_agricole == 100,
        "hors agricole uniquement",
        inplace=True,
    )
    donnees_nodu_avec_usages["usage"].mask(donnees_nodu_avec_usages.usage_manquant, "manquant", inplace=True)
    donnees_nodu_avec_usages.query('fichier_source == "ACHAT_FR"').reset_index().loc[
        :,
        [
            "annee",
            "usage",
            "quantite_substance_kg",
            "NODU_ha",
            "quantite_substance_sans_du_kg",
        ],
    ].drop_duplicates().groupby(by=["annee", "usage"], dropna=False).sum().reset_index().to_csv(
        chemin_dossier_output / "produits_quantites_sans_usages_statistiques.csv",
        sep=";",
        index=False,
        float_format="%.2f",
    )


def _exporter_substances_sans_usages(donnees_nodu_avec_usages, chemin_dossier_output) -> None:
    donnees_nodu_avec_usages.loc[donnees_nodu_avec_usages.usage_manquant, ["amm"]].drop_duplicates().reset_index(drop=True).sort_values(
        ["amm"]
    ).to_csv(
        chemin_dossier_output / "produits_sans_usages.csv",
        sep=";",
        index=False,
        float_format="%.2f",
    )


def _exporter_statistiques_substances_sans_du(donnees_nodu, chemin_dossier_output) -> None:
    donnees_nodu.reset_index().loc[:, ["annee", "substance", "cas", "classification", "du_present", "du_manquant"]].drop_duplicates().groupby(
        by=["annee", "classification"], dropna=False
    ).sum().reset_index().to_csv(
        chemin_dossier_output / "substances_sans_du_statistiques.csv",
        sep=";",
        index=False,
    )


def _exporter_substances_sans_du(donnees_nodu, chemin_dossier_output) -> None:
    donnees_nodu.loc[donnees_nodu.du_manquant].reset_index()[["substance", "cas", "classification", "annee"]].drop_duplicates().groupby(
        by=["substance", "cas"], dropna=False
    ).agg(
        {
            "classification": lambda x: " | ".join(x.unique()),
            "annee": lambda x: " | ".join(sorted((str(i) for i in x))),
        }
    ).reset_index().sort_values(
        ["substance", "classification"]
    ).to_csv(
        chemin_dossier_output / "substances_sans_du.csv", sep=";", index=False
    )


def _exporter_classement_substances(donnees_nodu, chemin_dossier_output) -> None:
    classements_substances = (
        donnees_nodu[
            [
                "fichier_source",
                "annee",
                "substance",
                "classification",
                "quantite_substance_kg",
                "NODU_ha",
                "quantite_substance_sans_du_kg",
            ]
        ]
        .groupby(by=["fichier_source", "annee", "substance", "classification"], dropna=False)
        .sum()
    )
    classements_substances["rang_quantite"] = classements_substances.groupby(by=["fichier_source", "annee"])["quantite_substance_kg"].rank(
        method="min", ascending=False
    )
    classements_substances["rang_NODU"] = classements_substances.groupby(by=["fichier_source", "annee"])["NODU_ha"].rank(
        method="min", ascending=False
    )
    classements_substances.loc[(classements_substances.rang_quantite <= 10) | (classements_substances.rang_NODU <= 10)].to_csv(
        chemin_dossier_output / "resultats_classement_substances_top10.csv",
        sep=";",
        float_format="%.2f",
    )
    classements_substances.query("annee == 2020").to_csv(
        chemin_dossier_output / "resultats_classement_substances_complet_2020.csv",
        sep=";",
        float_format="%.2f",
    )


def _exporter_statistiques(donnees_nodu, chemin_dossier_output) -> None:
    donnees_nodu[["fichier_source", "annee", "classification", "quantite_substance_kg", "NODU_ha", "quantite_substance_sans_du_kg",]].groupby(
        by=["fichier_source", "annee", "classification"], dropna=False
    ).sum().to_csv(
        chemin_dossier_output / "resultats_statistiques.csv",
        sep=";",
        float_format="%.2f",
    )


def _exporter_statistiques_par_departements(donnees_nodu, chemin_dossier_output) -> None:
    donnees_nodu.loc[donnees_nodu.fichier_source.str.contains("DPT")][
        [
            "fichier_source",
            "annee",
            "code_departement",
            "classification",
            "quantite_substance_kg",
            "NODU_ha",
            "quantite_substance_sans_du_kg",
        ]
    ].groupby(
        by=["fichier_source", "code_departement", "annee", "classification"],
        dropna=False,
    ).sum().to_csv(
        chemin_dossier_output / "resultats_statistiques_par_departements.csv",
        sep=";",
        float_format="%.2f",
    )


def _exporter_statistiques_codes_postaux(donnees_nodu, chemin_dossier_output) -> None:
    donnees_nodu = donnees_nodu.assign(code_postal_acheteur_categorie="présent")
    donnees_nodu.loc[donnees_nodu["code_postal_acheteur"].isnull(), "code_postal_acheteur_categorie"] = "NA"
    donnees_nodu.loc[
        (donnees_nodu["code_postal_acheteur"] == "00000"),
        "code_postal_acheteur_categorie",
    ] = "00000"

    donnees_nodu[
        [
            "fichier_source",
            "annee",
            "code_postal_acheteur_categorie",
            "classification",
            "quantite_substance_kg",
            "NODU_ha",
            "quantite_substance_sans_du_kg",
        ]
    ].groupby(
        by=[
            "fichier_source",
            "annee",
            "code_postal_acheteur_categorie",
            "classification",
        ],
        dropna=False,
    ).sum().to_csv(
        chemin_dossier_output / "resultats_statistiques_codes_postaux.csv",
        sep=";",
        float_format="%.2f",
    )


def _nettoyer_noms_substances(df: DataFrame) -> DataFrame:
    df["substance"] = df["substance"].str.lower()
    remplacements = {"é": "e", "è": "e", "ê": "e", "à": "a"}
    for char in remplacements.keys():
        df["substance"] = df["substance"].str.replace(char, remplacements[char])

    return df


def _calculer_repartition_codes_postaux_dans_communes(
    territoires: DataFrame,
) -> DataFrame:
    df = territoires.loc[
        territoires.categorie_territoire == "COMMUNE",
        ["codes_postaux_commune", "sau_ra_2020_ha"],
    ].reset_index()
    df = df[df.codes_postaux_commune.notnull()]
    df.codes_postaux_commune = df.codes_postaux_commune.str.split("|")
    df = df.explode("codes_postaux_commune")
    df = df.rename(columns={"codes_postaux_commune": "code_postal_commune"})
    # on ajoute un tout petit peu de SAU pour gérer le cas où un code postal est composé de communes sans sau
    df["sau_ra_2020_ha"] += 0.001
    df2 = (
        df.loc[:, ["code_postal_commune", "sau_ra_2020_ha"]]
        .groupby("code_postal_commune")
        .sum()
        .rename(columns={"sau_ra_2020_ha": "sau_totale_par_code_postal_ha"})
    )
    df = df.join(df2, on="code_postal_commune", how="left")
    df["cle_repartition"] = round(df["sau_ra_2020_ha"] / df["sau_totale_par_code_postal_ha"], 5)

    return df.reset_index(drop=True).drop(columns=["sau_totale_par_code_postal_ha"])


def _calculer_donnees_nodu_par_commune_acheteur(nodu_par_code_postal: DataFrame, df_intrants_agricoles: DataFrame) -> DataFrame:
    repartition_codes_postaux_dans_communes = _calculer_repartition_codes_postaux_dans_communes(df_intrants_agricoles)

    nodu_par_communes = nodu_par_code_postal.groupby(by=["annee", "code_postal_acheteur"]).sum().reset_index()

    nodu_par_communes = nodu_par_communes.merge(
        repartition_codes_postaux_dans_communes.loc[:, ["id_territoire", "code_postal_commune", "cle_repartition"]],
        left_on="code_postal_acheteur",
        right_on="code_postal_commune",
        how="left",
    )
    nodu_par_communes["quantite_substance_sans_du_kg"] *= nodu_par_communes["cle_repartition"]
    nodu_par_communes["quantite_substance_avec_du_kg"] *= nodu_par_communes["cle_repartition"]
    nodu_par_communes["NODU_ha"] *= nodu_par_communes["cle_repartition"]
    nodu_par_communes.drop(columns=["cle_repartition"], inplace=True)
    nodu_par_communes = nodu_par_communes.groupby(by=["id_territoire", "annee"]).sum().reset_index()

    return nodu_par_communes


@chronometre
def _calculer_valeurs_supracommunales(donnees_intrants_agricoles):
    donnees_intrants_agricoles = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(
        donnees_intrants_agricoles,
        "quantite_substance_sans_du_kg",
        "COMMUNE",
        ["REGROUPEMENT_COMMUNES", "EPCI"],
    )
    donnees_intrants_agricoles = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(
        donnees_intrants_agricoles,
        "quantite_substance_sans_du_kg",
        "DEPARTEMENT",
        ["REGION", "PAYS"],
    )
    donnees_intrants_agricoles = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(
        donnees_intrants_agricoles,
        "quantite_substance_avec_du_kg",
        "COMMUNE",
        ["REGROUPEMENT_COMMUNES", "EPCI"],
    )
    donnees_intrants_agricoles = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(
        donnees_intrants_agricoles,
        "quantite_substance_avec_du_kg",
        "DEPARTEMENT",
        ["REGION", "PAYS"],
    )
    donnees_intrants_agricoles = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(
        donnees_intrants_agricoles,
        "NODU_ha",
        "COMMUNE",
        ["REGROUPEMENT_COMMUNES", "EPCI"],
    )
    donnees_intrants_agricoles = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(
        donnees_intrants_agricoles,
        "NODU_ha",
        "DEPARTEMENT",
        ["REGION", "PAYS"],
    )
    return donnees_intrants_agricoles


@chronometre
def _calculer_nodu_normalise(donnees_intrants_agricoles):
    donnees_intrants_agricoles["NODU_normalise"] = donnees_intrants_agricoles["NODU_ha"] / donnees_intrants_agricoles["sau_ra_2020_ha"]
    donnees_intrants_agricoles["NODU_normalise"] = donnees_intrants_agricoles["NODU_normalise"].replace([np.inf, -np.inf], np.nan)
    return donnees_intrants_agricoles


def _calculer_moyennes_triennales_indicateurs_intrants(df_intrants):
    liste_champs_indicateurs = [
        "quantite_substance_sans_du_kg",
        "quantite_substance_avec_du_kg",
        "NODU_ha",
    ]
    indicateurs_intrants_moyens_triennaux = calculer_moyenne_glissante(df_intrants.loc[:, liste_champs_indicateurs], "annee", 3)
    return df_intrants.loc[:, df_intrants.columns.drop(liste_champs_indicateurs)].join(indicateurs_intrants_moyens_triennaux, how="right")
