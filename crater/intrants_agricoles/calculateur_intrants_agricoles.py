from pathlib import Path
from typing import List

from pandas import DataFrame

from crater.commun.config import IDS_CATEGORIES_TERRITOIRES_SUPRACOMMUNAUX
from crater.commun.export_fichier import (
    reinitialiser_dossier,
    exporter_df_indicateurs_par_territoires,
)
from crater.commun.logger import log
from crater.intrants_agricoles.pesticides import (
    calculer_indicateurs_pesticides,
    calculer_indicateurs_intrants_moyennes_triennales,
    exporter_statistiques,
)


def calculer_intrants_agricoles(
    territoires: DataFrame,
    classifications_substances_retenues: str,
    chemins_dossiers_bnvd: List[Path],
    chemin_fichier_du_2019: Path,
    chemin_fichier_du_2017: Path,
    chemin_fichier_usages_produits: Path,
    chemin_dossier_output: Path,
) -> None:
    log.info("##### CALCUL INTRANTS AGRICOLES #####")

    reinitialiser_dossier(chemin_dossier_output)

    colonnes = (
        ["nom_territoire", "categorie_territoire"]
        + IDS_CATEGORIES_TERRITOIRES_SUPRACOMMUNAUX
        + ["annee_dernier_mouvement_commune", "sau_ra_2020_ha", "codes_postaux_commune"]
    )
    df_intrants_toutes_annees = territoires[colonnes]

    (df_intrants_toutes_annees, nodu_substances,) = calculer_indicateurs_pesticides(
        df_intrants_toutes_annees,
        classifications_substances_retenues,
        chemins_dossiers_bnvd,
        chemin_fichier_du_2019,
        chemin_fichier_du_2017,
        chemin_dossier_output,
    )

    df_intrants_moyennes_triennales = calculer_indicateurs_intrants_moyennes_triennales(df_intrants_toutes_annees)

    df_intrants = _calculer_indicateurs_intrants(df_intrants_moyennes_triennales)

    exporter_statistiques(
        nodu_substances,
        chemin_fichier_usages_produits,
        chemin_dossier_output / "statistiques",
    )

    _exporter(
        df_intrants,
        df_intrants_toutes_annees,
        df_intrants_moyennes_triennales,
        chemin_dossier_output,
    )


def _calculer_indicateurs_intrants(df_intrants_moyennes_triennales):
    df_intrants = df_intrants_moyennes_triennales.reset_index()
    df_intrants = df_intrants.loc[df_intrants.annee == max(df_intrants.annee)]
    df_intrants = df_intrants.set_index("id_territoire")
    df_intrants = _ajouter_note(df_intrants)
    df_intrants = df_intrants.loc[:, ["nom_territoire", "categorie_territoire", "NODU_normalise", "note"]]
    return df_intrants


def _ajouter_note(df_intrants):
    df_intrants.loc[:, "note"] = (10 * (1 - df_intrants["NODU_normalise"])).clip(lower=0, upper=10).round(0).astype("Int64")
    return df_intrants


def _exporter(
    df_intrants: DataFrame,
    df_intrants_toutes_annees: DataFrame,
    df_intrants_moyennes_triennales: DataFrame,
    chemin_dossier_output: Path,
) -> None:
    log.info("##### EXPORT INTRANTS AGRICOLES #####")

    exporter_df_indicateurs_par_territoires(
        df_intrants,
        chemin_dossier_output,
        "intrants_agricoles",
    )
    df_intrants_toutes_annees_export = df_intrants_toutes_annees.drop(
        columns=["annee_dernier_mouvement_commune"] + IDS_CATEGORIES_TERRITOIRES_SUPRACOMMUNAUX
    )
    exporter_df_indicateurs_par_territoires(
        df_intrants_toutes_annees_export.reset_index("annee"),
        chemin_dossier_output,
        "intrants_agricoles_toutes_annees",
        "annee",
    )
    df_intrants_moyennes_triennales_export = df_intrants_moyennes_triennales.drop(
        columns=["annee_dernier_mouvement_commune"] + IDS_CATEGORIES_TERRITOIRES_SUPRACOMMUNAUX
    )
    exporter_df_indicateurs_par_territoires(
        df_intrants_moyennes_triennales_export.reset_index("annee"),
        chemin_dossier_output,
        "intrants_agricoles_moyennes_triennales",
        "annee",
    )
