from typing import List
import numpy as np
import pandas as pd
from pandas import DataFrame

from crater.commun.config import CATEGORIES_TERRITOIRES
from crater.commun.outils_dataframes import update_colonne_depuis_series_incluant_NAs


def categories_territoires_hors_communes(categories_territoires):
    return [i for i in categories_territoires if i != "COMMUNE"]


def traduire_categorie_territoire_vers_id_categorie_territoire(categorie_territoire):
    return "id_" + categorie_territoire.lower()


def traduire_code_insee_vers_id_commune_crater(df_colonne):
    return "C-" + df_colonne


def traduire_code_insee_vers_id_epci_crater(df_colonne):
    return "E-" + df_colonne


def traduire_code_insee_vers_id_departement_crater(df_colonne):
    return "D-" + df_colonne


def traduire_code_insee_vers_id_region_crater(df_colonne):
    return "R-" + df_colonne


def traduire_code_insee_vers_id_pays_crater(df_colonne):
    return "P-" + df_colonne


def obtenir_arrondissements_depuis_territoires(territoires: DataFrame) -> DataFrame:
    arrondissements = territoires.query("categorie_territoire == 'COMMUNE'").reset_index().loc[:, ["id_territoire", "ids_arrondissements_commune"]]
    arrondissements = arrondissements[arrondissements["ids_arrondissements_commune"].notna()]
    arrondissements["ids_arrondissements_commune"] = arrondissements.ids_arrondissements_commune.str.split("|")
    arrondissements = arrondissements.explode("ids_arrondissements_commune")
    arrondissements = arrondissements.rename(
        columns={
            "ids_arrondissements_commune": "id_arrondissement",
            "id_territoire": "id_commune",
        }
    )

    return arrondissements


def extraire_df_communes_appartenant_a_une_categorie_de_territoire(territoires: DataFrame, categorie_territoire: str) -> DataFrame:
    id_categorie_territoire = traduire_categorie_territoire_vers_id_categorie_territoire(categorie_territoire)
    if categorie_territoire == "REGROUPEMENT_COMMUNES":
        communes = territoires.query("categorie_territoire == 'COMMUNE'")
        communes = communes[communes.ids_regroupements_communes.notnull()]
        # Si aucune commune n'appartient à un regroupement de commune, on retourne le df vide
        if communes.shape[0] == 0:
            return communes
        communes.ids_regroupements_communes = communes.ids_regroupements_communes.str.split("|")
        communes = communes.explode("ids_regroupements_communes")
        communes = communes.rename(columns={"ids_regroupements_communes": "id_regroupement_communes"})
    else:
        communes = territoires.query("categorie_territoire == 'COMMUNE'")
        communes = communes[communes[id_categorie_territoire].notnull()]

    return communes


def ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(
    territoires: DataFrame,
    colonne_a_sommer: str,
    categorie_territoire_a_sommer: str = "COMMUNE",
    categories_territoires_cibles: List = categories_territoires_hors_communes(CATEGORIES_TERRITOIRES),
    compter_part_valeurs_na=False,
) -> DataFrame:
    """L'index du DataFrame territoires passé en paramètre doit contenir au moins une colonne avec l'id territoire.
    S'il possède des colonnes supplémentaires (cad un multi index, avec par ex la colonne annee)
    la somme sera faite par groupby sur l'ensemble de l'index multiple (par ex calcul de somme pour chaque territoire et chaque année)"""
    type = territoires.dtypes[colonne_a_sommer]
    if type not in ["Int64", "int64", "Float64", "float64"]:
        raise ValueError(f"ERREUR: type de colonne non valide ({type}) pour l'agregation de la colonne {colonne_a_sommer}")

    for categorie_territoire in categories_territoires_cibles:
        id_categorie_territoire = traduire_categorie_territoire_vers_id_categorie_territoire(categorie_territoire)
        territoires_a_agreger = _extraire_df_territoires_a_agreger(territoires, categorie_territoire_a_sommer, categorie_territoire)

        if territoires_a_agreger.shape[0] > 0:
            donnees_a_agreger = (
                territoires_a_agreger.loc[:, [id_categorie_territoire, colonne_a_sommer]]
                .set_index(id_categorie_territoire, append=True)
                .reset_index("id_territoire", drop=True)
                .rename_axis(index={id_categorie_territoire: "id_territoire"})
            )
            if donnees_a_agreger.index.nlevels > 1:
                donnees_a_agreger = donnees_a_agreger.reorder_levels(
                    [donnees_a_agreger.index.nlevels - 1] + list(range(donnees_a_agreger.index.nlevels - 1))
                )

            donnees_agregees = donnees_a_agreger.groupby(donnees_a_agreger.index.names).sum(min_count=1)

            donnees_agregees = donnees_agregees.join(_calculer_part_valeurs_na(donnees_a_agreger, colonne_a_sommer))

            territoires = update_colonne_depuis_series_incluant_NAs(territoires, colonne_a_sommer, donnees_agregees[colonne_a_sommer])
            if compter_part_valeurs_na is True:
                if colonne_a_sommer + "_part_na_pourcent" not in territoires:
                    territoires.loc[:, colonne_a_sommer + "_part_na_pourcent"] = np.nan

                territoires = update_colonne_depuis_series_incluant_NAs(
                    territoires, colonne_a_sommer + "_part_na_pourcent", donnees_agregees[colonne_a_sommer + "_part_na_pourcent"]
                )

    territoires = territoires.astype(dtype={colonne_a_sommer: type})
    return territoires


def _extraire_df_territoires_a_agreger(
    territoires: pd.DataFrame, categorie_territoire_a_sommer: str, categorie_territoire_cible: str
) -> pd.DataFrame:
    if categorie_territoire_a_sommer == "COMMUNE":
        territoires_a_agreger = extraire_df_communes_appartenant_a_une_categorie_de_territoire(territoires, categorie_territoire_cible)
    else:
        territoires_a_agreger = territoires.loc[territoires["categorie_territoire"] == categorie_territoire_a_sommer]
    return territoires_a_agreger


def _calculer_part_valeurs_na(df: pd.DataFrame, colonne_a_sommer: str) -> pd.DataFrame:

    return (
        (100 - df.groupby(df.index.names).count() / df.fillna(0).groupby(df.index.names).count() * 100)
        .round()
        .rename(columns={colonne_a_sommer: colonne_a_sommer + "_part_na_pourcent"})
    )


def ajouter_donnees_supraterritoriales_par_moyenne_ponderee_donnees_territoriales(
    territoires: DataFrame, nom_colonne_valeur, nom_colonne_poids
) -> DataFrame:
    territoires["colonne_valeur_ponderee"] = territoires[nom_colonne_valeur] * territoires[nom_colonne_poids]
    territoires["colonne_poids"] = territoires[nom_colonne_poids]
    territoires.loc[territoires[nom_colonne_valeur].isnull(), "colonne_poids"] = np.nan

    territoires = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(territoires, "colonne_valeur_ponderee")
    territoires = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(territoires, "colonne_poids")

    mask_territoires_cible = territoires["categorie_territoire"].isin(categories_territoires_hors_communes(CATEGORIES_TERRITOIRES))
    territoires.loc[mask_territoires_cible, nom_colonne_valeur] = (
        territoires.loc[mask_territoires_cible, "colonne_valeur_ponderee"] / territoires.loc[mask_territoires_cible, "colonne_poids"]
    )
    territoires = territoires.drop(columns="colonne_valeur_ponderee")
    territoires = territoires.drop(columns="colonne_poids")
    return territoires


def trier_territoires(territoires):
    territoires["categorie_territoire_pour_tri"] = pd.Categorical(
        territoires["categorie_territoire"],
        categories=CATEGORIES_TERRITOIRES[::-1],
        ordered=True,
    )
    colonnes_tri = ["categorie_territoire_pour_tri", "id_territoire"]
    sens_tri = [True, True]
    if "annee" in territoires.index.names:
        colonnes_tri = colonnes_tri + ["annee"]
        sens_tri = sens_tri + [True]

    return territoires.sort_values(by=colonnes_tri, ascending=sens_tri).drop(columns="categorie_territoire_pour_tri")


def calculer_donnees_infraterritoriales_par_repartition_donnees_supraterritoriales(
    territoires: DataFrame,
    colonne_valeur: str,
    colonne_poids: str,
    categorie_territoire_inferieur: str,
    categorie_territoire_superieur: str,
) -> DataFrame:

    type = territoires.dtypes[colonne_valeur]

    df_complet = territoires.copy().reset_index()

    id_categorie_territoire_superieur = traduire_categorie_territoire_vers_id_categorie_territoire(categorie_territoire_superieur)

    df_par_territoires_inferieurs = df_complet.loc[df_complet["categorie_territoire"] == categorie_territoire_inferieur].copy()

    df_par_territoires_inferieurs = _ajouter_colonne_total_via_somme_territoires_inferieurs(
        df_par_territoires_inferieurs,
        df_complet,
        colonne_valeur,
        categorie_territoire_inferieur,
        id_categorie_territoire_superieur,
    )

    df_par_territoires_inferieurs = _ajouter_colonne_total_via_territoire_superieur(
        df_par_territoires_inferieurs,
        df_complet,
        colonne_valeur,
        categorie_territoire_superieur,
        id_categorie_territoire_superieur,
    )

    df_par_territoires_inferieurs = _ajouter_colonne_total_a_repartir(df_par_territoires_inferieurs, colonne_valeur)

    df_par_territoires_inferieurs_a_estimer = _calculer_estimations(
        df_par_territoires_inferieurs,
        colonne_valeur,
        colonne_poids,
        id_categorie_territoire_superieur,
    )

    df_complet = df_complet.set_index("id_territoire")
    if colonne_valeur + "_est_estime" not in df_complet:
        df_complet[colonne_valeur + "_est_estime"] = False
    df_complet.update(df_par_territoires_inferieurs_a_estimer.loc[:, [colonne_valeur, colonne_valeur + "_est_estime"]])

    if type == "Int64":
        df_complet = df_complet.astype(dtype={colonne_valeur: "Float64"}).round()
    df_complet = df_complet.astype(dtype={colonne_valeur: type})

    return df_complet


def _calculer_estimations(
    df_par_territoires_inferieurs: pd.DataFrame,
    colonne_valeur: str,
    colonne_poids: str,
    id_categorie_territoire_superieur: str,
):
    df_par_territoires_inferieurs_a_estimer = df_par_territoires_inferieurs.loc[df_par_territoires_inferieurs[colonne_valeur].isna()].copy()

    df_par_territoires_inferieurs_a_estimer["poids"] = df_par_territoires_inferieurs_a_estimer[colonne_poids].fillna(0)

    df_par_territoires_inferieurs_a_estimer = pd.merge(
        df_par_territoires_inferieurs_a_estimer,
        df_par_territoires_inferieurs_a_estimer.loc[:, [id_categorie_territoire_superieur, "poids"]]
        .groupby(id_categorie_territoire_superieur)
        .sum()
        .rename(columns={"poids": "poids_total"})
        .reset_index(),
        how="left",
        on=id_categorie_territoire_superieur,
    )

    df_par_territoires_inferieurs_a_estimer.loc[
        df_par_territoires_inferieurs_a_estimer["poids_total"] == 0,
        "poids",
    ] = 1
    df_par_territoires_inferieurs_a_estimer = pd.merge(
        df_par_territoires_inferieurs_a_estimer.drop(columns=["poids_total"]),
        df_par_territoires_inferieurs_a_estimer.loc[:, [id_categorie_territoire_superieur, "poids"]]
        .groupby(id_categorie_territoire_superieur)
        .sum()
        .rename(columns={"poids": "poids_total"})
        .reset_index(),
        how="left",
        on=id_categorie_territoire_superieur,
    )

    df_par_territoires_inferieurs_a_estimer[colonne_valeur] = (
        df_par_territoires_inferieurs_a_estimer[colonne_valeur + "_total_a_repartir"]
        * df_par_territoires_inferieurs_a_estimer["poids"]
        / df_par_territoires_inferieurs_a_estimer["poids_total"]
    )

    df_par_territoires_inferieurs_a_estimer[colonne_valeur + "_est_estime"] = True

    return df_par_territoires_inferieurs_a_estimer.set_index("id_territoire")


def _ajouter_colonne_total_a_repartir(df_par_territoires_inferieurs: pd.DataFrame, colonne_valeur: str):
    df_par_territoires_inferieurs.loc[:, colonne_valeur + "_total_a_repartir"] = (
        df_par_territoires_inferieurs[colonne_valeur + "_total_via_territoire_superieur"]
        - df_par_territoires_inferieurs[colonne_valeur + "_total_via_somme_territoires_inferieurs"]
    )
    return df_par_territoires_inferieurs


def _ajouter_colonne_total_via_territoire_superieur(
    df_par_territoires_inferieurs: pd.DataFrame,
    df_complet: pd.DataFrame,
    colonne_valeur: str,
    categorie_territoire_superieure: str,
    id_categorie_territoire_superieur: str,
):
    return df_par_territoires_inferieurs.merge(
        df_complet.loc[df_complet["categorie_territoire"] == categorie_territoire_superieure, ["id_territoire", colonne_valeur],].rename(
            columns={
                colonne_valeur: colonne_valeur + "_total_via_territoire_superieur",
                "id_territoire": id_categorie_territoire_superieur,
            }
        ),
        how="left",
        on=id_categorie_territoire_superieur,
    )


def _ajouter_colonne_total_via_somme_territoires_inferieurs(
    df_par_territoires_inferieurs: pd.DataFrame,
    df_complet: pd.DataFrame,
    colonne_valeur: str,
    categorie_territoire_inferieure: str,
    id_categorie_territoire_superieur: str,
):
    return df_par_territoires_inferieurs.merge(
        df_complet.loc[
            df_complet["categorie_territoire"] == categorie_territoire_inferieure,
            [id_categorie_territoire_superieur, colonne_valeur],
        ]
        .groupby(id_categorie_territoire_superieur)
        .sum()
        .rename(columns={colonne_valeur: colonne_valeur + "_total_via_somme_territoires_inferieurs"})
        .reset_index(),
        how="outer",
        on=id_categorie_territoire_superieur,
    )


def remplir_de_zeros_sous_colonnes_si_totaux_ok(
    donnees: pd.DataFrame, variable: str, classe_a: str, classe_z: str, categories_territoires: list[str] = ["COMMUNE"]
):
    sommes = donnees.loc[:, slice(variable + "." + classe_a, variable + "." + classe_z)].sum(axis=1)

    mask_donnees = (donnees["categorie_territoire"].isin(categories_territoires)) & (donnees[variable + ".ensemble"] == sommes)

    donnees.loc[mask_donnees, slice(variable + "." + classe_a, variable + "." + classe_z)] = donnees.loc[
        mask_donnees, slice(variable + "." + classe_a, variable + "." + classe_z)
    ].fillna(0)
