from pathlib import Path

from crater.commun.config import CATEGORIES_TERRITOIRES
from crater.commun.export_fichier import reinitialiser_dossier


def generer_histogrammes(df_indicateur, nom_colonnes_indicateurs, chemin_dossier_output: Path):
    reinitialiser_dossier(chemin_dossier_output)

    for nom_colonne_indicateur in nom_colonnes_indicateurs:
        for categorie_territoire in CATEGORIES_TERRITOIRES:
            _generer_histogramme_indicateur_categorie_territoire(
                df_indicateur,
                chemin_dossier_output,
                categorie_territoire,
                nom_colonne_indicateur,
            )


def _generer_histogramme_indicateur_categorie_territoire(
    df_indicateur,
    chemin_dossier_output: Path,
    categorie_territoire,
    nom_colonne_indicateur,
):
    df_pour_categorie_territoire = df_indicateur.loc[df_indicateur["categorie_territoire"].isin([categorie_territoire])]
    histogramme_note = df_pour_categorie_territoire[nom_colonne_indicateur].hist(bins=50)

    fig = histogramme_note.get_figure()
    fig.savefig(f"{chemin_dossier_output}/{nom_colonne_indicateur}_{categorie_territoire}_hist.png")
    # Supprimer la figure courante, sinon les histogrammes s'empilent
    fig.clear()
