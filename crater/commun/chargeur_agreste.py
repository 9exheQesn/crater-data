import zipfile
from pathlib import Path

import pandas as pd

from crater.commun.outils_territoires import (
    traduire_code_insee_vers_id_commune_crater,
    traduire_code_insee_vers_id_departement_crater,
    traduire_code_insee_vers_id_region_crater,
    traduire_code_insee_vers_id_pays_crater,
)


def charger_agreste(chemin_fichier_zip: Path, nom_fichier_dans_zip: str, prefixe_nom_colonnes: str):
    return pd.read_csv(
        zipfile.ZipFile(chemin_fichier_zip, "r").open(nom_fichier_dans_zip),
        sep=";",
        dtype={
            "NOM": "str",
            "ANNREF": "str",
            "FRANCE": "str",
            "FRDOM": "str",
            "REGION": "str",
            "DEP": "str",
            "COM": "str",
            prefixe_nom_colonnes + "_DIM1": "str",
            prefixe_nom_colonnes + "_MOD_DIM1": "str",
            prefixe_nom_colonnes + "_LIB_DIM1": "str",
            prefixe_nom_colonnes + "_DIM2": "str",
            prefixe_nom_colonnes + "_MOD_DIM2": "str",
            prefixe_nom_colonnes + "_LIB_DIM2": "str",
            prefixe_nom_colonnes + "_DIM3": "str",
            prefixe_nom_colonnes + "_MOD_DIM3": "str",
            prefixe_nom_colonnes + "_LIB_DIM3": "str",
            "VALEUR": "float",
            "QUALITE": "str",
        },
    )


def filtrer_et_ajouter_colonne_id_territoire(df):
    mask_communes = df["COM"] != "............"
    df.loc[mask_communes, "id_territoire"] = traduire_code_insee_vers_id_commune_crater(df.loc[mask_communes, "COM"])

    mask_departements = (df["COM"] == "............") & (df["DEP"] != "............")
    df.loc[mask_departements, "id_territoire"] = traduire_code_insee_vers_id_departement_crater(df.loc[mask_departements, "DEP"])

    mask_regions = (df["DEP"] == "............") & (df["REGION"] != "............")
    df.loc[mask_regions, "id_territoire"] = traduire_code_insee_vers_id_region_crater(df.loc[mask_regions, "REGION"].str.replace("NR", ""))

    mask_pays = (df["REGION"] == "............") & (df["FRDOM"] == "METRO")
    df.loc[mask_pays, "id_territoire"] = traduire_code_insee_vers_id_pays_crater(df.loc[mask_pays, "FRANCE"])

    df = df[df["id_territoire"].notna()]
    return df
