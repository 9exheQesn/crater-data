import pandas
import pandas as pd
from pandas import DataFrame

from crater.commun.config import CATEGORIES_TERRITOIRES


def calculer_statistiques(df: DataFrame, nom_colonne_regroupement=None):
    df_stats = _creer_df_statistiques(df, nom_colonne_regroupement)
    df_stats = _calculer_nouvelles_mesures_statistiques(df_stats)
    df_stats = _traiter_colonne_regroupement(df_stats, nom_colonne_regroupement)
    df_stats = _transposer_et_trier(df_stats, nom_colonne_regroupement)
    return df_stats


def _creer_df_statistiques(df_indicateurs, nom_colonne_regroupement):
    nom_colonnes_groupby = (
        (["categorie_territoire"] + [nom_colonne_regroupement]) if (nom_colonne_regroupement is not None) else ["categorie_territoire"]
    )
    noms_colonnes_indicateurs_numeriques = df_indicateurs.drop(columns=nom_colonnes_groupby).select_dtypes("number").columns.values.tolist()
    df_indicateurs = df_indicateurs.reset_index().loc[:, nom_colonnes_groupby + noms_colonnes_indicateurs_numeriques]
    df_stats = df_indicateurs.groupby(nom_colonnes_groupby, dropna=False).agg(["size", "count", "mean", "min", q1, "median", q3, "max"]).copy()
    df_stats.rename(columns={"mean": "moy", "median": "q2"}, inplace=True)
    df_stats.columns.set_names(["indicateur", "mesure_statistique"], inplace=True)
    return df_stats


def _calculer_nouvelles_mesures_statistiques(df_stats):
    for nom_colonne_indicateur in df_stats.columns.levels[0]:
        df_stats[nom_colonne_indicateur, "count"] = df_stats[nom_colonne_indicateur, "size"] - df_stats[nom_colonne_indicateur, "count"]
    df_stats = df_stats.rename(columns={"size": "nb_lignes", "count": "nb_nan"})
    return df_stats


def _traiter_colonne_regroupement(df_stats, nom_colonne_regroupement):
    if nom_colonne_regroupement is not None:
        df_stats = df_stats.reset_index().pivot(index="categorie_territoire", columns=nom_colonne_regroupement)
    return df_stats


def _transposer_et_trier(df_stats, nom_colonne_regroupement):
    nom_colonnes_stack = ["indicateur"] if (nom_colonne_regroupement is None) else ["indicateur", nom_colonne_regroupement]
    df_stats = (
        df_stats.stack(nom_colonnes_stack)
        .reset_index()
        .loc[
            :,
            ["categorie_territoire"] + nom_colonnes_stack + ["nb_lignes", "nb_nan", "moy", "min", "q1", "q2", "q3", "max"],
        ]
    )
    df_stats["moy"] = pd.to_numeric(df_stats["moy"])
    df_stats["min"] = pd.to_numeric(df_stats["min"])
    df_stats["q1"] = pd.to_numeric(df_stats["q1"])
    df_stats["q2"] = pd.to_numeric(df_stats["q2"])
    df_stats["q3"] = pd.to_numeric(df_stats["q3"])
    df_stats["max"] = pd.to_numeric(df_stats["max"])

    df_stats.columns.name = None

    df_stats["categorie_territoire_pour_tri"] = pandas.Categorical(
        df_stats["categorie_territoire"],
        categories=CATEGORIES_TERRITOIRES[::-1],
        ordered=True,
    )
    colonnes_tri = ["categorie_territoire_pour_tri", "indicateur"]
    sens_tri = [True, True]
    if nom_colonne_regroupement is not None:
        colonnes_tri = colonnes_tri + [nom_colonne_regroupement]
        sens_tri = sens_tri + [False]

    df_stats = df_stats.sort_values(by=colonnes_tri, ascending=sens_tri).drop(columns="categorie_territoire_pour_tri").reset_index(drop=True)
    return df_stats


def q1(x):
    return x.quantile(0.25)


def q3(x):
    return x.quantile(0.75)
