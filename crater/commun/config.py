from pathlib import Path

ANNEE_REFERENTIEL_COMMUNES_INSEE = 2022

CRS_PROJET = "EPSG:2154"

ID_FRANCE = "P-FR"
CATEGORIES_TERRITOIRES = [
    "COMMUNE",
    "EPCI",
    "REGROUPEMENT_COMMUNES",
    "DEPARTEMENT",
    "REGION",
    "PAYS",
]
IDS_CATEGORIES_TERRITOIRES_SUPRACOMMUNAUX = [
    "id_epci",
    "ids_regroupements_communes",
    "id_departement",
    "id_region",
    "id_pays",
]
CATEGORIES_REGROUPEMENTS_COMMUNES = {
    "ND": {"categorie": "NOUVEAU_DEPARTEMENT", "prefixe_id": "ND"},
    "BV2012": {"categorie": "BASSIN_DE_VIE_2012", "prefixe_id": "BV2012"},
    "PN": {"categorie": "PARC_NATIONAL", "prefixe_id": "PN"},
    "PNR": {"categorie": "PARC_NATUREL_REGIONAL", "prefixe_id": "PNR"},
    "PAYS_PETR": {"categorie": "PAYS_PETR", "prefixe_id": "PAYS_PETR"},
    "SCOT": {"categorie": "SCHEMA_COHERENCE_TERRITORIAL", "prefixe_id": "SCOT"},
    "PAT": {"categorie": "PROJET_ALIMENTAIRE_TERRITORIAL", "prefixe_id": "PAT"},
    "AT": {"categorie": "AUTRE_TERRITOIRE", "prefixe_id": "AT"},
}
NUMEROS_REGIONS_HORS_DROM = range(1, 10, 1)
NUMEROS_DEPARTEMENTS_DROM = ["97", "98"]

CLASSIFICATIONS_SUBSTANCES_RETENUES_TOUTES = ""  # regex
CLASSIFICATIONS_SUBSTANCES_RETENUES_TOUTES_HORS_AUTRE = "CMR|Santé|Env|Organique|minéral"  # regex
CLASSIFICATIONS_SUBSTANCES_RETENUES_CMR = "CMR"  # regex
CLASSIFICATIONS_SUBSTANCES_RETENUES_DANGEREUSES_POUR_LA_SANTE = "T, T+, CMR|CMR|Santé A"  # regex

CHEMIN_CRATER_DATA_SOURCES = Path("../crater-data-sources")

CHEMIN_SOURCE_COMMUNES_ET_ARRONDISSEMENTS = Path(
    CHEMIN_CRATER_DATA_SOURCES / "insee/geographie/communes/2022/table-appartenance-geo-communes-22.xlsx"
)
CHEMIN_DOSSIER_SOURCE_GEOMETRIES_COMMUNES = Path(CHEMIN_CRATER_DATA_SOURCES / "open_street_map/geometries_communes/2022/communes-20220101")
CHEMIN_SOURCE_GEOMETRIES_EPCIS = CHEMIN_CRATER_DATA_SOURCES / "banatic" / "geometries_epcis" / "2022" / "Métropole" / "EPCI2022_region.shp"
CHEMIN_SOURCE_EPCIS = Path(CHEMIN_CRATER_DATA_SOURCES / "insee/geographie/epcis/2022/Intercommunalite_Metropole_au_01-01-2022.xlsx")
CHEMIN_SOURCE_REGROUPEMENTS_COMMUNES = Path(CHEMIN_CRATER_DATA_SOURCES / "regroupements_geographiques")
FICHIERS_REGROUPEMENTS_COMMUNES = [
    {
        "code": "BV2012",
        "fichiers_sources": [CHEMIN_SOURCE_REGROUPEMENTS_COMMUNES / "2022" / "bassins_de_vie_2012" / "BV2012_au_01-01-2022-reformate.xlsx"],
    },
    {
        "code": "PN",
        "fichiers_sources": [CHEMIN_SOURCE_REGROUPEMENTS_COMMUNES / "2021" / "parcs_nationaux" / "com2021.xlsx"],
    },
    {
        "code": "PNR",
        "fichiers_sources": [CHEMIN_SOURCE_REGROUPEMENTS_COMMUNES / "2021" / "parcs_naturels_regionaux" / "com2021.xlsx"],
    },
    {
        "code": "PAYS_PETR",
        "fichiers_sources": [CHEMIN_SOURCE_REGROUPEMENTS_COMMUNES / "2021" / "pays_et_petr" / "com2021.xlsx"],
    },
    {
        "code": "PAT",
        "fichiers_sources": [
            CHEMIN_SOURCE_REGROUPEMENTS_COMMUNES / "pat" / "0001_patly.xlsx",
            CHEMIN_SOURCE_REGROUPEMENTS_COMMUNES / "pat" / "0002_pat_pays_salonais_val_de_durance.xlsx",
            CHEMIN_SOURCE_REGROUPEMENTS_COMMUNES / "pat" / "0003_pat_pnr_des_ardennes_et_ardenne_metropole.xlsx",
            CHEMIN_SOURCE_REGROUPEMENTS_COMMUNES / "pat" / "0004_pait_dombes_val_de_saone.xlsx",
            CHEMIN_SOURCE_REGROUPEMENTS_COMMUNES / "pat" / "0005_pat_du_martres_tolosane.xlsx",
            CHEMIN_SOURCE_REGROUPEMENTS_COMMUNES / "pat" / "0006_pat_du_grand_clermont_et_pnr_livradois_forez.xlsx",
            CHEMIN_SOURCE_REGROUPEMENTS_COMMUNES / "pat" / "0007_pait_de_la_region_grenobloise.xlsx",
            CHEMIN_SOURCE_REGROUPEMENTS_COMMUNES / "pat" / "0008_pat_plaine_aux_plateaux.xlsx",
            CHEMIN_SOURCE_REGROUPEMENTS_COMMUNES / "pat" / "0009_pait_ardeche_meridionale.xlsx",
            CHEMIN_SOURCE_REGROUPEMENTS_COMMUNES / "pat" / "0010_pat_presqu_ile_briere_estuaires.xlsx",
        ],
    },
    {
        "code": "SCOT",
        "fichiers_sources": [
            CHEMIN_SOURCE_REGROUPEMENTS_COMMUNES / "scot" / "Lxx_scots_lyon.xlsx",
            CHEMIN_SOURCE_REGROUPEMENTS_COMMUNES / "scot" / "001_syndicat_intercommunal_amenagement_du_chablais.xlsx",
            CHEMIN_SOURCE_REGROUPEMENTS_COMMUNES / "scot" / "002_scot_du_biterrois.xlsx",
            CHEMIN_SOURCE_REGROUPEMENTS_COMMUNES / "scot" / "003_scot_de_l_agglomeration_tourangelle.xlsx",
            CHEMIN_SOURCE_REGROUPEMENTS_COMMUNES / "scot" / "004_scot_pays_basque_et_seignanx.xlsx",
            CHEMIN_SOURCE_REGROUPEMENTS_COMMUNES / "scot" / "005_scot_cotelub.xlsx",
        ],
    },
    {
        "code": "AT",
        "fichiers_sources": [
            CHEMIN_SOURCE_REGROUPEMENTS_COMMUNES / "autres" / "CML_carre_metropolitain_de_lyon.xlsx",
            CHEMIN_SOURCE_REGROUPEMENTS_COMMUNES / "autres" / "ISL_interscot_de_lyon.xlsx",
            CHEMIN_SOURCE_REGROUPEMENTS_COMMUNES / "autres" / "BIOVALLEE_vallee_de_la_drome.xlsx",
            CHEMIN_SOURCE_REGROUPEMENTS_COMMUNES / "autres" / "GE_gardiennes_de_l_eau_MEL.xlsx",
            CHEMIN_SOURCE_REGROUPEMENTS_COMMUNES / "autres" / "VALLEE_BARGUILLIERE_vallee_de_la_barguilliere.xlsx",
        ],
    },
]
REGROUPEMENTS_COMMUNES_PARTICULIERS = [
    {
        # composition =  Département du Rhône (D-69) - Métropole de Lyon (E-200046977)
        "nom": "Nouveau-Rhône",
        "id": CATEGORIES_REGROUPEMENTS_COMMUNES["ND"]["prefixe_id"] + "-69D",
        "categorie": CATEGORIES_REGROUPEMENTS_COMMUNES["ND"]["categorie"],
        "requete": """(id_departement == "D-69") & (id_epci != "E-200046977")""",
    },
    {
        # composition = CC Cœur de Beauce (E-200070159) + PAYS DUNOIS (PAYS_PETR-CE18)
        "nom": "PAT Beauce Dunois",
        "id": CATEGORIES_REGROUPEMENTS_COMMUNES["PAT"]["prefixe_id"] + "-0011",
        "categorie": CATEGORIES_REGROUPEMENTS_COMMUNES["PAT"]["categorie"],
        "requete": (
            """id_epci == "E-200070159" | """
            """id_commune in @composition_regroupements_communes.query('id_regroupement_communes == "PAYS_PETR-CE18"').id_commune"""
        ),
    },
    {
        # composition = Départments Bas-Rhin (D-67) + Haut-Rhin (D-68)
        "nom": "Collectivité Européenne d'Alsace",
        "id": CATEGORIES_REGROUPEMENTS_COMMUNES["AT"]["prefixe_id"] + "-CEA",
        "categorie": CATEGORIES_REGROUPEMENTS_COMMUNES["AT"]["categorie"],
        "requete": """id_departement in ['D-67', 'D-68']""",
    },
]

CHEMIN_SOURCE_GEOMETRIES_DEPARTEMENTS = CHEMIN_CRATER_DATA_SOURCES / "france_geojson" / "2018" / "departements.geojson"
CHEMIN_SOURCE_DEPARTEMENTS = Path(CHEMIN_CRATER_DATA_SOURCES / "insee/geographie/departements_regions/2022/departement_2022.csv")
CHEMIN_SOURCE_GEOMETRIES_REGIONS = CHEMIN_CRATER_DATA_SOURCES / "france_geojson" / "2018" / "regions.geojson"
CHEMIN_SOURCE_REGIONS = Path(CHEMIN_CRATER_DATA_SOURCES / "insee/geographie/departements_regions/2022/region_2022.csv")
CHEMIN_SOURCE_CODES_POSTAUX = Path(CHEMIN_CRATER_DATA_SOURCES / "la_poste/2022/laposte_hexasmal.csv")
CHEMIN_SOURCE_MOUVEMENTS_COMMUNES = Path(CHEMIN_CRATER_DATA_SOURCES / "insee/geographie/communes/2022/table_passage_geo2003_geo2022.xlsx")

CHEMIN_SOURCE_COMMUNES_PARCEL = Path(CHEMIN_CRATER_DATA_SOURCES / "parcel" / "2019" / "communes-parcel-juin-2020.csv")
CHEMIN_SOURCE_POPULATION_TOTALE = Path(CHEMIN_CRATER_DATA_SOURCES / "insee/population/2017/Communes.csv")
ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_POPULATION_TOTALE = 2019
CHEMIN_SOURCE_POPULATION_TOTALE_HISTORIQUE = Path(CHEMIN_CRATER_DATA_SOURCES / "insee/population/1968_2015/base-cc-serie-historique-2015.xlsx")
ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_POPULATION_TOTALE_HISTORIQUE = 2017
# Rq les données du RA 2010 sont basées sur les géometries de 2011,
# voir https://www.observatoire-des-territoires.gouv.fr/les-outils-interactifs/2021-outils-diacog
ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_AGRESTE_RA_2010 = 2011
CHEMIN_SOURCE_AGRESTE_RA_2010 = Path(CHEMIN_CRATER_DATA_SOURCES / "agreste/1970-2010")
ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_AGRESTE_RA_2020 = 2020
CHEMIN_SOURCE_AGRESTE_RA_2020 = Path(
    CHEMIN_CRATER_DATA_SOURCES / "agreste/2020/cartostat/cartostat_nb-exploitations_otex-12-postes_SAU_PBS_extrait_le_2023_03_23.csv"
)
CHEMIN_SOURCE_ARTIFICIALISATION = Path(CHEMIN_CRATER_DATA_SOURCES / "cerema/2009-2018/artificialisation metropole 2009_2018.csv")
ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_ARTIFICIALISATION = 2019
CHEMIN_SOURCE_NB_LOGEMENTS = Path(CHEMIN_CRATER_DATA_SOURCES / "observatoire_des_territoires/logements/2021/nombre_de_logements.xlsx")
CHEMIN_SOURCE_NB_LOGEMENTS_VACANTS = Path(CHEMIN_CRATER_DATA_SOURCES / "observatoire_des_territoires/logements/2021/nombre_de_logements_vacants.xlsx")
ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_NB_LOGEMENTS = 2021
CHEMIN_SOURCE_HVN = Path(CHEMIN_CRATER_DATA_SOURCES / "solagro/hvn/2000_2010_2017/FR_OSM2018_Score_HVN_RPG2017.xlsx")
ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_HVN = 2018
CHEMIN_SOURCE_SAU_BIO = Path(CHEMIN_CRATER_DATA_SOURCES / "agence_bio/2011-2019/Donnees_EPCI_AgenceBio_2019.xlsx")
ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_SAU_BIO = 2020
CHEMIN_SOURCE_BNVD = Path(CHEMIN_CRATER_DATA_SOURCES / "office_francais_biodiversite/bnvd/2021")
CHEMINS_DOSSIERS_SOURCE_BNVD = [
    CHEMIN_SOURCE_BNVD / "BNVD_TRACABILITE_20211213_VENTE_2015",
    CHEMIN_SOURCE_BNVD / "BNVD_TRACABILITE_20211213_VENTE_2016",
    CHEMIN_SOURCE_BNVD / "BNVD_TRACABILITE_20211213_VENTE_2017",
    CHEMIN_SOURCE_BNVD / "BNVD_TRACABILITE_20211213_VENTE_2018",
    CHEMIN_SOURCE_BNVD / "BNVD_TRACABILITE_20211213_VENTE_2019",
    CHEMIN_SOURCE_BNVD / "BNVD_TRACABILITE_20211213_VENTE_2020",
    CHEMIN_SOURCE_BNVD / "BNVD_TRACABILITE_20211213_ACHAT_2015",
    CHEMIN_SOURCE_BNVD / "BNVD_TRACABILITE_20211213_ACHAT_2016",
    CHEMIN_SOURCE_BNVD / "BNVD_TRACABILITE_20211213_ACHAT_2017",
    CHEMIN_SOURCE_BNVD / "BNVD_TRACABILITE_20211213_ACHAT_2018",
    CHEMIN_SOURCE_BNVD / "BNVD_TRACABILITE_20211213_ACHAT_2019",
    CHEMIN_SOURCE_BNVD / "BNVD_TRACABILITE_20211213_ACHAT_2020",
]

CHEMIN_SOURCE_DU_2019 = Path(CHEMIN_CRATER_DATA_SOURCES / "ministere_agriculture_alimentation/produits_phytosanitaires/arrete_2019_annexe.xlsx")
CHEMIN_SOURCE_DU_2017 = Path(CHEMIN_CRATER_DATA_SOURCES / "ministere_agriculture_alimentation/produits_phytosanitaires/arrete_2017_annexe5.csv")
CHEMIN_SOURCE_USAGES_PRODUITS = Path(CHEMIN_CRATER_DATA_SOURCES / "anses/e-phy/decisionamm-intrant-format-csv-20220126/produits_usages_v3_utf8.csv")

CHEMIN_SOURCE_COMMERCES_OSM = CHEMIN_CRATER_DATA_SOURCES / "open_street_map" / "commerces" / "2021" / "osm-shop-fr-20211214.csv"
CHEMIN_SOURCE_COMMERCES_BPE = CHEMIN_CRATER_DATA_SOURCES / "insee" / "bpe" / "2020" / "bpe20_ensemble_xy_csv.zip"
NOM_FICHIER_SOURCE_COMMERCES_BPE = "bpe20_ensemble_xy.csv"
CHEMIN_SOURCE_CARROYAGE_INSEE = CHEMIN_CRATER_DATA_SOURCES / "insee/carroyage/2015_carreaux_200m/Filosofi2015_carreaux_200m_metropole.gpkg.zip"

ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_PRELEVEMENTS_EAU_IRRIGATION = 2019
CHEMIN_SOURCE_PRELEVEMENTS_EAU_IRRIGATION = (
    CHEMIN_CRATER_DATA_SOURCES
    / "hubeau"
    / str(ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_PRELEVEMENTS_EAU_IRRIGATION)
    / "prelevements_eau_par_communes_20221116.csv"
)

ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_FLUX_AZOTE = 2022
CHEMIN_SOURCE_FLUX_AZOTE = CHEMIN_CRATER_DATA_SOURCES / "corentin_pinsard" / "2023" / "flux_azote_2023_02_16.xlsx"

CHEMIN_SOURCE_CTIFL_SERRES = CHEMIN_CRATER_DATA_SOURCES / "ctifl" / "2016" / "synthese_etude_serres_ctifl_2016.xlsx"


NOM_DOSSIER_CACHE = ".cache"

ANNEE_REFERENTIEL_TERRITOIRE_TAUX_PAUVRETE = 2020
CHEMIN_SOURCE_TAUX_PAUVRETE_COMMUNES = CHEMIN_CRATER_DATA_SOURCES / "insee" / "filosofi" / "2019" / "indic-struct-distrib-revenu-2019-COMMUNES_csv"
CHEMIN_SOURCE_TAUX_PAUVRETE_SUPRA_COMMUNES = CHEMIN_CRATER_DATA_SOURCES / "insee" / "filosofi" / "2019" / "indic-struct-distrib-revenu-2019-SUPRA_csv"
NOM_FICHIERS_TAUX_PAUVRETE = "FILO2019_DISP_Pauvres"

DOSSIER_IGN = Path("ign")

DOSSIER_CONFIG = Path("config")

CHEMIN_OUTPUT_DATA = Path("../crater-data-resultats/data")

DOSSIER_RPG = Path("rpg")
DOSSIER_PARCEL = Path("parcel")
ANNEE_REFERENTIEL_TERRITOIRES_PARCEL = 2019
DOSSIER_HUBEAU = Path("hubeau")
DOSSIER_TERRITOIRES_A_VALIDER_PARCEL = DOSSIER_PARCEL / "territoires_a_valider"
DOSSIER_BESOINS_PARCEL = DOSSIER_PARCEL / "besoins"
DOSSIER_TERRITOIRES_PARCEL = DOSSIER_PARCEL / "territoires"
DOSSIER_SURFACE_AGRICOLE_UTILE = Path("surface_agricole_utile")
DOSSIER_CRATER = Path("crater")
DOSSIER_TERRITOIRES = DOSSIER_CRATER / "territoires"
DOSSIER_REFERENTIEL_TERRITOIRES = DOSSIER_TERRITOIRES / "referentiel_territoires"
DOSSIER_DONNEES_TERRITOIRES = DOSSIER_TERRITOIRES / "donnees_territoires"
DOSSIER_OTEX_TERRITOIRES = DOSSIER_TERRITOIRES / "otex"
DOSSIER_POPULATION_AGRICOLE = DOSSIER_CRATER / "population_agricole"
DOSSIER_POLITIQUE_FONCIERE = DOSSIER_CRATER / "politique_fonciere"
DOSSIER_PRATIQUES_AGRICOLES = DOSSIER_CRATER / "pratiques_agricoles"
DOSSIER_PRODUCTIONS_BESOINS = DOSSIER_CRATER / "productions_besoins"
DOSSIER_PRODUCTION = DOSSIER_CRATER / "production"
DOSSIER_INTRANTS_AGRICOLES = DOSSIER_CRATER / "intrants_agricoles"
DOSSIER_PESTICIDES = DOSSIER_INTRANTS_AGRICOLES / "pesticides"
DOSSIER_EAU = DOSSIER_CRATER / "eau"
DOSSIER_NUTRIMENTS = DOSSIER_CRATER / "nutriments"
DOSSIER_ENERGIE = DOSSIER_CRATER / "energie"
DOSSIER_ENERGIE_IRRIGATION = DOSSIER_ENERGIE / "irrigation"
DOSSIER_ENERGIE_CARBURANTS = DOSSIER_ENERGIE / "carburants"
DOSSIER_ENERGIE_ENGRAIS_AZOTES = DOSSIER_ENERGIE / "engrais_azotes"
DOSSIER_PROXIMITE_COMMERCES = DOSSIER_CRATER / "proximite_commerces"
DOSSIER_CONSOMMATION = DOSSIER_CRATER / "consommation"
DOSSIER_CARTES = DOSSIER_CRATER / "geojson"
DOSSIER_SITEMAPS = DOSSIER_CRATER / "sitemaps"

URL_SOURCE_RPG = "ftp://RPG_ext:quoojaicaiqu6ahD@ftp3.ign.fr/"
NOMS_FICHIERS_RPG = {
    2019: {
        # DOMTOM
        "R-1": "RPG_2-0__SHP_UTM20W84GUAD_D971-2019_2019-01-15.7z",
        "R-2": "RPG_2-0__SHP_UTM20W84MART_D972-2019_2019-01-15.7z",
        "R-3": "RPG_2-0__SHP_UTM22RGFG95_D973-2019_2019-01-15.7z",
        "R-4": "RPG_2-0__SHP_RGR92UTM40S_D974-2019_2019-01-15.7z",
        "R-6": "RPG_2-0__SHP_RGM04UTM38S_D976-2019_2019-01-15.7z",
        # METROPOLE
        "R-11": "RPG_2-0__SHP_LAMB93_R11-2019_2019-01-15.7z",
        "R-24": "RPG_2-0__SHP_LAMB93_R24-2019_2019-01-15.7z",
        "R-27": "RPG_2-0__SHP_LAMB93_R27-2019_2019-01-15.7z",
        "R-28": "RPG_2-0__SHP_LAMB93_R28-2019_2019-01-15.7z",
        "R-32": "RPG_2-0__SHP_LAMB93_R32-2019_2019-01-15.7z",
        "R-44": "RPG_2-0__SHP_LAMB93_R44-2019_2019-01-15.7z",
        "R-52": "RPG_2-0__SHP_LAMB93_R52-2019_2019-01-15.7z",
        "R-53": "RPG_2-0__SHP_LAMB93_R53-2019_2019-01-15.7z",
        "R-75": "RPG_2-0__SHP_LAMB93_R75-2019_2019-01-15.7z",
        "R-76": "RPG_2-0__SHP_LAMB93_R76-2019_2019-01-15.7z",
        "R-84": "RPG_2-0__SHP_LAMB93_R84-2019_2019-01-15.7z",
        "R-93": "RPG_2-0__SHP_LAMB93_R93-2019_2019-01-15.7z",
        "R-94": "RPG_2-0__SHP_LAMB93_R94-2019_2019-01-15.7z",
    },
    2018: {
        # DOMTOM
        "R-1": "RPG_2-0__SHP_UTM20W84GUAD_D971-2018_2018-01-15.7z.001",
        "R-2": "RPG_2-0__SHP_UTM20W84MART_D972-2018_2018-01-15.7z.001",
        "R-3": "RPG_2-0__SHP_UTM22RGFG95_D973-2018_2018-01-15.7z.001",
        "R-4": "RPG_2-0__SHP_RGR92UTM40S_D974-2018_2018-01-15.7z.001",
        "R-6": "RPG_2-0__SHP_RGM04UTM38S_D976-2018_2018-01-15.7z.001",
        # METROPOLE
        "R-11": "RPG_2-0__SHP_LAMB93_R11-2018_2018-01-15.7z.001",
        "R-24": "RPG_2-0__SHP_LAMB93_R24-2018_2018-01-15.7z.001",
        "R-27": "RPG_2-0__SHP_LAMB93_R27-2018_2018-01-15.7z.001",
        "R-28": "RPG_2-0__SHP_LAMB93_R28-2018_2018-01-15.7z.001",
        "R-32": "RPG_2-0__SHP_LAMB93_R32-2018_2018-01-15.7z.001",
        "R-44": "RPG_2-0__SHP_LAMB93_R44-2018_2018-01-15.7z.001",
        "R-52": "RPG_2-0__SHP_LAMB93_R52-2018_2018-01-15.7z.001",
        "R-53": "RPG_2-0__SHP_LAMB93_R53-2018_2018-01-15.7z.001",
        "R-75": "RPG_2-0__SHP_LAMB93_R75-2018_2018-01-15.7z.001",
        "R-76": "RPG_2-0__SHP_LAMB93_R76-2018_2018-01-15.7z.001",
        "R-84": "RPG_2-0__SHP_LAMB93_R84-2018_2018-01-15.7z.001",
        "R-93": "RPG_2-0__SHP_LAMB93_R93-2018_2018-01-15.7z.001",
        "R-94": "RPG_2-0__SHP_LAMB93_R94-2018_2018-01-15.7z.001",
    },
    2017: {
        # DOMTOM
        "R-1": "RPG_2-0__SHP_UTM20W84GUAD_R01-2017_2017-01-01.7z.001",
        "R-2": "RPG_2-0__SHP_UTM20W84MART_R02-2017_2017-01-01.7z.001",
        "R-3": "RPG_2-0__SHP_UTM22RGFG95_R03-2017_2017-01-01.7z.001",
        "R-4": "RPG_2-0__SHP_RGR92UTM40S_R04-2017_2017-01-01.7z.001",
        "R-6": "RPG_2-0__SHP_RGM04UTM38S_R06-2017_2017-01-01.7z.001",
        # METROPOLE
        "R-11": "RPG_2-0__SHP_LAMB93_R11-2017_2017-01-01.7z.001",
        "R-24": "RPG_2-0__SHP_LAMB93_R24-2017_2017-01-01.7z.001",
        "R-27": "RPG_2-0__SHP_LAMB93_R27-2017_2017-01-01.7z.001",
        "R-28": "RPG_2-0__SHP_LAMB93_R28-2017_2017-01-01.7z.001",
        "R-32": "RPG_2-0__SHP_LAMB93_R32-2017_2017-01-01.7z.001",
        "R-44": "RPG_2-0__SHP_LAMB93_R44-2017_2017-01-01.7z.001",
        "R-52": "RPG_2-0__SHP_LAMB93_R52-2017_2017-01-01.7z.001",
        "R-53": "RPG_2-0__SHP_LAMB93_R53-2017_2017-01-01.7z.001",
        "R-75": "RPG_2-0__SHP_LAMB93_R75-2017_2017-01-01.7z.001",
        "R-76": "RPG_2-0__SHP_LAMB93_R76-2017_2017-01-01.7z.001",
        "R-84": "RPG_2-0__SHP_LAMB93_R84-2017_2017-01-01.7z.001",
        "R-93": "RPG_2-0__SHP_LAMB93_R93-2017_2017-01-01.7z.001",
        "R-94": "RPG_2-0__SHP_LAMB93_R94-2017_2017-01-01.7z.001",
    },
}
