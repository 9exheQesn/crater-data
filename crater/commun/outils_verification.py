import geopandas as gpd
from pandas import DataFrame

from crater.commun.config import NUMEROS_DEPARTEMENTS_DROM
from crater.commun.logger import log


def verifier_absence_doublons(df: DataFrame, colonne: str) -> None:
    doublons = df[df[colonne].duplicated()][colonne].tolist()
    if len(doublons) > 0:
        if len(doublons) > 100:
            doublons = doublons[:100] + ["..."]
        raise ValueError("ERREUR: plusieurs exemplaires pour : '" + colonne + "' : " + ", ".join(doublons))


def verifier_colonne2_unique_par_colonne1(df: DataFrame, colonne1: str, colonne2: str, warning_only=False) -> None:
    nb_uniques_df1 = df.groupby(colonne1).nunique().reset_index()
    l1 = nb_uniques_df1[nb_uniques_df1[colonne2] > 1][colonne1].to_list()
    if len(l1) > 0:
        if len(l1) > 100:
            l1 = l1[:100] + ["..."]
        if warning_only:
            log.info("ATTENTION, les '" + colonne1 + "' suivants ont plusieurs '" + colonne2 + "' : " + ", ".join(l1))
        else:
            raise ValueError("ERREUR: les '" + colonne1 + "' suivants ont plusieurs '" + colonne2 + "' : " + ", ".join(l1))


def verifier_absence_na(df: DataFrame, colonne_id: str, colonne: str) -> None:
    liste_territoires_colonne_na = df.loc[df[colonne].isnull()].reset_index()[colonne_id].tolist()
    if len(liste_territoires_colonne_na) > 0:
        if len(liste_territoires_colonne_na) > 100:
            liste_territoires_colonne_na = liste_territoires_colonne_na[:100] + ["..."]
        raise ValueError("ERREUR: NA dans " + colonne + " pour les territoires : " + ", ".join(liste_territoires_colonne_na))


def verifier_nommage_ids_territoires(df: DataFrame, colonne_id: str) -> None:
    liste_territoires_problematiques = df[df[colonne_id].str.contains(" ")][colonne_id].tolist()
    if len(liste_territoires_problematiques) > 0:
        raise ValueError("ERREUR: des " + colonne_id + " contiennent des espaces : " + ", ".join(liste_territoires_problematiques))


def verifier_coherence_referentiel_avec_geometries_communes(referentiel_territoires: DataFrame, geometries_communes: gpd.GeoDataFrame) -> None:
    log.info("##### VERIFICATION DES GEOMETRIES DES COMMUNES #####")

    communes_referentiel = (
        referentiel_territoires.reset_index()
        .query("categorie_territoire == 'COMMUNE'")
        .rename(columns={"id_territoire": "id_commune"})["id_commune"]
        .sort_values()
        .to_list()
    )
    geometries_communes = geometries_communes["id_commune"].sort_values().to_list()
    geometries_communes = [x for x in geometries_communes if x[2:4] not in NUMEROS_DEPARTEMENTS_DROM]

    if geometries_communes != communes_referentiel:
        log.info(
            "Communes présentes dans les géométries mais absentes du réferentiel: "
            + "|".join(sorted(list(set(geometries_communes) - set(communes_referentiel))))
        )
        log.info(
            "Communes présentes dans le réferentiel mais absentes des géométries: "
            + "|".join(sorted(list(set(communes_referentiel) - set(geometries_communes))))
        )
        raise ValueError("ERREUR : référentiel des communes incompatible avec les géométries!")
    else:
        log.info("    => Référentiel des communes compatible avec les géométries")
