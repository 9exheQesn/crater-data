import numpy as np
import pandas as pd
from pandas import Series

from crater.commun.logger import log


def message_synthese(intro, parametres):
    # intro: mots introductifs de la synthese
    # parametres = liste de blocs de message
    # chaque bloc étant composé de [x, intro, ordre, seuils, adjectifs, valeurs_adjectifs]
    # x: valeur de la variable input
    # intro: debut du message du bloc
    # ordre: croissant si plus la valeur est élevée, meilleur est le message
    # seuils: liste des seuils definissant les classes
    # adjectifs: adjectif associé à chaque classe
    # valeur adjectif: valeur de l'adjectif pour choix adverbe

    # test valeurs correctes:
    try:
        somme = 0
        for p in range(len(parametres)):
            somme += parametres[p][0]
        if not np.isfinite(somme):
            return "Données non disponibles"
    except Exception:
        return "Données non disponibles"

    # si ok:

    # classes
    classes = []
    for p in range(len(parametres)):
        classes.append(0)
        if parametres[p][2] == "croissant":
            for s in range(len(parametres[p][3])):
                if parametres[p][0] < parametres[p][3][s]:
                    classes[p] = s
                    break
                else:
                    classes[p] = s + 1
        elif parametres[p][2] == "décroissant":
            for s in range(len(parametres[p][3])):
                if parametres[p][0] > parametres[p][3][s]:
                    classes[p] = s
                    break
                else:
                    classes[p] = s + 1
        else:
            log.info(parametres[p][2])
            raise ValueError("ERREUR : argument incorrect !")

    # messages
    messages = []
    for p in range(len(parametres)):
        messages.append(parametres[p][1] + "<strong>" + parametres[p][4][classes[p]] + "</strong>")

    # ordre messages
    for p in range(len(parametres)):
        for i in range(len(parametres) - 1):
            if classes[i + 1] > classes[i]:
                messages[i + 1], messages[i] = messages[i], messages[i + 1]
                classes[i + 1], classes[i] = classes[i], classes[i + 1]

    # adverbes
    adverbes = [", "] * (len(parametres) - 2) + [" et "] * (min(len(parametres) - 1, 1))
    for p in range(len(parametres) - 1):
        if parametres[p][5][classes[p]] - parametres[p][5][classes[p + 1]] > 0:
            adverbes[p] = " mais "

    # message final
    message = (intro + messages[0]).capitalize()
    for p in range(len(parametres) - 1):
        message += adverbes[p] + messages[p + 1]

    return message


def discretiser_indicateur(
    indicateur: Series,
    liste_seuils,
    liste_valeurs_classes,
    type_classes,
    valeur_classe_defaut,
):
    valeurs_discretisees = pd.cut(
        indicateur,
        bins=[-np.inf, *liste_seuils, np.inf],
        labels=liste_valeurs_classes,
        right=False,
        include_lowest=True,
        ordered=False,
    )
    return valeurs_discretisees.astype(type_classes).fillna(valeur_classe_defaut)


def construire_message(
    introduction_globale,
    indicateurs1: Series,
    intro1: str,
    liste_seuils1,
    liste_messages1,
    valeurs_messages1,
    indicateurs2: Series,
    intro2: str,
    liste_seuils2,
    liste_messages2,
    valeurs_messages2,
):
    df = pd.DataFrame([indicateurs1, indicateurs2], index=["indicateur1", "indicateur2"]).transpose()
    df["message1"] = (
        intro1 + "<strong>" + discretiser_indicateur(df["indicateur1"], liste_seuils1, liste_messages1, pd.StringDtype(), "") + "</strong>"
    )
    df["valeur_message1"] = discretiser_indicateur(df["indicateur1"], liste_seuils1, valeurs_messages1, pd.Int64Dtype(), 0)
    df["message2"] = (
        intro2 + "<strong>" + discretiser_indicateur(df["indicateur2"], liste_seuils2, liste_messages2, pd.StringDtype(), "") + "</strong>"
    )
    df["valeur_message2"] = discretiser_indicateur(df["indicateur2"], liste_seuils2, valeurs_messages2, pd.Int64Dtype(), 0)
    mask_messages_a_inverser = df["valeur_message1"].abs() < df["valeur_message2"].abs()
    df.loc[mask_messages_a_inverser, ["message1", "valeur_message1", "message2", "valeur_message2"],] = df.loc[
        mask_messages_a_inverser,
        ["message2", "valeur_message2", "message1", "valeur_message1"],
    ].values

    df["mot_liaison"] = " et "
    mask_messages_en_opposition = df["valeur_message1"] * df["valeur_message2"] < 0
    df.loc[mask_messages_en_opposition, "mot_liaison"] = " mais "
    df["message"] = introduction_globale + df["message1"] + df["mot_liaison"] + df["message2"]
    df.loc[df["indicateur1"].isnull() | df["indicateur2"].isnull(), "message"] = "Données non disponibles"
    df["message"] = df["message"].str.capitalize()
    return df["message"]


def calculer_note_par_interpolation_2_seuils(
    indicateur: Series,
    seuil1: float,
    note_seuil1: float,
    seuil2: float,
    note_seuil2: float,
) -> Series:
    coefficient_directeur = (note_seuil2 - note_seuil1) / (seuil2 - seuil1)

    note: Series = (indicateur - seuil1) * coefficient_directeur + note_seuil1
    note = note.clip(lower=0, upper=10).round(2)
    return note


def calculer_note_par_interpolation_3_seuils(
    indicateur: Series,
    seuil1: float,
    note_seuil1: float,
    seuil2: float,
    note_seuil2: float,
    seuil3: float,
    note_seuil3: float,
) -> Series:

    note: Series = (
        calculer_note_par_interpolation_2_seuils(indicateur, seuil1, note_seuil1, seuil2, note_seuil2)
        .mask(
            indicateur > seuil2,
            calculer_note_par_interpolation_2_seuils(indicateur, seuil2, note_seuil2, seuil3, note_seuil3),
        )
        .clip(lower=0, upper=10)
        .round(2)
    )
    return note
