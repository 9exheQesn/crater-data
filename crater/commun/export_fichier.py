import io
import platform
import shutil
import subprocess
import sys
from datetime import datetime
from pathlib import Path
from pandas import DataFrame

from crater.commun.logger import log
from crater.commun.config import NOM_DOSSIER_CACHE
from crater.commun.statistiques import calculer_statistiques


def reinitialiser_dossier(nom_dossier: Path, reinitialiser_cache: bool = False):
    if nom_dossier.exists():
        for contenu in nom_dossier.iterdir():
            if contenu.is_file():
                contenu.unlink()
            elif contenu.is_dir() & (contenu.name != NOM_DOSSIER_CACHE):
                shutil.rmtree(contenu)
            elif contenu.is_dir() & (contenu.name == NOM_DOSSIER_CACHE) & reinitialiser_cache:
                shutil.rmtree(contenu)
    else:
        nom_dossier.mkdir(parents=True)


def exporter_csv(
    df: DataFrame,
    nom_fichier_output: Path,
    avec_index=False,
    float_format=None,
    entete=None,
):
    if entete is None:
        df.to_csv(
            nom_fichier_output,
            sep=";",
            encoding="utf-8",
            index=avec_index,
            float_format=float_format,
        )
    else:
        df_csv = df.to_csv(
            sep=";",
            encoding="utf-8",
            index=avec_index,
            float_format=float_format,
        )

        with open(nom_fichier_output, "w") as fp:
            fp.write(f"{entete}\n{df_csv}")


def exporter_meta_donnees(
    df: DataFrame,
    dossier_output: Path,
    nom_fichier_sans_extenstion: str,
):
    meta_donnees = f"# Informations sur le fichier {nom_fichier_sans_extenstion + '.csv'}, dans le dossier {str(dossier_output)}\n\n"
    meta_donnees += f"   Date de génération              : {datetime.now().strftime('%d/%m/%Y %H:%M:%S')}\n"
    meta_donnees += f"   OS utilisé pour la génération   : {platform.platform()}\n"
    meta_donnees += "   Version python                  : " + sys.version.replace("\n", "") + "\n"
    commit_id_long, commit_id_court, nom_branche = _recuperer_infos_git()
    meta_donnees += f"   GIT Id commit long crater-data  : {commit_id_long}\n"
    meta_donnees += f"   GIT Id commit court crater-data : {commit_id_court}\n"
    meta_donnees += f"   GIT Nom branche crater-data     : {nom_branche}\n"

    buffer_infos_dataframe = io.StringIO()
    df.info(verbose=True, buf=buffer_infos_dataframe)
    meta_donnees += f"\n\n# Description du dataframe\n\n{buffer_infos_dataframe.getvalue()}"

    with open(dossier_output / (nom_fichier_sans_extenstion + ".txt"), "w", encoding="utf-8") as f:
        f.write(meta_donnees)


def _recuperer_infos_git():
    try:
        commit_id_long = subprocess.check_output(["git", "rev-parse", "HEAD"]).decode("ascii").strip()
        commit_id_court = subprocess.check_output(["git", "rev-parse", "--short", "HEAD"]).decode("ascii").strip()
        nom_branche = subprocess.check_output(["git", "rev-parse", "--abbrev-ref", "HEAD"]).decode("ascii").strip()
    except Exception:
        commit_id_long = "Inconnu (Erreur d'accès au process git)"
        commit_id_court = "Inconnu (Erreur d'accès au process git)"
        nom_branche = "Inconnu (Erreur d'accès au process git)"
    return commit_id_long, commit_id_court, nom_branche


def exporter_df_indicateurs_par_territoires(
    df: DataFrame,
    dossier_output: Path,
    nom_fichier_sans_extension,
    nom_colonne_regroupement=None,
):
    log.info(f"    => Export fichier {dossier_output}/{nom_fichier_sans_extension}.csv")
    df = _reinitialiser_index(df)
    exporter_df(df, dossier_output, nom_fichier_sans_extension, "%.2f")
    exporter_csv(
        calculer_statistiques(df, nom_colonne_regroupement),
        dossier_output / (nom_fichier_sans_extension + ".stats.csv"),
        float_format="%.2f",
    )


def exporter_df(
    df,
    dossier_output,
    nom_fichier_sans_extension,
    float_format="%.2f",
):
    exporter_csv(df, dossier_output / (nom_fichier_sans_extension + ".csv"), False, float_format)
    exporter_meta_donnees(df, dossier_output, nom_fichier_sans_extension)
    return df


def _reinitialiser_index(df):
    if df.index.name is not None:
        df = df.reset_index()
    return df
